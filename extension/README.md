# rpany extension

The purpose of this demonstration is to show how new words and vocabularies can
be formed outside of the rpany namespace.

## Introduction

rpany has the concept of a 'registry' which holds 'vocabularies'.

Vocabularies are introduced to the registry using the RPANY\_OPEN and RPANY\_CLOSE
macros and teaching the stack new 'words'.

They can be created anywhere and host any manner of functionality.

The rpany 'extension' directory contains a collection of random vocabularies -
they range in quality and usefulness, but are predominantly designed to show
varying techniques which can be employed to extend the rpany functionality.

## Using Vocabularies

Upon construction of a stack object, we can teach it all the registered
vocabularies using:

```
rpany::stack s;
rpany::educate( s );
```

Alternatively, we can restrict the available vocabs using:

```
rpany::stack s;
rpany::educate( s, { "arith", "dot" } );
```

which will attempt to find the requested vocabs.

Vocabularies can be trivially created in the hosting application. This document
attempts to explain the process.

## Function Example

A full example of a simple function is shown here:

```
// Define a standard c++ function
double factorial( const int &n )
{
	if ( n <= 0 )
		return 1;
	else
		return n * factorial( n - 1 );
}

// Define and register a vocabulary
RPANY_OPEN( factorial, rpany::stack &s )
	s.teach( "fac", []( rpany::stack & ) { s += factorial( s.pull< int >( ) ); } );
	s.teach( "fac.", { "fac", "." } );
RPANY_CLOSE( name );
```

The general form is:

```
// Standard c++ function/class definitions

// Create a vocabulary
RPANY_OPEN( name, rpany::stack &s )
	// Teach stack s new words here
RPANY_CLOSE( name );
```

In the body of the vocab definition, we can teach the stack new words using the
overloaded 'teach' method.

While there are many forms which are valid here, the most trivial mechanism is
simply to provide it with a function which has a signature of:

```
void function( rpany::stack &s )
```

Assuming its existence, then it is sufficient to just use:

```
s.teach( "f", function );
```

Additionally, we can use existing words like:

```
s.teach( "f.", { "f", "." } );
```

Thus when executing f., f is invoked first, then . is invoked to output the top
of stack.

Yet another alternative is to use lambda's:

```
s.teach( "name", []( rpany::stack &s ) { ... } );
```

which is how the fac word is defined in the example above.

See below for details on how vocabularies are used.

## Class Example

Again, assuming a simple class like:

```
class rectangle 
{
	public:
		void set_width( double w ) { width = w; }
		void set_height( double h ) { height = h; }
		double area( ) { return width * height; }
	private:
		double width = 0, height = 0;
};
```

We can define a vocabulary called rectangle:

```
RPANY_OPEN( rectangle, rpany::stack &s )
	typedef std::shared_ptr< rectangle > rectangle_ptr;
	s.teach( "rect", [] ( rpany::stack &s ) { s += rectangle_ptr( new rectangle ); } );
	s.teach( "rect-width", [] ( rpany::stack &s ) { auto w = s.pull< double >( ); s.cast< rectangle_ptr >( )->set_width( w ); } );
	s.teach( "rect-height", [] ( rpany::stack &s ) { auto h = s.pull< double >( ); s.cast< rectangle_ptr >( )->set_height( h ); } );
	s.teach( "rect-area", [] ( rpany::stack &s ) { s += s.cast< rectangle_ptr >( )->area( ); } );
	s.teach( "rect-perimeter", [] ( rpany::stack &s ) { s += s.cast< rectangle_ptr >( )->perimeter( ); } );
	rpany::coercions[ COERCE_KEY( rectangle_ptr, std::string ) ] = []( const boost::any & ) { return boost::any( std::string( "<<rectangle>>" ) ); };
RPANY_CLOSE( rectangle );
```

This should be quite similar to the function example above.

To simplify things, it's advised that you use a std::shared\_ptr to wrap the
instances:

```
typedef std::shared_ptr< rectangle > rectangle_ptr;
```

With this in place, we can teach the stack how to create an instance of
rectangle using:

```
s.teach( "rect", [] ( rpany::stack &s ) { s.append( rectangle_ptr( new rectangle ) ); } );
```

This will put a new rectangle object at the top of the stack.

Assuming we want a usage like:

```
rect 50 rect-width 30 rect-height rect-area .
```

We would register each method as follows:

```
s.teach( "rect-width", [] ( rpany::stack &s ) { auto w = s.pull< double >( ); s.cast< rectangle_ptr >( )->set_width( w ); } );
s.teach( "rect-height", [] ( rpany::stack &s ) { auto h = s.pull< double >( ); s.cast< rectangle_ptr >( )->set_height( h ); } );
s.teach( "rect-area", [] ( rpany::stack &s ) { s += s.cast< rectangle_ptr >( )->area( ); } );
```

Finally, to avoid issues with the new object tripping casting execptions, we can
add a coercion rule like:

```
rpany::coercions[ COERCE_KEY( rectangle_ptr, std::string ) ] = []( const boost::any & ) { return boost::any( std::string( "<<rectangle>>" ) ); };
```

This should be sufficient to provide the syntax above.
