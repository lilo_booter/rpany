#pragma once

bool _extension_enrolled = rpany::enroll( "extension" );

#include "factorial.hpp"
#include "foo.hpp"
#include "process.hpp"
#include "rectangle.hpp"
#include "sum.hpp"
