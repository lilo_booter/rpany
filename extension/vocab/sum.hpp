#pragma once

// Define a simple function
inline double sum( const int &count )
{
	double result = 0;
	for ( double n = 0; n < static_cast< double >( count ); n ++ )
		result += n;
	return result;
}

RPANY_OPEN( sum, rpany::stack &s )
	s.teach( "sum", [] ( rpany::stack &s ) { s += sum( s.pull< int >( ) ); } );
RPANY_CLOSE( sum );

// These alternatives are defined for some basic benchmarking
template< typename T >
inline void ssum( rpany::stack &s, const int &count )
{
	s << 0.0;
	for( T n = 0; n < static_cast< T >( count ); n ++ )
		s << n << "+";
}

RPANY_OPEN( stack_sum, rpany::stack &s )
	s.teach( "ssum-double", [] ( rpany::stack &s ) { ssum< double >( s, s.pull< int >( ) ); } );
	s.teach( "ssum-int", [] ( rpany::stack &s ) { ssum< int >( s, s.pull< int >( ) ); } );
RPANY_CLOSE( stack_sum );
