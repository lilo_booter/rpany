#pragma once

RPANY_OPEN( process, rpany::stack &s )
	s.teach( "system", [] ( rpany::stack &s ) { s += system( s.pull< std::string >( ).c_str( ) ); } );
RPANY_CLOSE( process );
