#pragma once

// Define a simple class
class foo
{
	public:
		void bar( )
		{
			std::cout << "called " << ++ n_ << std::endl;
		}

	private:
		int n_ = 0;
};

RPANY_OPEN( foo, rpany::stack &s )
	typedef std::shared_ptr< foo > foo_ptr;
	s.teach( "foo", [] ( rpany::stack &s ) { s += foo_ptr( new foo ); } );
	s.teach( "bar", [] ( rpany::stack &s ) { s.cast< foo_ptr >( )->bar( ); } );
	rpany::coercion( COERCE_KEY( foo_ptr, std::string ), [] ( const boost::any & ) { return boost::any( std::string( "<<foo>>" ) ); } );
RPANY_CLOSE( foo );
