#pragma once

// Define a factorial function
inline double factorial( const int &n )
{
	if ( n <= 0 )
		return 1.0;
	else
		return n * factorial( n - 1 );
}

RPANY_OPEN( factorial, rpany::stack &s )
	s.teach( "fac", [] ( rpany::stack &s ) { s += factorial( s.pull< int >( ) ); } );
	s.teach( "fac.", { "fac", "." } );
RPANY_CLOSE( factorial );
