#pragma once

// Define a rectangle class
class rectangle
{
	public:
		void set_width( const double &w ) { width = w; }
		void set_height( const double &h ) { height = h; }
		double area( ) const { return width * height; }
		double perimeter( ) const { return 2 * ( width + height ); }
	private:
		double width = 0, height = 0;
};

RPANY_OPEN( rectangle, rpany::stack &s )
	typedef std::shared_ptr< rectangle > rectangle_ptr;
	s.teach( "rect", [] ( rpany::stack &s ) { s += rectangle_ptr( new rectangle ); } );
	s.teach( "rect-width", [] ( rpany::stack &s ) { auto width = s.pull< double >( ); s.cast< rectangle_ptr >( )->set_width( width ); } );
	s.teach( "rect-height", [] ( rpany::stack &s ) { auto height = s.pull< double >( ); s.cast< rectangle_ptr >( )->set_height( height ); } );
	s.teach( "rect-area", [] ( rpany::stack &s ) { s += s.cast< rectangle_ptr >( )->area( ); } );
	s.teach( "rect-perimeter", [] ( rpany::stack &s ) { s += s.cast< rectangle_ptr >( )->perimeter( ); } );
	rpany::coercion( COERCE_KEY( rectangle_ptr, std::string ), [] ( const boost::any & ) { return boost::any( std::string( "<<rectangle>>" ) ); } );
RPANY_CLOSE( rectangle );
