#include <rpany/rpany-host.hpp>
#include "vocab/__init__.hpp"

int main( int argc, char **argv )
{
	// Create the stack
	rpany::stack stack;

	// Load all registered vocabs
	rpany::educate( stack );

	// For now, just parse the arguments (ie: 1000000 sum . foo bar bar dump dup bar)
	return rpany::eval( stack, argc, argv, 1 );
}
