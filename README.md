# rpany

A [FORTH]-like C++ API implemented as a header only library.

The name is pronounced 'r p any' and it's a play on Reverse Polish Notation ([RPN])
and the underlying C++ component in use - [std::any]. The basic concept is that the
stack consists of 'any' objects.

## Features

* C++ API
* FORTH-like RPN based language
* C++ binding mechanism
* Scripting environment
* Read Evaluate Print Loop ([REPL]) functionality

## Usage

As a command line processing tool:

```
rpany
rpany $ "hello world"
rpany 1 2 3 "*" +
```

As a scripting tool:

```
rpany <<< '$ "hello world"'
echo '1 2 3 * +" | rpany
```

As a locked down interactive shell:

```
$ rpany-shell --basic
basic> help
arith : + - * / %
dot   : .
help  : help
basic> 1 2 3 * + .
7
```

As a full interactive shell:

```
$ rpany-shell
> $ "hello world" .
hello world
> 1 2 3 * + .
7
> : square 2 ** ;
> : sqrt 0.5 ** ;
> : hypot square swap square + sqrt ;
> 1 1 hypot .
1.4142135623731
> 2 1 hypot .
2.23606797749979
> 10 0 do i , space loop cr
0 1 2 3 4 5 6 7 8 9
```

As a shbang'd scripting tool:

```
#!/usr/bin/env rpany
$ "hello world" .
```

Or, indeed, as a library:

```
#include <rpany/rpany-host.hpp>

void myword( rpany::stack &stack )
{
	// Append directly to the data stack
	stack += "hello world";

	// Invoke dot to print the 'Top Of Stack' (TOS)
	stack << ".";
}

RPANY_OPEN( myvocab, rpany::stack &s )
	s.teach( "myword", myword );
RPANY_CLOSE( myvocab );

int main( int argc, char **argv )
{
	// Create the stack
	rpany::stack stack;

	// Educate the stack instance with all vocabs
	rpany::educate( stack );

	// Invoke the custom word defined above
	stack << "myword";

	// Do stuff
	double result;
	stack << 1 << 2 << 3 << "*" << "+" >> result;

	// Output the result
	std::cout << "Result is " << result << std::endl;

	return 0;
}
```

The stack can accept 'any' type which can be wrapped as a std::any or boost:any.
Internally, the used type is known as rp::any.

Vocabularies can be added at any point and can be defined arbitrarily in the
hosting application code.

If a hosting application has multiple source files which need the rpany
functionality, one of them must include rpany-host.hpp and the others should
use rpany.hpp.

## Prerequisites

The default requirements are:

* [cmake]
* C++17 compiler
* [boost::rational]
* [boost::format]

Alternatively, older c++ standards are supported by way of the boost library:

* [cmake]
* C++11 compiler
* [boost::any]
* [boost::format]
* [boost::rational]

## Build

The build system used is cmake - to build it:

```
mkdir out
cmake -DCMAKE_CXX_FLAGS="-O3 -Wall" -S . -B out
cmake --build out -- -j$( nproc )
sudo cmake --build out -- -j$( nproc ) install
```

I really don't know Windows, but this seems to work with cygwin:

```
mkdir release
cmake -G "$VMB_GENERATOR" -DCMAKE_INSTALL_PREFIX="$( cygpath -m "$VMB_PREFIX" )" -S . -B release
cmake --build release --config Release --target install
```

Where `VMB_GENERATOR` was "Visual Studio 14 2015 Win64" and `VMB_PREFIX` was the
same prefix I had already installed by boost headers to.

## Terminology

A `stack` accepts a sequence of `tokens`.

A `token` is a `number`, a `word` or a `string`.

Numbers are passed directly to the `data stack`.

Words manipulate the incoming tokens, data stack and strings.

Words are typically organised as collections known as `vocabularies`.

While words can be redefined, there is always only a single definition of a
given word. This is always the most recent definition of the word.

To avoid ambiguities, a word may be referrred to as ::vocab-\>word. If the word
doesn't (or no longer) exists within that vocab, an error is thrown.

## External Projects

External projects provide their own extending vocabularies:

* [rpany-linenoise-ng] - improves shell functionality
* [rpany-comms] - a client/server extension for rpany
* [rpany-pi] - a Raspberry Pi mixin
* [vml-batch] - a general purpose media processing tool

## Vocabularies

### `help`

Displays all available vocabs and words.

| Word   | Usage  | Result | Comment                         |
| ------ | ------ | ------ | ------------------------------- |
| `help` | `help` |        | Displays the stack's dictionary |

### `arith`

Provides basic arithmetic.

| Word   | Usage                      | Result     | Comment    |
| ------ | -------------------------- | ---------- | ---------- |
| `+`    | `<number> <number> +`      | `<number>` | Adds       |
| `-`    | `<number> <number> -`      | `<number>` | Subtracts  |
| `*`    | `<number> <number> *`      | `<number>` | Multiplies |
| `/`    | `<number> <number> /`      | `<number>` | Divides    |
| `%`    | `<number> <number> %`      | `<number>` | Modulus    |

### `dot`

Provides a means to print the top of stack.

| Word   | Usage                   | Result     | Comment                                         |
| ------ | ----------------------- | ---------- | ----------------------------------------------- |
| `.`    | `<item> .`              |            | Prints top item with new line and removes it    |

### `string`

Provides a means to introduce a string to the stack.

| Word   | Usage                   | Result      | Comment                                          |
| ------ | ----------------------- | ----------- | ------------------------------------------------ |
| `$`    | `$ <string>`            | `<string>`  | Puts the string at TOS                           |

### `io`

Provides input/output.

| Word     | Usage                   | Result     | Comment                                         |
| -------- | ----------------------- | ---------- | ----------------------------------------------- |
| `,`      | `<item> ,`              |            | Prints TOS without new line and removes it      |
| `emit`   | `<char> emit`           |            | Prints TOS as a char and removes it             |
| `read`   | `read`                  | `<string>` | Reads a string from stdin and puts it at TOS    |
| `eof`    | `eof`                   | `<bool>`   | Indicates if the input is at eof                |
| `cr`     | `cr`                    |            | Outputs a new line                              |
| `space`  | `space`                 |            | Outputs a space                                 |
| `flush`  | `flush`                 |            | Flushes the output stream                       |
| `prompt` | `<string> prompt`       | `<string>` | Prints tos, reads input and returns result      |

### `format`

Provides a means to format strings using a c-style format string.

| Word     | Usage                   | Result         | Comment                                          |
| -------- | ----------------------- | -------------- | ------------------------------------------------ |
| `fprint` | `... <string> fprint`   | `<string>`     | Creates a string output and removes inputs       |
| `fpeek`  | `... <string> fpeek`    | `... <string>` | Same but leaves inputs intact                    |
| `f.`     | `... f. <string>`       |                | Format and display                               |
| `f,`     | `... f. <string>`       |                | Format and display without new line              |
| `f$`     | `... f$ <string>`       | `<string>`     | Format and leave output                          |
| `fp.`    | `... fp. <string>`      | `...`          | Format and display with new line, leaving inputs |
| `fp,`    | `... fp. <string>`      | `...`          | Format and display, leaving inputs               |
| `fp$`    | `... fp$ <string>`      | `... <string>` | Format and leave inputs/output                   |

The format string is evaluated from left to right, hence:

```
> f. "hello world"
hello world
> $ world f. "hello %s"
hello world
> $ hello $ world f. "%s %s"
hello world
> $ "hello world" f. "%s"
hello world
```

Some support is provided for alignment and padding using a subset of the typical
`%[flags][width][.precision]specifier` format of the std::format (or boost::format
for pre-c++20) library.

### `stack`

Provides various stack manipulations.

| Word    | Usage                             | Result                                  | Comment                                |
| ------- | --------------------------------- | --------------------------------------- | -------------------------------------- |
| `clear` | `... clear`                       |                                         | Empties the entire stack               |
| `dup`   | `<item> dup`                      | `<item> <item>`                         | Duplicates the item at TOS             |
| `over`  | `<item1> <item2> over`            | `<item1> <item2> <item1>`               | Duplicates the item below TOS          |
| `swap`  | `<item1> <item2> swap`            | `<item2> <item1>`                       | Swaps the top two items at TOS         |
| `rot`   | `<item1> <item2> <item3> rot`     | `<item2> <item3> <item1>`               | Brings the 3rd item to TOS             |
| `nip`   | `<item1> <item2> nip`             | `<item2>`                               | Removes the item below TOS             |
| `tuck`  | `<item1> <item2> tuck`            | `<item2> <item1> <item2>`               | Copy TOS below the second stack item   |
| `2dup`  | `<item1> <item2> 2dup`            | `<item1> <item2> <item1> <item2>`       | Duplicates the top two items           |

As well as some lower level stack manipulations:

| Word    | Usage                             | Result                                  | Comment                                |
| ------- | --------------------------------- | --------------------------------------- | -------------------------------------- |
| `pick`  | `<int> pick`                      | `<item>`                                | Duplicates the nth item as TOS         |
| `roll`  | `<int> roll`                      | `<item>`                                | Moves the nth item to TOS              |
| `depth` | `depth`                           | `<int>`                                 | Number of items on stack placed as TOS |
| `drop`  | `<item> drop`                     |                                         | Removes the item at TOS                |
| `wipe`  | `<int> wipe`                      |                                         | Removes N item at TOS                  |

### `debug`

Provides some functionality for debugging and examining the state of the stack.

| Word         | Usage                             | Result | Comment                                           |
| ------------ | --------------------------------- | ------ | ------------------------------------------------- |
| `dump-long`  | `... dump-long`                   | `...`  | Reports the contents of the stack in a long form  |
| `dump-short` | `... dump-short`                  | `...`  | Reports the contents of the stack in a short form |
| `dump`       | `... dump`                        | `...`  | Rewritable word that defaults to dump-long        |
| `trace`      | `<flags> trace`                   |        | Sets the trace flags (see below)                  |

The dump words are just convenience functionality for giving a summary of the
current contents of the stack.

The trace words provide a means to provide information about the stack. This is
specified as a numeric value which is the sum of any of the individual flags
below:

| Value | Description                                                                       |
| ----- | --------------------------------------------------------------------------------- |
| 1     | Reports all tokens as they are pushed to the stack for evaluation                 |
| 2     | Reports all data which is appended to the stack                                   |
| 4     | Reports definitions of words as they are created                                  |
| 8     | Indicates the depth of the parsed tokens (as in how many stack frames are active) |
| 16    | Reports on optimisations associated to loop unrolling                             |
| 32    | Reports all executed words in every iteration of a loop                           |

### `math`

Provides basic math.

| Word     | Usage                      | Result     | Comment            |
| -------- | -------------------------- | ---------- | ------------------ |
| `**`     | `<number> <number> **`     | `<number>` | Power              |
| `abs`    | `<number> abs`             | `<number>` | Abs                |
| `floor`  | `<number> floor`           | `<number>` | floor              |
| `ceil`   | `<number> ceil`            | `<number>` | ceil               |
| `round`  | `<number> round`           | `<number>` | round              |
| `negate` | `<number> negate`          | `<number>` | Negate             |
| `sin`    | `<number> sin`             | `<number>` | Sine               |
| `cos`    | `<number> cos`             | `<number>` | Cosine             |
| `tan`    | `<number> tan`             | `<number>` | Tan                |
| `asin`   | `<number> asin`            | `<number>` | Arc sine           |
| `acos`   | `<number> acos`            | `<number>` | Arc cosine         |
| `atan`   | `<number> atan`            | `<number>` | Arc tan            |
| `ln`     | `<number> ln`              | `<number>` | Natural logarithm  |
| `exp`    | `<number> exp`             | `<number>` | Base-e exponential |
| `pi`     | `pi`                       | `<number>` | pi                 |
| `e`      | `e`                        | `<number>` | e                  |
| `rand`   | `rand`                     | `<number>` | Random int         |

### `rational`

Provides basic rational number arithmetic. Rationals are entered into the system as strings of
the format `<int>[:<int>]` and can be freely mixed with normal mathematical words introduced
above. The type `<rational>` in the following table can be thought of as anything of the form
`<int>:<int>` or `<int>` or any numeric value which lacks a fractional part.

| Word       | Usage                      | Result             | Comment                                   |
| ---------- | -------------------------- | ------------------ | ----------------------------------------- |
| `rational` | `<string> rational`        | `<rational>`       | Forces a coercion from string to rational |
| `r+`       | `<rational> <rational> r+` | `<rational>`       | Adds two rationals                        |
| `r-`       | `<rational> <rational> r-` | `<rational>`       | Subtracts two rationals                   |
| `r*`       | `<rational> <rational> r*` | `<rational>`       | Multiplies two rationals                  |
| `r/`       | `<rational> <rational> r/` | `<rational>`       | Divides two rationals                     |
| `r-num`    | `<rational> r-num`         | `<rational> <int>` | Extracts the numerator                    |
| `r-den`    | `<rational> r-den`         | `<rational> <int>` | Extracts the denominator                  |

Some examples of use:

```
> 1920 1080 r/ .
16:9
> 576 16:9 r* .
1024:1
> 1024 720 r/ .
64:45
> 720 64:45 * .
1024
> 720 64:45 r* 576 r/ .
16:9
```

### `strings`

Provides string manipulation functionality.

| Word          | Usage                            | Result           | Comment                                                 |
| ------------- | -------------------------------- | ---------------- | ------------------------------------------------------- |
| `str-len`     | `<str> str-len`                  | `<str> <int>`    | Puts the string length as TOS                           |
| `str-cmp`     | `<str1> <str2> str-cmp`          | `<int>`          | Compares strings                                        |
| `str-sub`     | `<str> <start> <end> str-sub`    | `<substr>`       | Puts the substring at the TOS                           |
| `str-find`    | `<str1> <str2> str-find`         | `<str1> <pos>`   | Puts the position of second string in first at tos      |
| `str-find-r`  | `<str1> <str2> str-find-r`       | `<str1> <pos>`   | Puts the last position of second string in first at tos |
| `str-ifind`   | `<str1> <str2> str-ifind`        | `<str1> <pos>`   | Puts the position of second string in first at tos      |
| `str-ifind-r` | `<str1> <str2> str-find-r`       | `<str1> <pos>`   | Puts the last position of second string in first at tos |
| `str-npos`    | `str-npos`                       | `<pos>`          | Value pushed when strings not found                     |
| `str-at`      | `<str> <pos> str-at`             | `<str> <char>`   | Places the char the specified pos at tos                |

### `lists`

Provides some basic 'list' manipulations.

A list is loosely defined as a number of items immediately followed by the
number of items. Hence, a stack containing 'a b c 3' would be seen as a list
of 3 items.

Adding an item to a list is trivially achieved by: `<list> <item> swap 1 +`,
while other manipulations are more involved. The lists vocab attempts to provide
some words for common manipulations.

| Word           | Usage                                | Result          | Comment                                           |
| -------------- | -------------------------------------| --------------- | ------------------------------------------------- |
| `[[`           | `[[ ... ]]`                          | `<list>`        | Puts all strings on stack with the number at TOS  |
| `list-join`    | `<list> <list> list-join`            | `<list>`        | Adds two lists together                           |
| `list-append`  | `<list> <item> list-append`          | `<list>`        | Adds an item to the end of the list               |
| `list-in-list` | `<list> <list> <index> list-in-list` | `<list>`        | Inserts a list to another list at the index given |
| `list-insert`  | `<list> <item> <index> list-insert`  | `<list>`        | Inserts an item into a list at the index given    |
| `list-at`      | `<list> <index> list-at`             | `<list> <item>` | Picks the nth item from the list                  |
| `list-move`    | `<list> <index> list-move`           | `<list> <item>` | Moves the nth item from the list                  |
| `join`         | `<list> join`                        | `<string>`      | Joins and quotes strings into one string          |
| `split`        | `<string> split`                     | `<list>`        | Splits string by spaces into a list               |

### `vector`

Provides another mechanism to handle lists.

Constructors:

| Word              | Usage                   | Result              | Comments                                    |
| ----------------- | ----------------------- | ------------------- | ------------------------------------------- |
| `vector-new`      | `vector-new`            | `<vector>`          | Creates an empty vector                     |
| `list-to-vector`  | `<list> list-to-vector` | `<vector>`          | Creates a new vector with the list contents |
| `vector-clone`    | `<vector> vector-clone` | `<vector> <vector>` | Creates a clone of the first vector         |

Accessors and modifiers:

| Word               | Usage                              | Result            | Comments                                |
| ------------------ | ---------------------------------- | ----------------- | --------------------------------------- |
| `vector-append`    | `<vector> <item> vector-append`    | `<vector>`        | Appends item to vector                  |
| `vector-join`      | `<vector> <vector> vector-join`    | `<vector>`        | Appends top vector on to bottom         |
| `vector-join-list` | `<vector> <list> vector-join-list` | `<vector>`        | Appends top list to vector              |
| `vector-remove`    | `<vector> <index> vector-remove`   | `<vector>`        | Removes the item at the specified index |
| `vector-query`     | `<vector> <index> vector-query`    | `<vector> <item>` | Obtains the item at the specified index |
| `vector-size`      | `<vector> vector-size`             | `<vector> <int>`  | Obtains the vector size                 |
| `vector-describe`  | `<vector> vector-describe`         | `<vector>`        | Prints the vector                       |
| `vector-to-list`   | `<vector> vector-to-list`          | `<list>`          | Converts vector to list                 |

In c++, this is simply implemented as a `std::vector< rp::any >` wrapped in a `std::shared_ptr`.

### `vars`

Provides a means to define, assign and dereference variables.

| Word        | Usage                     | Result    | Comment                                     |
| ----------- | ------------------------- | --------- | ------------------------------------------- |
| `variable`  | `variable <name>`         |           | Creates a new variable with the given name  |
| `variable!` | `<value> variable <name>` |           | Creates a new variable with name and value  |
| `!`         | `<value> <var> !`         |           | Assigns value to variable                   |
| `@`         | `<var> @`                 | `<value>` | Returns value of variable to the stack      |

### `locals`

Provides a means to define and assign local variables.

| Word        | Usage                         | Result    | Comment                                                   |
| ----------- | ----------------------------- | --------- | --------------------------------------------------------- |
| `local`     | `<value> local <name>`        |           | Creates or updates a local variable assigns value at tos  |
| `locals`    | `<value> ... <vector> locals` |           | Creates and assigns multiple local vars                   |

Unlike normal variables, local variables exist while the scope which created
them persists and they are not inherited by substacks.

An example of the usage of `locals` in `vmlshell` would be:

```
> : 3swap { x y z } locals z y x ;
> debug-on
> 1 2 3
'1' '2' '3'
> 3swap
'3' '2' '1'
> 3swap
'1' '2' '3'
```

### `time`

Provides functionality for time related features.

| Word           | Usage                     | Result     | Comment                                                |
| -------------- | ------------------------- | ---------- | ------------------------------------------------------ |
| `sleep`        | `<double> sleep`          |            | Sleeps for the period requested                        |
| `time`         | `time`                    | `<time>`   | Puts an object relating to the current time at TOS     |
| `time-elapsed` | `<time> time-elapsed`     | `<double>` | Puts the number of seconds between now and time at TOS |
| `time-diff`    | `<time> <time> time-diff` | `<double>` | Puts the number of seconds between the times at TOS    |

### `compare`

Provides various comparison methods.

| Word   | Usage                   | Result     | Comment                                         |
| ------ | ----------------------- | ---------- | ----------------------------------------------- |
| `==`   | `<item1> <item2> ==`    | `<bool>`   | Determines if the string values are identical   |
| `!=`   | `<item1> <item2> !=`    | `<bool>`   | Determines if the string values are different   |
| `>`    | `<item1> <item2> >`     | `<bool>`   | Determines if the numeric values are greater    |
| `<`    | `<item1> <item2> <`     | `<bool>`   | Determines if the numeric values are less       |
| `>=`   | `<item1> <item2> >=`    | `<bool>`   | Determines if the numeric values are >=         |
| `<=`   | `<item1> <item2> <=`    | `<bool>`   | Determines if the numeric values are <=         |

### `bools`

Provides various boolean logic operations.

| Word   | Usage                | Result   | Comment        |
| ------ | -------------------- | -------- | -------------- |
| `&&`   | `<bool> <bool> &&`   | `<bool>` | Boolean 'and'  |
| `\|\|` | `<bool> <bool> \|\|` | `<bool>` | Boolean 'or'   |
| `not`  | `<bool> not`         | `<bool>` | Boolean 'not'  |

### `bits`

Provides various bit operations.

| Word | Usage            | Result  | Comment             |
| ---- | ---------------- | ------- | ------------------- |
| `<<` | `<int> <int> <<` | `<int>` | Bitwise shift left  |
| `>>` | `<int> <int> >>` | `<int>` | Bitwise shift right |
| `&`  | `<int> <int> &`  | `<int>` | Bitwise 'and'       |
| `\|` | `<int> <int> \|` | `<int>` | Bitwise 'or'        |
| `^`  | `<int> <int> ^`  | `<int>` | Bitwise 'xor'       |
| `~`  | `<int> ~`        | `<int>` | Bitwise complement  |

### `if`

Provides conditional branching.

| Word   | Usage                                      | Comment                             |
| ------ | ------------------------------------------ | ----------------------------------- |
| `if`   | `<bool> if <true> then`                    | Runs `<true>` if bool was true      |
| `if`   | `<bool> if <true> else <false> then`       | Runs either `<true>` or `<false>`   |

Alternatively:

| Word   | Usage                                      | Comment                             |
| ------ | ------------------------------------------ | ----------------------------------- |
| `?{`   | `<bool> ?{ <true> }`                       | Runs `<true>` if bool was true      |
| `?{`   | `<bool> ?{ <true> }:{ <false> }`           | Runs either `<true>` or `<false>`   |

### `cases`

Provides a means to have more complex conditional branching.

| Word   | Usage                                                                             |
| ------ | --------------------------------------------------------------------------------- |
| `&{`   | `<value> &{ [ +> <condition> {{ <tokens> }} ] * [ [ -> \| *> {{ <tokens> }} ] &}` |

This provides a switch/case type statement - an example of which is:

```
: test
	&{
	+> 0 == {{ $ "yay" }}
	+> 0 <  {{ $ "too low" }}
	+> 0 >  {{ $ "too high" }}
	&}
;
```

Being a word which consumes the tos, and pushes it back to the stack for each case
`<condition>`. These `<condition>` should leave a bool value at the of the stack -
the first which gives true has their associated `<tokens>` executed.

There is a special case `*>` which can be used to specify a sequence of tokens to
execute if no matches are found. The value is pushed to the stack before execution.
Alternatively, `->` gives a default clause which doesn't push item back.

A more condensed form of the same is:

```
: test &{ +> 0 == {{ $ "yay" }} +> 0 < {{ $ "too low" }} +> 0 > {{ $ "too high" }} &} ;
```

In this case:

* `0 test` will put 'yay' on the stack
* `-1 test` will put 'too low' on the stack
* `1 test` will put 'too high' on the stack

### `loops`

Provides looping until a condition or infinitely.

| Word     | Usage                              | Comment                              |
| -------- | ---------------------------------- | ------------------------------------ |
| `begin`  | `begin <body> until`               | Runs `<body>` until it provides true |
| `begin`  | `begin <body> again`               | Runs `<body>` forever                |
| `begin`  | `begin <cond> while <body> repeat` | Runs `<body>` while `<cond>` is true |
| `leave`  | `leave`                            | Exits the current inner loop         |

Provides looping over a numeric range.

| Word     | Usage                             | Comment                                                      |
| -------- | --------------------------------- | ------------------------------------------------------------ |
| `do`     | `<end> <start> do <body> loop`    | Runs `<body>` n times where n is abs( end - start )          |
| `do`     | `<end> <start> do <body> +loop`   | Runs `<body>` n times and increment is specified by `<body>` |
| `leave`  | `leave`                           | Exits the current inner loop                                 |
| `loops?` | `loops?`                          | Pushes the number of active loops to the stack               |
| `index?` | `<loop> index?`                   | Pushes the index value of the requested loop to the stack    |
| `i`      | `i`                               | Pushes the current index of the inner loop to the stack      |
| `j`      | `j`                               | Pushes the current index of the outer loop to the stack      |
| `k`      | `k`                               | Pushes the current index of the 2nd outer loop to the stack  |

Provides iteration over a `list` or `vector` of `<any>` objects:

| Word     | Usage                             | Comment                                                      |
| -------- | --------------------------------- | ------------------------------------------------------------ |
| `for`    | `<list> for <body> next`          | Runs `<body>` for each item in the list                      |

For example:

```
$ rpany [[ 1 2 3 ]] for i , space next cr
1 2 3
$ rpany [[ 3 4 5 ]] for [[ 6 7 8 ]] for i j "*" , space next cr next
18 21 24
24 28 32
30 35 40
```

### `eval`

Provides a means to parse tokens out of a string.

| Word         | Usage                 | Comment                             |
| ------------ | --------------------- | ----------------------------------- |
| `eval`       | `<string> eval`       | Parses and evaluates the string     |
| `eval-word?` | `<string> eval-word?` | Evaluates the word if it exists     |
| `eval-file`  | `<string> eval-file`  | Attempts to evaluate the file given |
| `eval-list`  | `<list> eval-list`    | Attempts to evaluate the list given |

### `rstack`

Provides return stack functionality.

| Word         | Usage            | Result   | Comment                                           |
| ------------ | ---------------- | ---------|-------------------------------------------------- |
| `>r`         | `<item> >r`      |          | Moves item to return stack                        |
| `r>`         | `r>`             | `<item>` | Moves item from return stack                      |
| `r@`         | `r@`             | `<item>` | Duplicates item on top of return stack            |
| `rdepth`     | `rdepth`         | `<int>`  | Indicates the number of items on the return stack |
| `rclear`     | `rclear`         |          | Clears the contents of the return stack           |
| `>r-list`    | `.. <n> >r-list` |          | Moves list to return stack                        |
| `r-list>`    | `.. <n> >r-list` | `.. <n>` | Moves list from return stack                      |

### `substack`

Provides mechanisms to create and interact with substacks.

Constructors:

| Word               | Usage                   | Result      | Comment                                     |
| ------------------ | ----------------------- | ----------- | ------------------------------------------- |
| `ss-new`           | `ss-new`                | `<ss>`      | New stack                                   |
| `ss-clone`         | `ss-clone`              | `<ss>`      | New stack with same dictionary as self      |
| `ss-inherit`       | `<ss> ss-inherit`       | `<ss> <ss>` | New stack shares data                       |
| `ss-self`          | `ss-self`               | `<ss>`      | New stack shares data with self             |
| `ss-self-educated` | `<ss> ss-self-educated` | `<ss> <ss>` | New stack shares data and dict from self    |

Methods:

| Word               | Usage                         | Result         | Comment                                     |
| ------------------ | ----------------------------- | -------------- | ------------------------------------------- |
| `ss-educate`       | `<ss> <string> ss-educate`    | `<ss>`         | Educates the stack with the specified vocab |
| `ss-educate-all`   | `<ss> ss-educate-all`         | `<ss>`         | Educates the stack will all known vocabs    |
| `ss-ready`         | `<ss> ss-ready`               | `<ss> <bool>`  | Indicates if the stack can execute tokens   |
| `ss-eval`          | `<ss> <string> ss-eval`       | `<ss>`         | Evaluate string on the stack                |
| `?ss-eval-word`    | `<ss> <string> ?ss-eval-word` | `<ss>`         | Evaluate word on the stack if it exists     |

### `words`

Provides read only access to the dictionary.

| Word          | Usage                   | Result     | Comment                                            |
| ------------- | ----------------------- | ---------- | -------------------------------------------------- |
| `vocabs-help` | `vocabs-help`           |            | Displays the complete dictionary                   |
| `vocab-help`  | `vocab-help <vocab>`    |            | Displays dictionary of a specific vocab            |
| `vocab-list`  | `vocab-list <vocab>`    | `<list>`   | Provides a list of all of the words in the vocab   |
| `vocab-see`   | `vocab-see <vocab>`     |            | Displays the definitions of all words in the vocab |
| `words`       | `words`                 |            | Displays all available words as a long list        |

### `word`

Provides a means to define words.

| Word        | Usage                   | Result     | Comment                                |
| ----------- | ----------------------- | ---------- | -------------------------------------- |
| `:`         | `: <word> <def> ;`      |            | Defines a new word as 'word'           |
| `forget`    | `forget <word>`         |            | Removes 'word'                         |
| `see`       | `see <word>`            |            | Outputs the definition of 'word'       |
| `find-name` | `$ <word> find-name`    | `<bool>`   | Tests if the requested word exists     |

In the general case, the expansion of a word is deferred until execution, however,
this means that all parser words must be satisified in the definition.

In other words, this is valid:

```
: something if $ hello else $ bye then ;
```

since the 'if' usage is fully defined by the following tokens in the definition.

This, however, is invalid:

```
: fi then ;
: something if $ hello else $ bye fi ;
```

There are a few issues with this, but the major one is that 'if' cannot be closed
as the 'then' token is never received. Any attempt to use any part of a parser
in its own word will also fail.

To work around this, rpany provides an `inline` word which can be used as:

```
: fi inline then ;
: something if $ hello else $ bye fi ;
```

In this case, the inline definition immediately replaces the token, thus, the
parser receives 'then'.

This has the impact of storing the defintion of 'something' as the expanded
definition. Thus, when you run:

```
see something
```

The output is:

```
: something if $ hello else $ bye then ;
```

### `parse`

Provides a mechanism to create 'prefix' words - ie: words which take their input
from the next token(s) rather than the stack.

| Word        | Usage                   | Comment                                |
| ----------- | ----------------------- | -------------------------------------- |
| `parse`     | `parse <exec>`          | Execute exec on the following token    |

To use this, you can create a new inline word like:

```
> : print inline parse . ;
> print "hello world"
hello world
```

In this case the `parse` word consumes 2 tokens - the first of those being the
`.` and the second being the next token in the input. When executed, the second
token is appended to the data stack, and the first is executed as a word.

While that is generally all you need, there's an additional syntax for more
complex inline use using `[[` and `]]`.

```
> : quote inline parse [[ f. "'%s'" ]] ;
> quote "hello world"
'hello world'
```

This is ultimately the same, except that the parse word accumulates a list
instead. This isn't necessary, as you can always define a new word and use the
first form, but is provided as a courtesy to avoid namespace pollution.

This arrangement means that all of these are equivalent:

```
: print inline parse . ;
: print-worker . ;
: print inline parse print-worker ;
: print inline parse [[ . ]] ;
: print inline parse [[ [[ . ]] eval-list ]] ;
```

Additionally, a no-op is effectively equivalent to a `$`:

```
: $ inline parse [[ ]] ;
```
[FORTH]: https://en.wikipedia.org/wiki/Forth_(programming_language)
[RPN]: https://en.wikipedia.org/wiki/Reverse_Polish_notation
[REPL]: https://en.wikipedia.org/wiki/Read%E2%80%93eval%E2%80%93print_loop
[std::any]: https://en.cppreference.com/w/cpp/utility/any
[boost::any]: https://www.boost.org/doc/libs/1_69_0/doc/html/any.html
[cmake]: https://cmake.org/
[boost::format]: https://www.boost.org/doc/libs/1_69_0/libs/format/
[boost::lexical_cast]: https://www.boost.org/doc/libs/1_69_0/doc/html/boost_lexical_cast.html
[boost::rational]: https://www.boost.org/doc/libs/1_69_0/libs/rational/
[vml-batch]: https://gitlab.com/lilo_booter/vml-batch/blob/master/README.md
[vml-batch vocabs]: https://gitlab.com/lilo_booter/vml-batch/blob/master/README.md#vocabularies
[rpany-linenoise-ng]: https://gitlab.com/lilo_booter/rpany-linenoise-ng/blob/master/README.md
[rpany-comms]: https://gitlab.com/lilo_booter/rpany-comms/blob/master/README.md
[rpany-pi]: https://gitlab.com/lilo_booter/rpany-pi/blob/master/README.md
