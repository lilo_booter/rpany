#pragma once

// All header files used from external and c++ stdlib are loaded here
#if __cplusplus >= 201703L

#include <any>
#include <boost/format.hpp>

namespace rp
{
	using std::any;
	using std::any_cast;
	using boost::format;
};

#define ANY_EMPTY( any ) !any.has_value( )
#define ANY_EMPLACE( any, value, type ) any.emplace< type >( value )

#else

#include <boost/any.hpp>
#include <boost/format.hpp>

namespace rp = boost;
#define ANY_EMPTY( any ) any.empty( )
#define ANY_EMPLACE( any, value, type ) any = value

#endif

#include <boost/rational.hpp>

#include <memory>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <deque>
#include <map>
#include <unordered_map>
#include <vector>
#include <set>
#include <stdexcept>
#include <typeinfo>
#include <typeindex>
#include <iomanip>
#include <cstdlib>
#include <ctime>
#include <thread>
#include <mutex>
#include <chrono>
#include <inttypes.h>
#include <sys/stat.h>
#include <signal.h>
#include <cctype>
#include <cmath>
#include <regex>

#if defined( WIN32 ) || defined( _WIN32 )
#include <io.h>
#endif
