#pragma once

// Forward declaration of the stack classes
class stack;

// Typedef for shared_ptr of parser objects
class parser;
typedef std::shared_ptr< parser > parser_ptr;

// Public types
typedef void ( * word_function )( stack & );
typedef void ( * word_parser )( stack &, const std::string & );
typedef std::vector< rp::any > word_list;

// Type used by vector vocab
typedef std::vector< rp::any > vector_type;
typedef std::shared_ptr< vector_type > vector_ptr;

// Rational types
typedef boost::rational< intmax_t > rational;

// Exceptions
struct stack_exception : public std::exception
{
	stack_exception( const std::string &msg ) : msg_( msg ) { }
	const char *what( ) const throw ( ) { return msg_.c_str( ); }
	std::string msg_;
};

#define RPANY_EXCEPTION( name, msg ) struct name : public stack_exception { name( const std::string &m = msg ) : stack_exception( m ) { } }

RPANY_EXCEPTION( overflow_exception, "overflow" );
RPANY_EXCEPTION( underflow_exception, "underflow" );
RPANY_EXCEPTION( leave_exception, "leave" );
RPANY_EXCEPTION( coercion_exception, "coercion" );
RPANY_EXCEPTION( undefined_exception, "undefined" );
RPANY_EXCEPTION( invalid_exception, "invalid" );
RPANY_EXCEPTION( interrupted_exception, "interrupted" );
RPANY_EXCEPTION( security_exception, "security" );

#undef RPANY_EXCEPTION

extern bool exception_trace;

#define _RPANY_THROW( name, msg ) \
	do { \
		std::ostringstream _st; \
		_st << #name; \
		if ( std::string( msg ) != "" ) _st << ": " << ( msg ); \
		if ( rpany::exception_trace ) _st << " (" << __FILE__ << ":" << __LINE__ << ")"; \
		throw( name( _st.str( ) ) ); \
	} while( 0 )

#define RPANY_THROW_DEFAULT( name ) _RPANY_THROW( name, "" )
#define RPANY_THROW( name, msg ) _RPANY_THROW( name, msg )
#define RPANY_THROW_IF( condition, name, msg ) if ( condition ) _RPANY_THROW( name, msg )

#define RPANY_COMPILE_ONLY( args, word ) word, [ ]( args ) { RPANY_THROW( stack_exception, "Interpreting a compile only word '" + std::string( word ) + "'" ); }

// General check to see if any is a specific type
template < typename T >
bool is( const rp::any &any )
{
	return any.type( ) == typeid( T );
}

// Attempt to obtain the type
template < typename T >
T as( const rp::any &any )
{
	return rp::any_cast< T >( any );
}

// Obtain any as string if it is a string (ie: no coercion)
inline std::string if_string( const rp::any &any )
{
	return is< std::string >( any ) ? as< std::string >( any ) : "";
}

inline bool is_quoted( const std::string &s )
{
	const std::string quotes( "\"'" );
	return s.size( ) > 1 && s[ 0 ] == s[ s.size( ) - 1 ] && quotes.find( s[ 0 ] ) != std::string::npos;
}

inline std::string unquote( const std::string &s )
{
	return is_quoted( s ) ? s.substr( 1, s.size( ) - 2 ) : s;
}

inline std::string quoted( const std::string &s )
{
	if ( s.empty( ) || ( !is_quoted( s ) && s.find( ' ' ) != std::string::npos ) )
		return '"' + s + '"';
	return s;
}

inline int quotes( const std::string &token )
{
	int quotes = 0;
	char qchar = token.size( ) > 0 && ( token[ 0 ] == '\'' || token[ 0 ] == '\"' ) ? token[ 0 ] : '\"';
	for ( size_t i = token.find( qchar ); i != std::string::npos; quotes ++ )
	{
		i = token.find( qchar, i + 1 );
		if ( i != std::string::npos && token[ i - 1 ] == '\\' ) quotes --;
	}
	return quotes;
}

inline std::string tokenise( std::istringstream &iss )
{
	std::string token;
	iss >> std::skipws >> token;
	while ( iss.good( ) && quotes( token ) % 2 != 0 )
	{
		char t;
		iss >> std::noskipws >> t;
		token += t;
	}
	return token;
}

inline bool is_tty( )
{
#if defined( WIN32 ) || defined( _WIN32 )
	return _isatty( _fileno( stdin ) ) != 0;
#else
	return isatty( fileno( stdin ) );
#endif
}

extern volatile int interrupt_count;

inline void default_signal_handler( int )
{
	if ( interrupt_count ++ > 0 )
	{
		std::cerr << std::endl << "Process interrupted" << std::endl;
		exit( 1 );
	}
}

inline bool is_interrupted( )
{
	return interrupt_count > 0;
}

inline void reset_interrupt_state( )
{
	interrupt_count = 0;
}

#if defined( WIN32 ) || defined( _WIN32 )
extern bool windows_escape_sequences( );
extern BOOL WINAPI windows_signal_handler( DWORD type );
#endif

inline void register_signal_handler( )
{
#if defined( WIN32 ) || defined( _WIN32 )
	windows_escape_sequences( );
	SetConsoleCtrlHandler( windows_signal_handler, TRUE );
#else
	signal( SIGINT, default_signal_handler );
#endif
}
