#pragma once

namespace vars
{
	// Helper type for variables
	class variable
	{
		public:
			variable( const std::string &name_, const rp::any &value_ = rp::any( ) )
			: name( name_ )
			, value( value_ )
			{ }

			const std::string name;
			rp::any value;
	};

	typedef std::shared_ptr< variable > var_ptr;
}

class variables
{
	public:
		typedef std::map< std::string, rp::any > variables_type;

		rp::any &operator[ ] ( const std::string &name )
		{
			return variables_[ name ];
		}

		// Provides a begin iterator for the variables
		variables_type::const_iterator vbegin( ) const
		{
			return variables_.begin( );
		}

		// Provides an end iterator for the variables
		variables_type::const_iterator vend( ) const
		{
			return variables_.end( );
		}

		// Provides a find iterator for the name in the variables
		variables_type::const_iterator vfind( const std::string &name ) const
		{
			return variables_.find( name );
		}

		void verase( const std::string &name )
		{
			auto iter = variables_.find( name );
			if ( iter != variables_.end( ) )
				variables_.erase( iter );
		}

		void reset_variables( )
		{
			variables_ = { };
		}

	private:
		variables_type variables_;
};
