#pragma once

template< typename T >
class state_template
{
	public:
		void push( const T &value ) { s.push_back( value ); }
		T &pick( const size_t &index = 0 ) { if ( index >= s.size( ) ) RPANY_THROW( stack_exception, "State index request out of range" );  return s[ s.size( ) - index - 1 ]; };
		T *ptr( const size_t &index = 0 ) { if ( index >= s.size( ) ) RPANY_THROW( stack_exception, "State index request out of range" );  return &( s[ s.size( ) - index - 1 ] ); };
		void pop( ) { s.pop_back( ); }
		size_t size( ) const { return s.size( ); }
		void clear( ) { s.clear( ); }
	private:
		std::deque< T > s;
};

template< typename T >
class state_instance
{
	public:
		typedef state_template< T > state_type;
		state_instance( state_type &state_, const T &value ) : state( state_ ) { state.push( value ); }
		T &pick( ) { return state.pick( ); }
		T *ptr( ) { return state.ptr( ); }
		~state_instance( ) { state.pop( ); }
	private:
		state_type &state;
};
