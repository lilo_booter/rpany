#pragma once

inline int eval( stack &s, const std::string &input, bool rethrow = false )
{
	std::string token;

	try
	{
		std::istringstream iss( input );
		while( iss.good( ) )
		{
			token = tokenise( iss );
			if ( token.empty( ) || token[ 0 ] == '#' ) break;
			if ( is_quoted( token ) )
				s << unquote( token );
			else
				s << token;
		}
		s.execute_on_eol( );
	}
	catch( const interrupted_exception & )
	{
		if ( rethrow ) throw;
		reset_interrupt_state( );
	}
	catch( const std::exception &e )
	{
		// Report the error or rethrow
		if ( rethrow ) throw;
		s.error( ) << "ERROR: " << e.what( ) << " (" << token << ")" << std::endl;
		return 1;
	}

	return 0;
}

inline int eval( stack &s, std::istream &stream, bool rethrow = false )
{
	int result = 0;
	std::string line;
	int index;

	for( index = 0; !s.closed( ) && result == 0 && std::getline( stream, line ); index ++ )
		result = eval( s, line, rethrow );

	if ( result != 0 )
		RPANY_THROW( stack_exception, ( rp::format( "Failed at line %d" ) % index ).str( ) );

	return result;
}

inline void run_main( stack &s, std::string &token, int argc, char **argv, int index )
{
	const std::string arg_word = "<<>>";
	const std::string switch_word = "__switches__";

	if ( s.is_word( switch_word ) )
	{
		s << "__switches__";

		if ( s.is_parsing( ) )
			RPANY_THROW( stack_exception, "Script in parsing state after switches" );

		bool has_arg_word = s.is_word( arg_word );

		for( ; index < argc; index ++ )
		{
			token = argv[ index ];

			const bool is_switch = token.find( "-" ) == 0;

			if ( is_switch )
			{
				const auto has_value = token.find( "=" );

				if ( has_value != std::string::npos )
				{
					s += token.substr( has_value + 1 );
					s << token.substr( 0, has_value + 1 );
				}
				else
				{
					s << token;
				}
			}
			else
			{
				s += token;
				if ( has_arg_word ) s << arg_word;
			}
		}
	}
	else
	{
		bool has_arg_word = s.is_word( arg_word );

		for( ; index < argc; index ++ )
		{
			token = argv[ index ];
			s += token;
			if ( has_arg_word ) s << arg_word;
		}
	}

	if ( s.is_parsing( ) )
		RPANY_THROW( stack_exception, "Script in parsing state after arg handling" );

	if ( s.is_word( arg_word ) )
		s.forget( arg_word );

	token = "__main__";
	s << token;
}

inline bool is_script( std::istream &stream )
{
	std::string line;
	if ( !!stream && std::getline( stream, line ) && line.substr( 0, 2 ) == "#!" )
	{
		enrollment_type tools = enrolled( );
		for ( auto iter = tools.begin( ); iter != tools.end( ); iter ++ )
			if ( line.find( *iter ) != std::string::npos )
				return true;
	}
	return false;
}

// Provides a callback mechanism to allow hosting applications to override the normal
// command line argument handling.

typedef bool ( *arg_handler_func )( rpany::stack &s, int argc, char **argv, int &index );

// Command line arguments have a special case - they accept a file before
// tokens.
//
// To avoid potential ambiguities, the first argument can be specified as a --
// to indicate that no check should be made.
//
// Hence, rpany accepts the following arguments:
//
// rpany [ file | -- ] [ token ]*
//
// Only the first argument is identified as a file. If it is unidentified, it
// will be regarded as a token.
//
// Failure to be handled as either will be treated as an error and will cause
// termination.
//
// When the file contains a __main__ word, arguments are not evaluated, but
// simply appended to the stack.

inline int eval( stack &s, int argc, char **argv, int start = 0, arg_handler_func handler = 0 )
{
	std::string token;

	// Return immediately if no arguments are given
	if ( start == argc )
		return 0;

	try
	{
		int index = start;

		token = argv[ start ];
		std::ifstream stream( token );

		if ( token == "--" )
			index ++;
		else if ( is_script( stream ) && eval( s, stream ) == 0 )
			index ++;

		if ( s.is_word( std::string( "__main__" ) ) )
		{
			run_main( s, token, argc, argv, index );
		}
		else
		{
			while( index < argc )
			{
				if ( handler && !s.is_parsing( ) )
					if ( handler( s, argc, argv, index ) )
						continue;

				token = argv[ index ++ ];
				s << token;
			}
		}

		if ( s.is_parsing( ) )
			RPANY_THROW( stack_exception, "Script in parsing state after arg handling" );
	}
	catch( const leave_exception & )
	{
		return 0;
	}
	catch( const interrupted_exception & )
	{
		return 1;
	}
	catch( const std::exception &e )
	{
		s.error( ) << "ERROR: " << e.what( ) << " (" << token << ")" << std::endl;
		return 1;
	}
	return 0;
}
