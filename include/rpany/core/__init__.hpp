#pragma once

namespace rpany {
#include <rpany/core/base.hpp>
#include <rpany/core/numeric.hpp>
#include <rpany/core/registry.hpp>
#include <rpany/core/coerce.hpp>
#include <rpany/core/state.hpp>
#include <rpany/core/stack.hpp>
#include <rpany/core/eval.hpp>
#include <rpany/core/consts.hpp>
#include <rpany/core/substack.hpp>
#include <rpany/core/parsers.hpp>
#include <rpany/core/factory.hpp>
}
