#pragma once

// Coercion Subsystem
//
// Attempts to coerce the any argument received to an any of the requested type T.
//
// Example:
//
// rp::any i = 5;
// rp::any d = coerce< double >( i );
// std::string s = convert< std::string >( d );
//
// In order to make this efficicient, the coercion mappings are held in map of
// pairs which are specified as { from, to } (eg: { int, double }). 
//
// The value of each entry holds a function which does the conversion. 

typedef std::pair< std::type_index, std::type_index > coercion_key;

struct pair_hash
{
	template < class T1, class T2 >
	std::size_t operator( ) ( const std::pair< T1, T2 > &pair) const
	{
		return std::hash< T1 >( )( pair.first ) ^ std::hash< T2 >( )( pair.second );
	}
};

typedef rp::any ( *coercion_function )( const rp::any & );
typedef std::unordered_map< const coercion_key, coercion_function, pair_hash > coercion_map;

extern coercion_map coercions;
extern std::mutex mutex;

// rp::any coerce< T >( const rp::any &a )

template < typename T >
inline rp::any coerce( const rp::any &a )
{
	if ( is< T >( a ) )
		return a;

	coercion_key key( typeid( T ), a.type( ) );

	std::lock_guard< std::mutex > guard( mutex );
	if ( coercions.find( key ) != coercions.end( ) )
		return coercions[ key ]( a );

	RPANY_THROW( coercion_exception, "No coercion found" );
}

// T convert< T >( const rp::any &a )

template < typename T >
inline T convert( const rp::any &a )
{
	if ( is< T >( a ) )
		return as< T >( a );

	return as< T >( coerce< T >( a ) );
}

// General non-throwing way to attempt to get a string representation
inline const std::string as_string( const rp::any &any )
{
	std::string result;

	try
	{
		if ( ANY_EMPTY( any ) )
			result = "<unnassigned>";
		else
			result = convert< std::string >( any );
	}
	catch( ... )
	{
		result = "<non-serialisable>";
	}

	return result;
}

// lexical_cast is no good for double -> string conversion

template < typename T >
inline rp::any convert_to_string( const rp::any &a )
{
	std::ostringstream stream;
	stream.precision( 15 );
	stream << as< T >( a );
	return stream.str( );
}

inline rp::any convert_string_to_rational( const rp::any &a )
{
	char *p = 0;
	const std::string &value = as< std::string >( a );
	const intmax_t numerator = strtoimax( value.c_str( ), &p, 0 );
	const intmax_t denominator = *p == ':' ? strtoimax( p + 1, &p, 0 ) : 1;
	if ( *p ) RPANY_THROW( coercion_exception, "Attempt to convert non-rational string '" + value + "'" );
	return rational( numerator, denominator );
}

inline rp::any convert_rational_to_string( const rp::any &a )
{
	const auto &value = as< rational >( a );
	std::ostringstream stream;
	stream << value.numerator( ) << ":" << value.denominator( );
	return stream.str( );
}

template< typename T >
inline rp::any convert_from_rational( const rp::any &a )
{
	const auto &value = as< rational >( a );
	return boost::rational_cast< T >( value );
}

template< typename T >
inline rp::any convert_to_rational( const rp::any &a )
{
	const auto &value = as< T >( a );
	if ( std::floor( value ) != value ) RPANY_THROW( coercion_exception, "Attempt to convert non-integer to rational" );
	return rational( static_cast< long int >( value ) );
}

inline double convert_string_to_double( const std::string &value )
{
	char *p = 0;
	double result = strtod( value.c_str( ), &p );
	if ( *p == ':' && static_cast< int >( result ) == result )
		result = as< double >( convert_from_rational< double >( convert_string_to_rational( value ) ) );
	else if ( *p )
		RPANY_THROW( coercion_exception, "Attempt to convert non-numeric string '" + value + "'" );
	return result;
}

inline rp::any convert_string_to_double( const rp::any &a )
{
	return convert_string_to_double( as< std::string >( a ) );
}

inline rp::any convert_string_to_int( const rp::any &a )
{
	char *p = 0;
	const std::string &value = as< std::string >( a );
	const int result = strtol( value.c_str( ), &p, 0 );
	if ( *p ) RPANY_THROW( coercion_exception, "Attempt to convert non-numeric string '" + value + "'" );
	return result;
}

inline rp::any convert_string_to_size_t( const rp::any &a )
{
	std::istringstream stream( as< std::string >( a ) );
	size_t result;
	stream >> result;
	if ( !stream.eof( ) && !stream.good( ) )
		RPANY_THROW( coercion_exception, "Attempt to convert string '" + as< std::string >( a ) + "' to size_t failed" );
	return result;
}

inline rp::any convert_string_to_bool( const rp::any &a )
{
	const std::string &value = as< std::string >( a );
	bool result = false;
	if ( is_numeric( value ) )
		result = convert_string_to_double( value ) != 0;
	else
		result = value != "";
	return result;
}

inline rp::any convert_double_to_size( const rp::any &a )
{
	return static_cast< size_t >( as< double >( a ) );
}

template< typename T >
inline rp::any convert_numeric_to_bool( const rp::any &a )
{
	return as< T >( a ) != 0;
}

template< typename F, typename T >
inline rp::any convert_by_cast( const rp::any &a )
{
	return static_cast< T >( as< F >( a ) );
}

template< typename TO, typename FROM >
inline TO lexical_cast( const FROM &from )
{
	TO result;
	std::ostringstream ostream;
	ostream << from;
	std::istringstream istream( ostream.str( ) );
	istream >> result;
	if ( !istream.eof( ) )
		RPANY_THROW( coercion_exception, ( rp::format( "Unable to convert %s" ) % ostream.str( ) ).str( ) );
	return result;
}

#define COERCE_CREATE( from, to ) COERCE_KEY( from, to ), COERCE_FUNC( from, to )

	#define COERCE_KEY( from, to ) rpany::coercion_key( COERCE_TYPE( to ), COERCE_TYPE( from ) )
		#define COERCE_TYPE( id ) typeid( id )

	#define COERCE_FUNC( from, to ) [ ]( const rp::any &a ) { return COERCE_CAST( to, COERCE_GET( a, from ) ); }
		#define COERCE_CAST( id, value ) rp::any( COERCE_LEX( id, value ) )
			#define COERCE_LEX( id, value ) lexical_cast< id >( value )
		#define COERCE_GET( a, id ) rp::any_cast< id >( a )

inline void coercion( const coercion_key &key, const coercion_function &function )
{
	std::lock_guard< std::mutex > guard( mutex );
	coercions[ key ] = function;
}

inline coercion_map coercions_create( )
{
	return
	{
		{ COERCE_KEY( std::string, int ), convert_string_to_int },
		{ COERCE_KEY( std::string, double ), convert_string_to_double },
		{ COERCE_KEY( std::string, size_t ), convert_string_to_size_t },
		{ COERCE_KEY( std::string, bool ), convert_string_to_bool },
		{ COERCE_KEY( std::string, rational ), convert_string_to_rational },

		{ COERCE_KEY( rational, std::string ), convert_rational_to_string },
		{ COERCE_KEY( rational, double ), convert_from_rational< double > },
		{ COERCE_KEY( rational, int ), convert_from_rational< int > },
		{ COERCE_KEY( rational, bool ), convert_from_rational< bool > },

		{ COERCE_CREATE( bool, int ) },
		{ COERCE_CREATE( bool, double ) },
		{ COERCE_CREATE( bool, std::string ) },

		{ COERCE_CREATE( int, double ) },
		{ COERCE_CREATE( int, size_t ) },
		{ COERCE_KEY( int, bool ), convert_numeric_to_bool< int > },
		{ COERCE_KEY( int, std::string ), convert_to_string< int > },
		{ COERCE_KEY( int, rational ), convert_to_rational< int > },

		{ COERCE_KEY( size_t, bool ), convert_numeric_to_bool< size_t > },
		{ COERCE_KEY( size_t, std::string ), convert_to_string< size_t > },
		{ COERCE_CREATE( size_t, int ) },
		{ COERCE_CREATE( size_t, double ) },

		{ COERCE_KEY( double, bool ), convert_numeric_to_bool< double > },
		{ COERCE_KEY( double, std::string ), convert_to_string< double > },
		{ COERCE_CREATE( double, int ) },
		{ COERCE_CREATE( double, bool ) },
		{ COERCE_KEY( double, size_t ), convert_double_to_size },
		{ COERCE_KEY( double, rational ), convert_to_rational< double > },

		{ COERCE_KEY( int64_t, bool ), convert_numeric_to_bool< int64_t > },

		{ COERCE_KEY( char, std::string ), convert_to_string< char > },
		{ COERCE_KEY( char, int ), convert_by_cast< char, int > },
		{ COERCE_KEY( int, char ), convert_by_cast< int, char > },

		{ COERCE_KEY( word_function, std::string ), [] ( const rp::any & ) { return rp::any( std::string( "<word_function>" ) ); } },
		{ COERCE_KEY( word_list, std::string ), [] ( const rp::any & ) { return rp::any( std::string( "<word_list>" ) ); } },
		{ COERCE_KEY( parser_ptr, std::string ), [] ( const rp::any & ) { return rp::any( std::string( "<parser_ptr>" ) ); } },
	};
}

template < typename T >
T cast( const rp::any &any )
{
	return convert< T >( any );
}
