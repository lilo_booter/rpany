#pragma once

#include "streams.hpp"
#include "variables.hpp"

namespace word {

enum id_t
{
	is_empty,
	is_function,
	is_parser,
	is_list,
	is_vocab,
	is_inline,
};

struct type
{
	type( )
	: word( "unknown" )
	, id( is_empty )
	{ }

	type( const std::string &vocab_, const std::string &word_, const rp::any &any, id_t id_ )
	: vocab( vocab_ )
	, word( word_ )
	, id( id_ )
	, def( any )
	{
		switch( id )
		{
			case is_empty:
				break;
			case is_function:
				object.function = as< word_function >( any );
				break;
			case is_parser:
				object.parser = as< word_parser >( any );
				break;
			case is_list:
			case is_vocab:
			case is_inline:
				object.list = rp::any_cast< word_list >( &def );
				break;
		}
	}

	std::string vocab;
	std::string word;
	id_t id;
	rp::any def;

	union union_t
	{
		word_function function = 0;
		word_parser parser;
		word_list *list;
	};

	union_t object;
};

// When a :'d word is executed, we create a new frame for local variables
// Local variables remain in scope until the word which defines them exits or
// a new local variable of the same name is created in a subsequent word
typedef std::map< std::string, rp::any > frame_type;
typedef std::deque< frame_type > frame_stack_type;
}

class dictionary : public streams, public variables
{
	public:
		// Internal types
		struct dictionary_type
		{
			typedef std::set< std::string > list;
			typedef std::map< std::string, list > vocab_type;
			typedef std::map< std::string, word::type > lexicon_t;
			typedef vocab_type::const_iterator vocab_iterator;
			lexicon_t lexicon;
			vocab_type vocab;
			std::deque< std::string > current;
		};

		typedef std::shared_ptr< dictionary_type > dictionary_type_ptr;

		dictionary( )
		: dictionary_( dictionary_type_ptr( new dictionary_type ) )
		, lexicon( dictionary_->lexicon )
		{
			frame_stack_.push_back( std::move( word::frame_type( ) ) );
		}

		dictionary( const dictionary &d )
		: dictionary_( d.dictionary_ )
		, lexicon( dictionary_->lexicon )
		{
			frame_stack_.push_back( std::move( word::frame_type( ) ) );
		}

		const dictionary_type_ptr &get_dictionary( ) const
		{
			return dictionary_;
		}

		const word::frame_stack_type &get_all_stack_frames( )
		{
			return frame_stack_;
		}

		word::frame_type &get_stack_frame( )
		{
			return frame_stack_.back( );
		}

		void inherit( const dictionary &d )
		{
			for ( auto iter = d.dictionary_->vocab.begin( ); iter != d.dictionary_->vocab.end( ); iter ++ )
				dictionary_->vocab[ iter->first ] = dictionary_type::list( iter->second );
			for ( auto iter = d.dictionary_->lexicon.begin( ); iter != d.dictionary_->lexicon.end( ); iter ++ )
			{
				auto &type = iter->second;
				lexicon.emplace( type.word, word::type( type.vocab, type.word, type.def, type.id ) );
				inherit_var( d, type.word );
			}
		}

		void inherit( const dictionary &d, const std::string &vocab )
		{
			auto vocab_iter = d.dictionary_->vocab.find( vocab );
			RPANY_THROW_IF( vocab_iter == d.dictionary_->vocab.end( ), undefined_exception, "Vocab '" + vocab + "' not found" );
			dictionary_->vocab[ vocab_iter->first ] = dictionary_type::list( vocab_iter->second );
			for ( auto it = vocab_iter->second.begin( ); it != vocab_iter->second.end( ); it ++ )
			{
				auto find = d.find_in_lexicon( *it );
				RPANY_THROW_IF( find == d.end_of_lexicon( ), undefined_exception, "Word " + *it + " not found" );
				auto &type = find->second;
				lexicon.emplace( type.word, word::type( type.vocab, type.word, type.def, type.id ) );
				inherit_var( d, type.word );
			}
		}

		void inherit_var( const dictionary &d, const std::string &word )
		{
			auto var = d.vfind( word );
			if ( var != d.vend( ) )
			{
				if ( is< vars::var_ptr >( var->second ) )
				{
					auto ptr = as< vars::var_ptr >( var->second );
					( *this )[ word ] = vars::var_ptr( new vars::variable( word, ptr->value ) );
				}
				else
				{
					( *this )[ word ] = var->second;
				}
			}
		}

		virtual void push( const rp::any & ) = 0;

		virtual void append( const rp::any & ) = 0;

		virtual void word_defined( const std::string & ) = 0;

		void vocab_start( const std::string &vocab )
		{
			dictionary_->current.push_back( vocab );
		}

		const std::string &vocab_current( ) const
		{
			const static std::string default_vocab = "user";
			return dictionary_->current.size( ) ? dictionary_->current.back( ) : default_vocab;
		}

		const dictionary_type::lexicon_t &get_lexicon( ) const
		{
			return lexicon;
		}

		void add( const std::string &word, const word::type &entry )
		{
			prepare_word( word );
			dictionary_->vocab[ entry.vocab ].insert( word );
			lexicon.emplace( word, std::move( entry ) );
			word_defined( word );
		}

		void teach( const std::string &word )
		{
			add( word, std::move( word::type( vocab_current( ), word, word_list( ), word::is_empty ) ) );
		}

		void teach( const std::string &word, const word_function &function )
		{
			add( word, std::move( word::type( vocab_current( ), word, function, word::is_function ) ) );
		}

		void teach( const std::string &word, const word_parser &parser )
		{
			add( word, std::move( word::type( vocab_current( ), word, parser, word::is_parser ) ) );
		}

		void teach( const std::string &word, const word_list &list )
		{
			word_list clean;

			for ( auto iter = list.begin( ); iter != list.end( ); iter ++ )
			{
				if ( is< char * >( *iter ) )
					clean.push_back( std::string( as< char * >( *iter ) ) );
				else if ( is< const char * >( *iter ) )
					clean.push_back( std::string( as< const char * >( *iter ) ) );
				else
					clean.push_back( *iter );
			}

			const std::string &first = get< std::string >( clean, "" );

			word::id_t id = word::is_list;

			if ( first == "inline" )
				id = word::is_inline;
			else if ( first == "vocab" )
				id = word::is_vocab;

			prepare_word( word );
			dictionary_->vocab[ vocab_current( ) ].insert( word );
			lexicon.emplace( word, std::move( word::type( vocab_current( ), word, clean, id ) ) );
			word_defined( word );
		}

		void teach( const std::string &word, const char **list )
		{
			word_list body;
			for( int i = 0; list[ i ]; i ++ )
				body.push_back( list[ i ] );
			teach( word, body );
		}

		void vocab_end( )
		{
			dictionary_->current.pop_back( );
		}

		const dictionary_type::vocab_type &vocabs( ) const
		{
			return dictionary_->vocab;
		}

		void forget( const std::string &token, bool clean_vars = true )
		{
			// In case the token is provided as a full word, convert here
			std::string word = extract_word( token );

			// Obtain the lexicon entry
			auto iter = find_in_lexicon( word );
			RPANY_THROW_IF( iter == end_of_lexicon( ), undefined_exception, word + " not found" );

			// Remove from vocab
			remove_from_vocab( iter->second );

			// Remove from the lexicon
			lexicon.erase( iter );

			// Remove any variable associated to the word
			if ( clean_vars )
				verase( word );
		}

		void reset_dictionary( )
		{
		}

		dictionary_type::lexicon_t::iterator start_of_lexicon( ) const
		{
			return lexicon.begin( );
		}

		dictionary_type::lexicon_t::iterator find_in_lexicon( const std::string &token ) const
		{
			std::string word = extract_word( token );
			return lexicon.find( word );
		}

		dictionary_type::lexicon_t::iterator end_of_lexicon( ) const
		{
			return lexicon.end( );
		}

		bool is_word( const rp::any &token ) const
		{
			return token.type( ) == typeid( std::string ) && is_word( rp::any_cast< std::string >( token ) );
		}

		bool is_word( const std::string &token ) const
		{
			return find_in_lexicon( token ) != end_of_lexicon( );
		}

		bool is_vocab( const rp::any &token ) const
		{
			return token.type( ) == typeid( std::string ) && is_vocab( rp::any_cast< std::string >( token ) );
		}

		bool is_vocab( const std::string &token ) const
		{
			const auto iter = find_in_lexicon( token );
			return iter != end_of_lexicon( ) && iter->second.id == word::is_vocab;
		}

		bool is_inline( const rp::any &token ) const
		{
			return token.type( ) == typeid( std::string ) && is_inline( rp::any_cast< std::string >( token ) );
		}

		bool is_inline( const std::string &token ) const
		{
			const auto iter = find_in_lexicon( token );
			return iter != end_of_lexicon( ) && iter->second.id == word::is_inline;
		}

		bool is_local_var( const rp::any &token, rp::any &value ) const
		{
			return token.type( ) == typeid( std::string ) && is_local_var( rp::any_cast< std::string >( token ), value );
		}

		bool is_local_var( const std::string &token, rp::any &value ) const
		{
			for( auto iter = frame_stack_.rbegin( ); iter != frame_stack_.rend( ); iter ++ )
			{
				auto viter = iter->find( token );
				if ( viter != iter->end( ) )
				{
					value = viter->second;
					return true;
				}
			}
			return false;
		}

		template < typename T >
		T get( const word_list &list, const T &def, size_t index = 0 ) const
		{
			return index < list.size( ) && is< T >( list.at( index ) ) ? as< T >( list.at( index ) ) : T( def );
		}

		bool is_parser( const rp::any &token ) const
		{
			bool result = false;
			if ( is< std::string >( token ) )
			{
				const auto iter = find_in_lexicon( as< std::string >( token ) );
				result = iter != end_of_lexicon( ) && iter->second.id == word::is_parser;
			}
			return result;
		}

		bool start_parser( const rp::any &token )
		{
			bool result = is_parser( token );
			if ( result )
			{
				const std::string str = as< std::string >( token );
				auto iter = find_in_lexicon( str );
				auto &word = iter->second;
				run_word( word.object.parser, str );
			}
			return result;
		}

		bool inline_parser( word_list &list, const rp::any &token )
		{
			bool result = is_inline( token );
			if ( result )
			{
				auto iter = find_in_lexicon( as< std::string >( token ) );
				auto &word = iter->second;
				const auto &definition = *( word.object.list );
				list.insert( list.end( ), ++ definition.begin( ), definition.end( ) );
			}
			else
			{
				result = start_parser( token );
			}
			return result;
		}

		word_list optimise( const word_list &list )
		{
			word_list result;

			if ( trace( ) & 16 )
				trace_list( "ante", list );

			for ( auto iter = list.begin( ); iter != list.end( ); iter ++ )
			{
				const bool is_string = is< std::string >( *iter );
				if ( is_string && is_rational( as< std::string >( *iter ) ) )
					result.push_back( convert< rational >( *iter ) );
				else if ( is_string && is_numeric( as< std::string >( *iter ) ) )
					result.push_back( convert< double >( *iter ) );
				else if ( is_string )
					optimise_word( result, *iter );
				else if ( is< word_list >( *iter ) )
					optimise( result, as< word_list >( *iter ) );
				else
					result.push_back( *iter );
			}

			if ( trace( ) & 16 )
				trace_list( "post", result );

			return result;
		}

		void optimise_word( word_list &result, const rp::any &any )
		{
			const std::string &token = if_string( any );
			const auto diter = find_in_lexicon( token );

			if ( diter != end_of_lexicon( ) )
				optimise( result, diter->second );
			else
				result.push_back( any );
		}

		void optimise( word_list &result, const word::type &type )
		{
			switch( type.id )
			{
				case word::is_empty:
					break;
				case word::is_function:
				case word::is_parser:
					result.push_back( type.object.function );
					break;
				case word::is_list:
					result.push_back( *type.object.list );
					break;
				case word::is_vocab:
					result.push_back( type.word );
					return;
				case word::is_inline:
					RPANY_THROW( undefined_exception, "Invalid location for inline word '" + type.word + "'" );
			}
		}

		void optimise( word_list &result, const word_list &list )
		{
			word_list temp = optimise( list );
			result.insert( result.end( ), result.begin( ), result.end( ) );
		}

		void execute( const word_list &list )
		{
			for ( auto iter = list.begin( ); iter != list.end( ); iter ++ )
			{
				rp::any local_var;
				if ( trace_ & 32 && is< std::string >( *iter ) ) error( ) << "%% " << as< std::string >( *iter ) << std::endl;
				if ( is_interrupted( ) )
					RPANY_THROW( interrupted_exception, "Interrupted" );
				if ( is< word_function >( *iter ) )
					run_word( as< word_function >( *iter ) );
				else if ( is_numeric( *iter ) )
					append( *iter );
				else if ( is< word_list >( *iter ) )
					run_word( as< word_list >( *iter ) );
				else if ( is< parser_ptr >( *iter ) )
					as< parser_ptr >( *iter )->execute_request( self( ) );
				else if ( is_local_var( *iter, local_var ) )
					append( local_var );
				else if ( is_word( *iter ) )
					run_word( *iter );
				else if ( is< std::string >( *iter ) )
					push( *iter );
				else
					RPANY_THROW( undefined_exception, "Invalid token '" + convert< std::string >( *iter ) + "'" );
			}
		}

		void trace( int level )
		{
			trace_ = level;
		}

		int trace( ) const
		{
			return trace_;
		}

		void validate_word( const std::string &word )
		{
			if ( is_numeric( word ) )
				RPANY_THROW( invalid_exception, "Word cannot be numeric - '" + word + "' is invalid" );
			else if ( word.find( ' ' ) != std::string::npos )
				RPANY_THROW( invalid_exception, "Word cannot contain spaces - '" + word + "' is invalid" );
			else if ( word.find( '#' ) == 0 )
				RPANY_THROW( invalid_exception, "Word cannot start with hash - '" + word + "' is invalid" );
			else if ( word.find( "::" ) == 0 )
				RPANY_THROW( invalid_exception, "Words cannot start with :: - '" + word + "' is invalid" );
		}

		void validate( const std::string &word )
		{
			validate_word( word );
			if ( find_in_lexicon( word ) == end_of_lexicon( ) )
				dictionary_->vocab[ vocab_current( ) ].insert( word );
		}

	protected:
		// Provided by the extending stack implemenation
		virtual stack &self( ) = 0;

		void remove_from_vocab( const word::type &word )
		{
			// Obtain vocab list
			auto vocab_iter = dictionary_->vocab.find( word.vocab );
			RPANY_THROW_IF( vocab_iter == dictionary_->vocab.end( ), undefined_exception, "Vocab '" + word.vocab + "' not found" );
			auto &list = vocab_iter->second;

			// Remove word from list
			auto list_iter = list.find( word.word );
			RPANY_THROW_IF( list_iter == list.end( ), undefined_exception, "Word '" + word.word + "' not found in vocab '" + word.vocab + "'." );
			list.erase( list_iter );

			// Remove empty vocab list
			if ( list.size( ) == 0 )
				dictionary_->vocab.erase( vocab_iter );
		}

		void prepare_word( const std::string &word )
		{
			validate_word( word );
			auto iter = find_in_lexicon( word );
			if ( iter != end_of_lexicon( ) )
			{
				remove_from_vocab( iter->second );
				lexicon.erase( iter );
			}
		}

		bool is_full_word_syntax( const rp::any &token ) const
		{
			if ( token.type( ) != typeid( std::string ) ) return false;
			return is_full_word_syntax( rp::any_cast< std::string >( token ) );
		}

		bool is_full_word_syntax( const std::string &token ) const
		{
			return token.find( "::" ) == 0 && token.find( "->", 2 ) != std::string::npos;
		}

		std::string extract_word( const std::string &token ) const
		{
			if ( !is_full_word_syntax( token ) ) return token;
			const auto accessor = token.find( "->", 2 );
			if ( token.find( "::" ) != 0 || accessor == std::string::npos ) RPANY_THROW( invalid_exception, "Misrouted token - '" + token + "' is not of the form ::vocab->word" );
			const auto vocab_name = token.substr( 2, accessor - 2 );
			const auto vocab = dictionary_->vocab.find( vocab_name );
			if ( vocab == dictionary_->vocab.end( ) ) RPANY_THROW( undefined_exception, "Unrecognised vocab '" + vocab_name + "' in '" + token + "'" );
			const auto word = token.substr( accessor + 2 );
			if ( vocab->second.find( word ) == vocab->second.end( ) ) RPANY_THROW( undefined_exception, "Unrecognised word '" + word + "' in '" + token + "'" );
			return word;
		}

		void run_word( const rp::any &token )
		{
			run_word( as< std::string >( token ) );
		}

		void run_word( const std::string &token )
		{
			auto iter = find_in_lexicon( token );
			run_word( iter->second );
		}

		void run_word( const word::type &type )
		{
			const std::string &word = type.word;
			switch( type.id )
			{
				case word::is_empty:
					break;
				case word::is_function:
					run_word( type.object.function );
					break;
				case word::is_parser:
					run_word( type.object.parser, word );
					break;
				case word::is_vocab:
					run_vocab( word );
					break;
				case word::is_list:
					run_word( *type.object.list );
					break;
				case word::is_inline:
					RPANY_THROW( undefined_exception, "Invalid location for inline word '" + word + "'" );
			}
		}

		void run_word( const word_function &function )
		{
			function( self( ) );
		}

		void run_word( const word_parser &function, const std::string &token )
		{
			function( self( ), token );
		}

		void run_word( const word_list &list, int start = 0 )
		{
			try
			{
				frame_stack_.push_back( std::move( word::frame_type( ) ) );
				int index = 0;
				for ( auto iter = list.begin( ); iter != list.end( ); iter ++, index ++ )
				{
					rp::any local_var;
					if ( trace_ & 32 && is< std::string >( *iter ) ) error( ) << "%%% " << as< std::string >( *iter ) << std::endl;
					if ( index < start )
						continue;
					else if ( is< word_function >( *iter ) )
						run_word( as< word_function >( *iter ) );
					else if ( is< word_list >( *iter ) )
						run_word( as< word_list >( *iter ) );
					else if ( is< parser_ptr >( *iter ) )
						as< parser_ptr >( *iter )->execute_request( self( ) );
					else if ( is_local_var( *iter, local_var ) )
						append( local_var );
					else
						push( *iter );
				}
				frame_stack_.pop_back( );
			}
			catch( ... )
			{
				frame_stack_.pop_back( );
				throw;
			}
		}

		void run_vocab( const std::string &vocab )
		{
			try
			{
				vocab_start( vocab );
				run_word( *( find_in_lexicon( vocab )->second.object.list ), 1 );
				vocab_end( );
			}
			catch( ... )
			{
				vocab_end( );
				throw;
			}
		}

		void run_inline( const rp::any &token )
		{
			const auto diter = find_in_lexicon( as< std::string >( token ) );
			const auto &definition = *diter->second.object.list;
			for ( auto iter = ++ definition.begin( ); iter != definition.end( ); iter ++ )
				push( *iter );
		}

		void trace_list( const std::string &title, const word_list &list )
		{
			error( ) << title << ": ";
			for( auto iter = list.begin( ); iter != list.end( ); iter ++ )
				error( ) << convert< std::string >( *iter ) << " ";
			error( ) << std::endl;
		}

		dictionary_type_ptr dictionary_;
		dictionary_type::lexicon_t &lexicon;
		word::frame_stack_type frame_stack_;
		int trace_ = 0;
};
