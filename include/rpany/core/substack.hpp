#pragma once

namespace substack {

inline vector_ptr vector_pull( stack &s )
{
	s << "list-to-vector";
	return s.pull< vector_ptr >( );
}

template < typename T >
void execute_list( stack &s )
{
	typedef std::shared_ptr< T > ptr;
	auto vector = vector_pull( s );
	stack &ss = *( s.cast< ptr >( ) );
	for ( auto iter : *vector )
		ss << as< std::string >( iter );
}

template < typename T >
void eval_list( stack &s )
{
	typedef std::shared_ptr< T > ptr;
	auto vector = vector_pull( s );
	stack &ss = *( s.cast< ptr >( ) );
	for ( auto iter : *vector )
		ss.evaluate( as< std::string >( iter ) );
}

// FIXME: These should be moved to the io vocab
inline int get_max_width( stack &s )
{
	size_t result = 132;
	if ( s.is_word( std::string( "report-max-width" ) ) )
		s << "report-max-width" >> result;
	else if ( s.vfind( "report-max-width" ) != s.vend( ) )
		result = as< int >( s[ "report-max-width" ] );
	return static_cast< int >( result );
}

inline int get_max_height( stack &s )
{
	size_t result = 50;
	if ( s.is_word( std::string( "report-max-height" ) ) )
		s << "report-max-height" >> result;
	else if ( s.vfind( "report-max-height" ) != s.vend( ) )
		result = as< int >( s[ "report-max-height" ] );
	return static_cast< int >( result );
}

template < typename T >
void inherit_io( stack &s )
{
	typedef std::shared_ptr< T > ptr;
	stack &ss = *( s.cast< ptr >( ) );
	ss.set_input( s.input( ) );
	ss.set_output( s.output( ) );
	ss.set_error( s.error( ) );
	// FIXME: Need a better way to handle variables (perhaps using local/variable)
	ss[ "report-max-width" ] = get_max_width( s );
	ss[ "report-max-height" ] = get_max_height( s );
}

template < typename T >
void educate_substack( stack &s )
{
	typedef std::shared_ptr< T > ptr;
	const std::string vocab = s.pull< std::string >( );
	stack &ss = *( s.cast< ptr >( ) );
	if ( vocab_enrolled( vocab ) )
		educate( ss, vocab );
	else
		ss.inherit( s, vocab );
}

template < typename T >
void educate_list( stack &s )
{
	typedef std::shared_ptr< T > ptr;
	auto vector = vector_pull( s );
	stack &ss = *( s.cast< ptr >( ) );
	for ( auto iter : *vector )
	{
		const auto vocab = as< std::string >( iter );
		if ( vocab_enrolled( vocab ) )
			educate( ss, vocab );
		else
			ss.inherit( s, vocab );
	}
}

template < typename T >
void create( stack &s )
{
	typedef std::shared_ptr< T > ptr;
	s.teach( "ss-new", [] ( stack &s ) { s += ptr( new T ); inherit_io< T >( s ); } );
	s.teach( "ss-clone", [] ( stack &s ) { s += ptr( new T ); s.cast< ptr >( )->inherit( s ); inherit_io< T >( s ); } );
	s.teach( "ss-inherit", [] ( stack &s ) { s += ptr( new T( *( s.cast< ptr >( ) ) ) ); inherit_io< T >( s ); } );
	s.teach( "ss-self", [] ( stack &s ) { s += ptr( new T( s ) ); inherit_io< T >( s ); } );
	s.teach( "ss-self-educated", [] ( stack &s ) { s += ptr( new T( *( s.cast< ptr >( ) ), s ) ); inherit_io< T >( s ); } );
	s.teach( "ss-educate", educate_substack< T > );
	s.teach( "ss-educate-list", educate_list< T > );
	s.teach( "ss-exec-list", execute_list< T > );
	s.teach( "ss-eval-list", eval_list< T > );
	s.teach( "ss-educate-all", [] ( stack &s ) { educate( *( s.cast< ptr >( ) ) ); } );
	s.teach( "ss-ready", [] ( stack &s ) { s += !s.cast< ptr >( )->is_parsing( ); } );
	s.teach( "ss-eval", [] ( stack &s ) { auto cmd = s.pull< std::string >( ); eval( *( s.cast< ptr >( ) ), cmd ); } );
	s.teach( "ss-eval-throw", [] ( stack &s ) { auto cmd = s.pull< std::string >( ); eval( *( s.cast< ptr >( ) ), cmd, true ); } );
	s.teach( "ss-pull", [] ( stack &s ) { T &ss = *( s.cast< ptr >( ) ); s += ss.pull( ); } );
	s.teach( "ss-dictionary", [] ( stack &s ) { T &ss = *( s.cast< ptr >( ) ); s += ss.get_dictionary( ); } );
	s.teach( "?ss-eval-word", [] ( stack &s ) { auto word = s.pull< std::string >( ); T &ss = *( s.cast< ptr >( ) ); if ( ss.is_word( word ) ) eval( ss, word ); } );
	s.teach( "ss-eof", [] ( stack &s ) { T &ss = *( s.cast< ptr >( ) ); s += ss.closed( ); } );
	s.teach( "ss-disable-append", [] ( stack &s ) { T &ss = *( s.cast< ptr >( ) ); ss.disable_append( ); } );
	s.teach( "ss-enable-append", [] ( stack &s ) { T &ss = *( s.cast< ptr >( ) ); ss.enable_append( ); } );
}

}
