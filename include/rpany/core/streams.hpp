#pragma once

using namespace std::chrono;

class streams
{
	public:

		// Specifies streams associated to the stack
		void set_output( std::ostream &out ) { out_ = &out; }
		void set_error( std::ostream &err ) { err_ = &err; }
		void set_input( std::istream &inp ) { inp_ = &inp; }
		void set_sleep( std::function< void ( double ) > &sleep ) { sleep_ = sleep; }

		// Obtain streams
		std::ostream &output( ) const { return *out_; }
		std::ostream &error( ) const { return *err_; }
		std::istream &input( ) const { return *inp_; }
		void close( ) { inp_->setstate( std::ios_base::eofbit ); closed_ = true; }
		bool closed( ) const { return closed_ || !inp_->good( ); }
		void sleep( double time ) { sleep_( time ); }

	private:
		// Sets the default streams
		mutable std::ostream *out_ = &std::cout;
		mutable std::ostream *err_ = &std::cerr;
		mutable std::istream *inp_ = &std::cin;
		mutable std::function< void ( double ) > sleep_ = []( double time ) { std::this_thread::sleep_for( milliseconds( static_cast< int >( time * 1000.0 ) ) ); };
		bool closed_ = false;
};
