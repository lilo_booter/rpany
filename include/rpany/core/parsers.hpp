#pragma once

class parse_token : public parser
{
	public:
		typedef void ( *func_t )( stack &s, const std::string &token );

		parse_token( const func_t &func_ )
		: func( func_ )
		{ }

		bool parse( stack &s, const rp::any &token ) { token_ = if_string( token ); return true; }
		void execute( stack &s ) { func( s, token_ ); }

	private:
		std::string token_;
		const func_t func;
};

template < parse_token::func_t F >
void create_parse_token( stack &s, const std::string &token )
{
	s.set_parser( new parse_token( F ) );
}

class parse_line : public parser
{
	public:
		typedef void ( *func_t )( stack &s, const std::vector< std::string > &tokens );

		parse_line( const func_t &func_ )
		: func( func_ )
		{ }

		bool parse( stack &s, const rp::any &token ) override { tokens_.push_back( if_string( token ) ); return false; }
		void execute( stack &s ) override { func( s, tokens_ ); }
		bool execute_on_eol( ) const override { return true; }

	private:
		std::vector< std::string > tokens_;
		const func_t func;
};

template < parse_line::func_t F >
void create_parse_line( stack &s, const std::string &token )
{
	s.set_parser( new parse_line( F ) );
}
