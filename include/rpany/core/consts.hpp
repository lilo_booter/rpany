#pragma once

namespace math {

constexpr double PI = 3.14159265358979;
constexpr double E = 2.71828182845905;

}
