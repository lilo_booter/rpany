#pragma once

namespace rpany {

// Provide mutex for the registry and coercion systems
// (this should be a shared_mutex [c++17] to allow concurrent read access)
std::mutex mutex;

// Provide the registry
enrollment_type enrollment;
enrolled_vocabs_type enrolled_vocabs;
registry_type registry;
registry_set_type registry_set;

// Provide the factory
factory::method_list factory::methods;

// Initialise the coercions map
coercion_map coercions = coercions_create( );

// Enroll self
bool _enroll_rpany = enroll( "rpany" );

// Initialise the random seed
inline bool srand( )
{
	std::srand( static_cast< unsigned int >( std::time( 0 ) ) );
	return true;
}

bool _set_random_seed = srand( );

// Sometimes it's helpful to pinpoint which line an exception is thrown from. This
// var is used by the RPANY_THROW macro to enable it.
bool exception_trace = getenv( "RPANY_THROW_TRACE" ) ? true : false;

// Incremented when the process is interrupted
volatile int interrupt_count = 0;

#if defined( WIN32 ) || defined( _WIN32 )
// Windows interrupt handler
BOOL WINAPI windows_signal_handler( DWORD type )
{
	if ( interrupt_count ++ > 0 )
	{
		std::cerr << std::endl << "Process interrupted" << std::endl;
		return FALSE;
	}
	return TRUE;
}

// I hate Windows
#ifndef ENABLE_VIRTUAL_TERMINAL_PROCESSING
#define ENABLE_VIRTUAL_TERMINAL_PROCESSING 0x0004
#endif

// I HATE WINDOWS
bool windows_escape_sequences( )
{
	// Set output mode to handle virtual terminal sequences
	HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);

	// OK - don't do it then
	if ( hOut == INVALID_HANDLE_VALUE ) return false;

	// OK - don't do it then
	DWORD dwMode = 0;
	if ( !GetConsoleMode( hOut, &dwMode ) ) return false;

	// Are you sure sure you want to?
	dwMode |= ENABLE_VIRTUAL_TERMINAL_PROCESSING;
	if ( !SetConsoleMode( hOut, dwMode ) ) return false;

	// OK then
	return true;
}

#endif

}
