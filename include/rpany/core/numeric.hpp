#pragma once

// bool is_numeric
//
// Determines if the argument provided is a numeric or not.

inline bool is_numeric( const std::string &token )
{
	char *p = 0;
	strtod( token.c_str( ), &p );
	return *p ? false : true;
}

inline bool is_rational( const std::string &token )
{
	char *p = 0;
	if ( !isdigit( token[ 0 ] ) ) return false;
	strtoimax( token.c_str( ), &p, 0 );
	if ( *p != ':' ) return false;
	strtoimax( p + 1, &p, 0 );
	return *p ? false : true;
}

inline bool is_numeric( const rp::any &token )
{
	std::type_index type = token.type( );
	return type == typeid( int ) || 
		   type == typeid( double ) || 
		   type == typeid( float ) || 
		   type == typeid( bool ) || 
		   type == typeid( size_t ) || 
		   type == typeid( rational ) ||
		   ( type == typeid( std::string ) && is_numeric( as< std::string >( token ) ) ) ||
		   ( type == typeid( std::string ) && is_rational( as< std::string >( token ) ) );
}
