#pragma once

// parser class
//
// Provides a general mechanism for parsing and caching a sequence of tokens

class parser : public std::enable_shared_from_this< parser >
{
	public:
		virtual ~parser( ) { }
		const word_list &tokens( ) { return tokens_; }
		void push( const rp::any &t ) { tokens_.push_back( t ); }
		virtual bool parse( stack &, const rp::any & ) = 0;
		virtual bool execute_on_eol( ) const { return false; }

		void execute_request( stack &s )
		{
			optimise_request( s );
			execute( s );
		}

		virtual void execute( stack & ) = 0;

		void optimise_request( stack &s )
		{
			if ( !optimised )
			{
				optimise( s );
				optimised = true;
			}
		}

		virtual void optimise( stack & ) { }

		word_list &inline_list( ) { return inline_; }
	private:
		word_list tokens_;
		word_list inline_;
		bool optimised = false;
};
