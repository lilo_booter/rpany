#pragma once

typedef std::shared_ptr< stack > stack_ptr;

namespace factory {

typedef stack_ptr ( *method )( );
typedef std::vector< method > method_list;

extern method_list methods;

inline stack_ptr default_stack( )
{
	return stack_ptr( new stack );
}

inline void enroll( method fn )
{
	std::lock_guard< std::mutex > guard( mutex );
	methods.push_back( fn );
}

inline method current( )
{
	std::lock_guard< std::mutex > guard( mutex );
	if ( methods.size( ) ) return methods.back( );
	return default_stack;
}

inline stack_ptr create( )
{
	method fn = current( );
	return fn( );
}

inline stack_ptr create( const stack &s )
{
	stack_ptr ss = create( );
	ss->set_input( s.input( ) );
	ss->set_output( s.output( ) );
	ss->set_error( s.error( ) );
	ss->inherit( s );
	return ss;
}

inline stack_ptr create( const stack_ptr &s )
{
	return create( *s );
}

}
