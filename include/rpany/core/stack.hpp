#pragma once

#include "parser.hpp"
#include "dictionary.hpp"

int eval( stack &stack, const std::string &string, bool rethrow );

// stack
//
// Provides the rpany::stack logic - holds all state relating to execution and compilation.

class stack : public dictionary, public std::enable_shared_from_this< stack >
{
	public:
		// Internal types

		typedef std::deque< parser_ptr > parser_type;
		typedef std::vector< rp::any > stack_type;
		typedef std::shared_ptr< stack_type > stack_type_ptr;

		stack( )
		: stack_( stack_type_ptr( new stack_type ) )
		{ }

		stack( const stack &s )
		: stack_( s.stack_ )
		{ }

		stack( const stack &s, const dictionary &d )
		: dictionary( d )
		, stack_( s.stack_ )
		{ }

		// This is the main method used in this class.
		//
		// Parsing state has the highest priority, followed by words, followed by numbers.

		virtual void push( const rp::any &token )
		{
			if ( trace( ) & 1 && !is_extended( ) ) error( ) << "+ " << as_string( token ) << std::endl;
			rp::any local_var;
			if ( is_parsing( ) )
				parse_token( token );
			else if ( is< std::string >( token ) && is_rational( as< std::string >( token ) ) )
				append_object( convert< rational >( token ) );
			else if ( is_numeric( token ) )
				append_object( token );
			else if ( is_local_var( token, local_var ) )
				append( local_var );
			else if ( is< word_function >( token ) )
				run_word( as< word_function >( token ) );
			else if ( is< parser_ptr >( token ) )
				as< parser_ptr >( token )->execute_request( self( ) );
			else if ( is_inline( token ) )
				run_inline( token );
			else if ( is_word( token ) )
				run_word( token );
			else
				RPANY_THROW( undefined_exception, "Invalid token '" + as_string( token ) + "'" );
		}

		// We want to accept char *, but we don't want to hold them on the stack, hence
		// we convert to std::string here and invoke the main push method.

		void push( const char *token )
		{
			push( std::string( token ) );
		}

		// Convenience method to evaluate a string

		stack &evaluate( const std::string &string, bool rethrow = false )
		{
			eval( self( ), string, rethrow );
			return self( );
		}

		// Allows words parsers to consume all tokens on a line
		void execute_on_eol( )
		{
			if ( is_parsing( ) && parser_.back( )->execute_on_eol( ) )
			{
				auto parser = parser_.back( );
				parser_.pop_back( );
				parser->execute( self( ) );
			}
		}

		// Enables the stack for data append
		void enable_append( )
		{
			allow_append_ = true;
		}

		// Disables the stack for data append
		void disable_append( )
		{
			allow_append_ = false;
		}

		// Extending classes can defer stack evaluation

		virtual void commit( ) { }

		// Optionally, the implementation can provide a pending list for dump reporting
		virtual const word_list pending( ) const { return { }; }

		// Allow commits to be blocked for read access (write access ignores this)
		void block( ) { blocked_ = true; }
		bool blocked( ) const { return blocked_; }
		void unblock( ) { blocked_ = false; }

		// Words can bypass the push logic to put arbitrary data on to the stack

		void append( const rp::any &token )
		{
			if ( trace( ) & 2 ) error( ) << "* " << as_string( token ) << std::endl;
			append_object( token );
		}

		void append( const char *token )
		{
			append_object( std::string( token ) );
		}

		// This is triggered when a new word is defined
		void word_defined( const std::string &word )
		{
			if ( trace( ) & 4 ) evaluate( "see " + word );
		}

		// Fetch the nth item from the stack (0 is top of stack)

		rp::any &pick( int index = 0 )
		{
			if ( !blocked( ) ) commit( );
			const int size = static_cast< int >( ( *stack_ ).size( ) );
			if ( index >= 0 && index < size )
				return ( *stack_ )[ size - index - 1 ];
			else if ( index < 0 && std::abs( index ) <= size )
				return ( *stack_ )[ size + index ];
			else
				RPANY_THROW_DEFAULT( underflow_exception );
		}

		// Alternative to pick - cast picked index to requested type
		template < typename T >
		T &cast( int index = 0 )
		{
			return rp::any_cast< T & >( pick( index ) );
		}

		// Alternative to pick and cast - coerce picked index to requested type
		template < typename T >
		T coerce( int index = 0 )
		{
			return convert< T >( pick( index ) );
		}

		void roll( int index = 0 )
		{
			commit( );
			rp::any item;
			const int size = static_cast< int >( ( *stack_ ).size( ) );
			int offset = -1;
			if ( index >= 0 && index < size )
				offset = size - index - 1;
			else if ( index < 0 && std::abs( index ) <= size )
				offset = size + index;
			else
				RPANY_THROW_DEFAULT( underflow_exception );

			( *stack_ ).push_back( ( *stack_ )[ offset ] );
			( *stack_ ).erase( ( *stack_ ).begin( ) + offset );
		}

		// Remove n items from the stack

		void pop( int count = 1 )
		{
			commit( );
			if ( count < 0 )
				RPANY_THROW( underflow_exception, "Invalid pop count" );

			if ( static_cast< size_t >( count ) > ( *stack_ ).size( ) )
				RPANY_THROW_DEFAULT( underflow_exception );

			auto first = ( *stack_ ).end( ) - count;
			( *stack_ ).erase( first, ( *stack_ ).end( ) );
		}

		void pop( vector_ptr &vector, int count = -1 )
		{
			// Commit any incomplete stuff at top of stack
			commit( );

			// Courtesy: obtain from stack if necessary
			if ( count == -1 ) count = pull< int >( );

			// If we're still negative, then there's a problem
			if ( count < 0 )
				RPANY_THROW( underflow_exception, "Invalid pop count" );

			// Ensure there are enough items on the stack
			if ( static_cast< size_t >( count ) > ( *stack_ ).size( ) )
				RPANY_THROW_DEFAULT( underflow_exception );

			// Allocate the vector if necessary
			if ( vector == 0 )
				vector.reset( new vector_type( ) );

			// Reduce reallocations
			vector->reserve( vector->size( ) + count );

			// Move stack items into the vector
			auto first = ( *stack_ ).end( ) - count;
			for ( auto iter = first; iter != ( *stack_ ).end( ); iter ++ )
				vector->push_back( std::move( *iter ) );

			// Remove the moved items
			( *stack_ ).erase( first, ( *stack_ ).end( ) );
		}

		rp::any pull( )
		{
			commit( );
			if ( ( *stack_ ).size( ) == 0 )
				RPANY_THROW_DEFAULT( underflow_exception );

			rp::any result = std::move( ( *stack_ ).back( ) );
			( *stack_ ).pop_back( );
			return result;
		}

		rp::any pull( int index )
		{
			if ( index ) roll( index );
			return std::move( pull( ) );
		}

		template< typename T >
		T pull( int index = 0 )
		{
			commit( );
			return std::move( convert< T >( pull( index ) ) );
		}

		// Stack interrogation
		size_t depth( )
		{
			if ( !blocked( ) ) commit( );
			return ( *stack_ ).size( );
		}

		bool has_depth( )
		{
			try
			{
				return depth( ) != 0;
			}
			catch( const std::exception &e )
			{
				error( ) << "ERROR: Invalid Result: " << e.what( ) << std::endl;
			}
			return false;
		}

		// Set the current parser
		void set_parser( parser *parser )
		{
			parser_.push_back( parser_ptr( parser ) );
		}

		bool is_parsing( ) const
		{
			return parser_.size( ) != 0;
		}

		// General reset state method
		void reset( bool dictionary = false )
		{
			commit( );
			parser_.clear( );
			( *stack_ ).clear( );
			if ( dictionary )
				reset_dictionary( );
		}

	protected:
		// Provide stack reference to this
		virtual inline stack &self( )
		{
			return *this;
		}

		virtual bool is_extended( ) const
		{
			return false;
		}

	private:
		void append_object( const rp::any &object )
		{
			if ( !allow_append_ )
				RPANY_THROW( invalid_exception, "Stack append is disabled" );

			commit( );
			( *stack_ ).push_back( object );
		}

		void parse_token( const rp::any &token )
		{
			if ( trace( ) & 8 && is< std::string >( token ) )
				error( ) << std::string( parser_.size( ) + 1, '+' ) << " " << if_string( token ) << '\n';

			parser_ptr parser = parser_.back( );
			if ( parser->parse( self( ), token ) )
			{
				parser->push( token );
				parser_.pop_back( );
				push( parser );
			}
			else
			{
				word_list list = parser->inline_list( );
				if ( list.size( ) )
				{
					parser->inline_list( ).clear( );
					for ( auto iter = list.begin( ); iter != list.end( ); iter ++ )
						push( *iter );
				}
				else
				{
					parser_.back( )->push( token );
				}
			}
		}

		// State of the stack
		parser_type parser_;
		const stack_type_ptr stack_;
		bool blocked_ = false;
		bool allow_append_ = true;
};

class scoped_block
{
	public:
		scoped_block( stack &s_ ) : s( s_ ) { s.block( ); }
		~scoped_block( ) { s.unblock( ); }
		stack &s;
};

template< typename T >
inline stack &operator >>( stack &s, T &output )
{
	output = std::move( s.pull< T >( ) );
	return s;
}

template< typename T >
inline stack &operator <<( stack &s, const T &input )
{
	s.push( input );
	return s;
}

template< typename T >
inline stack &operator +=( stack &s, const T &input )
{
	s.append( input );
	return s;
}
