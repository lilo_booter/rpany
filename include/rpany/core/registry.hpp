#pragma once

#if defined( WIN32 ) || defined( _WIN32 )
	#pragma warning(disable:4996)
	#pragma warning(disable:4804)
	#pragma warning(disable:4180)
#endif

// Defines a registry type for vocabularies
typedef std::vector< std::string > enrollment_type;
typedef std::map< std::string, enrollment_type > enrolled_vocabs_type;
typedef std::map< std::string, word_function > registry_type;
typedef std::pair< std::string, word_function > registry_pair;
typedef std::vector< registry_pair > registry_set_type;

// Holds the state of the registry
extern std::mutex mutex;
extern enrollment_type enrollment;
extern enrolled_vocabs_type enrolled_vocabs;
extern registry_type registry;
extern registry_set_type registry_set;

inline bool enroll( const std::string &tool )
{
	std::lock_guard< std::mutex > guard( mutex );
	enrollment.push_back( tool );
	return true;
}

inline enrollment_type enrolled( )
{
	std::lock_guard< std::mutex > guard( mutex );
	return enrollment;
}

inline int vocab_register( const std::string &name, word_function function )
{
	std::lock_guard< std::mutex > guard( mutex );
	if ( registry.find( name ) == registry.end( ) )
		enrolled_vocabs[ enrollment.back( ) ].push_back( name );
	registry[ name ] = function;
	registry_set.push_back( registry_pair( name, function ) );
	return 0;
}

inline bool vocab_enrolled( const std::string &name )
{
	std::lock_guard< std::mutex > guard( mutex );
	return registry.find( name ) != registry.end( );
}

inline enrollment_type enrolled_vocab( const std::string &tool )
{
	std::lock_guard< std::mutex > guard( mutex );
	auto iter = enrolled_vocabs.find( tool );
	if ( iter == enrolled_vocabs.end( ) )
		RPANY_THROW( stack_exception,  "'" + tool + "' is unknown" );
	return iter->second;
}

inline void educate( stack &s, const std::string &vocab )
{
	std::lock_guard< std::mutex > guard( mutex );
	auto iter = registry.find( vocab );
	if ( iter == registry.end( ) )
		RPANY_THROW( stack_exception, "Unable to locate a vocab called '" + vocab + "' in the registry" );
	iter->second( s );
}

inline void educate( stack &s, const std::vector< std::string > &vocabs )
{
	for ( auto iter = vocabs.begin( ); iter != vocabs.end( ); iter ++ )
		educate( s, *iter );
}

inline void educate( stack &s )
{
	std::string exclude = std::getenv( "RPANY_EXCLUDE" ) ? " " + std::string( std::getenv( "RPANY_EXCLUDE" ) ) + " " : "";
	for ( auto iter = registry_set.begin( ); iter != registry_set.end( ); iter ++ )
	{
		if ( exclude != "" )
		{
			if ( exclude.find( " " + iter->first + " " ) != std::string::npos ) continue;
			std::cerr << "vocab: " << iter->first << std::endl;
		}
		iter->second( s );
	}
}

#define RPANY_OPEN( name, arg ) inline void vocab_##name( arg ) { s.vocab_start( #name );
#define RPANY_CLOSE( name ) s.vocab_end( ); }; int _vocab_##name = vocab_register( #name, vocab_##name )
