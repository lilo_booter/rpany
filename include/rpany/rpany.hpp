#pragma once

#if defined( WIN32 ) || defined( _WIN32 )
	#define WIN32_LEAN_AND_MEAN
	#include <windows.h>
	#ifndef _CRT_SECURE_NO_WARNINGS
		#define _CRT_SECURE_NO_WARNINGS
	#endif
#endif

// Include system provided headers
#include <rpany/system.hpp>

// Includes the rpany headers
#include <rpany/core/__init__.hpp>
