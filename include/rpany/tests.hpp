#pragma once

namespace rpany {

inline std::string print( const word_list &list )
{
	std::ostringstream s;

	for ( auto token = list.begin( ); token != list.end( ); token ++ )
	{
		if ( token != list.begin( ) ) s << " ";

		std::string value = is< const char * >( *token ) ? std::string( as< const char * >( *token ) ) : convert< std::string >( *token );

		if ( list.size( ) > 1 )
			s << quoted( value );
		else
			s << value;
	}

	return s.str( );
}

enum exception_type
{
	e_underflow = 0x1,
	e_overflow = 0x2,
	e_leave = 0x4,
	e_coercion = 0x8,
	e_undefined = 0x10,
	e_stack = 0x20,
	e_std_exception = 0x40,
};

inline bool exception_check( const std::exception &e, int exceptions )
{
	bool result = false;
	if ( exceptions & e_underflow ) result = result || dynamic_cast< const underflow_exception * >( &e );
	if ( exceptions & e_overflow ) result = result || dynamic_cast< const overflow_exception * >( &e );
	if ( exceptions & e_leave ) result = result || dynamic_cast< const leave_exception * >( &e );
	if ( exceptions & e_coercion ) result = result || dynamic_cast< const coercion_exception * >( &e );
	if ( exceptions & e_undefined ) result = result || dynamic_cast< const undefined_exception * >( &e );
	if ( exceptions & e_stack ) result = result || dynamic_cast< const stack_exception * >( &e );
	if ( exceptions & e_std_exception ) result = true;
	return result;
}

inline bool test( stack &s, const std::string &description, const word_list &input, const word_list &output, const bool &eval = true, int exceptions = 0 )
{
	bool result = true;

	try
	{
		// Push each input token to the stack
		if ( eval )
		{
			for ( auto token = input.begin( ); token != input.end( ); token ++ )
				rpany::eval( s, as< std::string >( *token ), true );
		}
		else
		{
			for ( auto token = input.begin( ); token != input.end( ); token ++ )
			{
				if ( is< const char * >( *token ) )
					s << std::string( as< const char * >( *token ) );
				else
					s << *token;
			}
		}

		s.depth( );
	}
	catch ( std::exception &e )
	{
		result = exception_check( e, exceptions );
	}
	catch ( ... )
	{
		result = false;
	}

	try
	{
		// Stack should be identical to output
		for ( auto item = output.rbegin( ); item != output.rend( ); item ++ )
		{
			auto other = s.pick( );
			if ( is< const char * >( *item ) )
				result = result && convert< std::string >( other ) == std::string( as< const char * >( *item ) );
			else
				result = result && convert< std::string >( other ) == convert< std::string >( *item );
			s.pop( );
		}

		// Stack should be empty now
		result = result && s.depth( ) == 0;
	}
	catch ( std::exception &e )
	{
		result = exception_check( e, exceptions );
	}
	catch ( ... )
	{
		result = false;
	}

	// Report success or failure
	s.output( ) << ( result ? "PASS" : "FAIL" ) << ": " << description << " -> " << print( input ) << " -> [ " << print( output ) << " ]" << std::endl;

	try
	{
		// Always leave an empty stack for the next test
		s.pop( static_cast< int >( s.depth( ) ) );
	}
	catch( ... )
	{
		result = false;
	}

	return result;
}

struct suite
{
	suite( stack &s_, const std::string &project_ )
	: s( s_ )
	, project( project_ )
	{ }

	int count = 0;
	int failed = 0;
	stack &s;
	const std::string project;

	void run( const std::string &description, const word_list &input, const word_list &output, const bool &eval = false, int exceptions = 0 )
	{
		if ( !test( s, description, input, output, eval, exceptions ) )
			failed ++;
		count ++;
	}

	void eval( const std::string &description, const std::string &command, const word_list &output, int exceptions = 0 )
	{
		run( description, { command }, output, true, exceptions );
	}

	~suite( )
	{
		if ( failed == 0 )
			s.error( ) << project << ": " << count << " tests passed" << std::endl;
		else
			s.error( ) << project << ": " << failed << " failed from " << count << " tests" << std::endl;
	}
};

struct rt
{
	rt( const std::string &d, const word_list &i, const word_list &o, int e = 0 )
	: description( d ), input( i ), output( o ), exceptions( e )
	{ }

	const std::string description;
	const word_list input;
	const word_list output;
	const int exceptions = 0;
};

typedef std::vector< rt > run_group;

inline void run_test( suite &t, const std::string &name, const run_group &run )
{
	for ( auto iter = run.begin( ); iter != run.end( ); iter ++ )
		t.run( name + ": " + ( *iter ).description, ( *iter ).input, ( *iter ).output, false, ( *iter ).exceptions );
}

struct et
{
	et( const std::string &d, const std::string &c, const word_list &o, int e = 0 )
	: description( d ), command( c ), output( o ), exceptions( e )
	{ }

	const std::string description;
	const std::string command;
	const word_list output;
	const int exceptions = 0;
};

typedef std::vector< et > eval_group;

inline void eval_test( suite &t, const std::string &name, const eval_group &eval )
{
	for ( auto iter = eval.begin( ); iter != eval.end( ); iter ++ )
		t.eval( name + ": " + ( *iter ).description, ( *iter ).command, ( *iter ).output, ( *iter ).exceptions );
}

#define EVAL et
#define RUN rt

}
