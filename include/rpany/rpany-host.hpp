#pragma once

#if defined( _WIN32 ) || defined( WIN32 )
#	define WIN32_LEAN_AND_MEAN
#	include <windows.h>
#endif

// Define the full rpany core
#include <rpany/rpany.hpp>
#include <rpany/core/__host__.hpp>

// Provide includes required by vocab
#include <cmath>
#include <functional>
#include <stdlib.h>

// Required to enable kbhit type functionality
#if defined( _WIN32 ) || defined( WIN32 )
#	include <conio.h>
#	include <io.h>
#else
#	include <termios.h>
#endif

// Initialise the vocab
#include <rpany/vocab/__init__.hpp>
