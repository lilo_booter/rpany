#pragma once

namespace parse {

class parser_parse : public parser
{
	public:
		bool parse( stack &s, const rp::any &token )
		{
			const auto string = if_string( token );

			if ( !closed && first && words.size( ) == 0 && string != "[[" )
			{
				words.push_back( token );
				closed = true;
				return false;
			}

			first = false;

			if ( !closed && string == "[[" )
			{
				if ( nested ++ ) words.push_back( token );
				return false;
			}

			if ( !closed && string == "]]" )
			{
				closed = -- nested == 0;
				if ( !closed ) words.push_back( token );
				return false;
			}

			if ( !closed )
			{
				words.push_back( token );
				return false;
			}

			parsed = token;

			return true;
		}

		virtual void execute( stack &s )
		{
			s += parsed;
			for ( auto iter = words.begin( ); iter != words.end( ); iter ++ )
				s.push( *iter );
		}

	protected:
		bool first = true;
		bool closed = false;
		int nested = 0;
		word_list words;
		rp::any parsed;
};


RPANY_OPEN( parse, stack &s )
	s.teach( "parse", [ ] ( stack &s, const std::string &token ) { s.set_parser( new parser_parse ); } );
RPANY_CLOSE( parse );

}
