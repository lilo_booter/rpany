#pragma once

namespace dump {

inline void dump_long( stack &s )
{
	scoped_block block( s );
	int count = static_cast< int >( s.depth( ) );
	for ( int index = - count ; index < 0; index ++ )
		s.output( ) << std::abs( index + 1 ) << ": '" << as_string( s.pick( index ) ) << "'" << std::endl;
	const word_list pending = s.pending( );
	for ( auto iter = pending.begin( ); iter != pending.end( ); iter ++ )
		s.output( ) << "*: '" << as_string( *iter ) << "'" << std::endl;
}

inline void dump_short( stack &s )
{
	scoped_block block( s );
	int count = static_cast< int >( s.depth( ) );
	for ( int index = - count ; index < 0; index ++ )
		s.output( ) << "'" << as_string( s.pick( index ) ) << "' ";
	const word_list pending = s.pending( );
	for ( auto iter = pending.begin( ); iter != pending.end( ); iter ++ )
		s.output( ) << "[ '" << as_string( *iter ) << "' ] ";
	count += static_cast< int >( pending.size( ) );
	if ( count != 0 ) s.output( ) << std::endl;
}

inline void dump_dot( stack &s )
{
	scoped_block block( s );
	int count = static_cast< int >( s.depth( ) );
	for ( int index = - count ; index < 0; index ++ )
		s.output( ) << as_string( s.pick( index ) ) << std::endl;
	const word_list pending = s.pending( );
	for ( auto iter = pending.begin( ); iter != pending.end( ); iter ++ )
		s.output( ) << as_string( *iter ) << std::endl;
	s.pop( count );
}

inline void dup_non_zero( stack &s )
{
	double value = convert< double >( s.pick( ) );
	if ( value != 0.0 )
		s += s.pick( 0 );
}

RPANY_OPEN( stack, stack &s )
	s.teach( "depth", [ ] ( stack &s ) { s += s.depth( ); } );
	s.teach( "drop", [ ] ( stack &s ) { s.pop( ); } );
	s.teach( "pick", [ ] ( stack &s ) { s += s.pick( s.pull< int >( ) ); } );
	s.teach( "roll", [ ] ( stack &s ) { s.roll( s.pull< int >( ) ); } );
	s.teach( "wipe", [ ] ( stack &s ) { s.pop( s.pull< int >( ) ); } );
	s.teach( "clear", [ ] ( stack &s ) { s.pop( static_cast< int >( s.depth( ) ) ); } );
	s.teach( "dup", [ ] ( stack &s ) { s += s.pick( ); } );
	s.teach( "over", [ ] ( stack &s ) { s += s.pick( 1 ); } );
	s.teach( "swap", [ ] ( stack &s ) { s.roll( 1 ); } );
	s.teach( "rot", [ ] ( stack &s ) { s.roll( 2 ); } );
	s.teach( "2dup", [ ] ( stack &s ) { s += s.pick( 1 ); s += s.pick( 1 ); } );
	s.teach( "-rot", [ ] ( stack &s ) { s.roll( 2 ); s.roll( 2 ); } );
	s.teach( "nip", [ ] ( stack &s ) { s.roll( 1 ); s.pop( 1 ); } );
	s.teach( "tuck", [ ] ( stack &s ) { s.roll( 1 ); s += s.pick( 1 ); } );
	s.teach( "2drop", [ ] ( stack &s ) { s.pop( 2 ); } );
	s.teach( "2swap", [ ] ( stack &s ) { s.roll( 2 ); s.roll( 3 ); s.roll( 2 ); s.roll( 3 ); } );
	s.teach( "2over", [ ] ( stack &s ) { s += s.pick( 3 );  s += s.pick( 3 ); } );
	s.teach( "2nip", [ ] ( stack &s ) { s.pull( 3 );  s.pull( 2 ); } );
	s.teach( "2tuck", [ ] ( stack &s ) { s.roll( 3 ); s.roll( 3 ); s += s.pick( 3 ); s += s.pick( 3 ); } );
	s.teach( "2rot", [ ] ( stack &s ) { s.roll( 5 ); s.roll( 5 ); } );
	s.teach( "?dup", dup_non_zero );
RPANY_CLOSE( stack );

RPANY_OPEN( debug, stack &s )
	s.teach( "dump-long", dump_long );
	s.teach( "dump-short", dump_short );
	s.teach( "dump", { "dump-long" } );
	s.teach( "dump.", dump_dot );
	s.teach( "trace", [ ] ( stack &s ) { s.trace( s.pull< int >( ) ); } );
RPANY_CLOSE( debug );

}
