#pragma once

namespace conditional {

class parser_if : public parser
{
	public:
		parser_if( const std::string &split = "else", const std::string &end = "then" )
		: split_( split )
		, end_( end )
		{ }

		bool parse( stack &s, const rp::any &token )
		{
			const std::string word( if_string( token ) );

			// Check if we're at the end
			const bool done = word == end_;

			if ( !done )
			{
				// Handle the else word here
				if ( word == split_ )
				{
					if ( in_if_ == false ) RPANY_THROW( stack_exception, "Already encountered an else in this if condition" );
					in_if_ = false;
				}
				else if ( !s.inline_parser( inline_list( ), token ) )
				{
					word_list &list = in_if_ ? if_ : else_;
					list.push_back( token );
				}
			}

			return done;
		}

		void execute( stack &s )
		{
			s.execute( s.pull< bool >( ) ? if_ : else_ );
		}

		void optimise( stack &s )
		{
			if_ = s.optimise( if_ );
			else_ = s.optimise( else_ );
		}

	private:
		const std::string split_;
		const std::string end_;
		bool in_if_ = true;
		word_list if_;
		word_list else_;
};

RPANY_OPEN( if, stack &s )
	s.teach( "if", [ ] ( stack &s, const std::string &token ) { s.set_parser( new parser_if ); } );
	s.teach( "?{", [ ] ( stack &s, const std::string &token ) { s.set_parser( new parser_if( "}:{", "}" ) ); } );
	s.teach( RPANY_COMPILE_ONLY( stack &s, "else" ) );
	s.teach( RPANY_COMPILE_ONLY( stack &s, "then" ) );
	s.teach( RPANY_COMPILE_ONLY( stack &s, "}:{" ) );
	s.teach( RPANY_COMPILE_ONLY( stack &s, "}" ) );
RPANY_CLOSE( if );

}
