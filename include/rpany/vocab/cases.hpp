#pragma once

namespace cases {

// Parser evaluates:
//
// CASE [ CONDITION <tokens> OPEN <tokens> CLOSE ]* [ DEFAULT OPEN <tokens> CLOSE ] ESAC

const std::string CASE = "&{";
const std::string CONDITION = "+>";
const std::string DEFAULT = "*>";
const std::string DEFAULT_ALT = "->";
const std::string OPEN = "{{";
const std::string CLOSE = "}}";
const std::string ESAC = "&}";

// Implementation needs to track state
enum state_t
{
	s_case,
	s_condition,
	s_default,
	s_open_condition,
	s_open_default
};

// A case is defined as a pair of token lists
struct case_t
{
	word_list condition;
	word_list body;

	void optimise( stack &s )
	{
		condition = s.optimise( condition );
		body = s.optimise( body );
	}
};

// Collection type for cases
typedef std::vector< case_t > cases_t;

class cases_parser : public parser
{
	public:
		// Invoked by stack 'push' method when this parser is active
		bool parse( stack &s, const rp::any &token )
		{
			const std::string word( if_string( token ) );
			const bool done = is_done( word );
			if ( !done )
				if ( state_ == s_case || !s.inline_parser( inline_list( ), token ) )
					handle( token, word );
			return done;
		}

		bool is_done( const std::string &word )
		{
			return state_ == s_case && word == ESAC;
		}

		void handle( const rp::any &token, const std::string &word )
		{
			switch( state_ )
			{
				case s_case:
					if ( word == CONDITION ) { cases_.push_back( case_t( ) ); state_ = s_condition; }
					else if ( word == DEFAULT && !default_used_ ) { default_used_ = 1; state_ = s_default; }
					else if ( word == DEFAULT_ALT && !default_used_ ) { default_used_ = -1; state_ = s_default; }
					else if ( word == DEFAULT || word == DEFAULT_ALT ) { RPANY_THROW( stack_exception, "Default case specified multiple times" ); }
					else RPANY_THROW( stack_exception, "Invalid token - must be " + CONDITION + ", " + DEFAULT + ", " + DEFAULT_ALT + " or " + ESAC );
					break;

				case s_condition:
					if ( word == OPEN ) state_ = s_open_condition;
					else cases_.back( ).condition.push_back( token );
					break;

				case s_default:
					if ( word == OPEN ) state_ = s_open_default;
					else RPANY_THROW( stack_exception, "Invalid token - must be " + OPEN );
					break;

				case s_open_condition:
					if ( word == CLOSE ) state_ = s_case;
					else cases_.back( ).body.push_back( token );
					break;

				case s_open_default:
					if ( word == CLOSE ) state_ = s_case;
					else default_.push_back( token );
					break;
			};
		}

		// Invoked when execution is required
		void execute( stack &s )
		{
			rp::any tos = s.pull( );
			bool done = false;
			for( auto iter = cases_.begin( ); !done && iter != cases_.end( ); iter ++ )
			{
				s += tos;
				s.execute( iter->condition );
				done = s.pull< bool >( );
				if ( done )
					s.execute( iter->body );
			}
			if ( !done && default_used_ )
			{
				if ( default_used_ > 0 ) s += tos;
				s.execute( default_ );
			}
		}

		void optimise( stack &s )
		{
			for ( auto iter = cases_.begin( ); iter != cases_.end( ); iter ++ )
				( *iter ).optimise( s );
			default_ = s.optimise( default_ );
		}

	private:
		state_t state_ = s_case;
		cases_t cases_;
		int default_used_ = 0;
		word_list default_;
};

RPANY_OPEN( cases, stack &s )
	s.teach( CASE, [ ] ( stack &s, const std::string &token ) { s.set_parser( new cases_parser ); } );
	s.teach( RPANY_COMPILE_ONLY( stack &s, CONDITION ) );
	s.teach( RPANY_COMPILE_ONLY( stack &s, DEFAULT ) );
	s.teach( RPANY_COMPILE_ONLY( stack &s, DEFAULT_ALT ) );
	s.teach( RPANY_COMPILE_ONLY( stack &s, OPEN ) );
	s.teach( RPANY_COMPILE_ONLY( stack &s, CLOSE ) );
	s.teach( RPANY_COMPILE_ONLY( stack &s, ESAC ) );
RPANY_CLOSE( cases );

}
