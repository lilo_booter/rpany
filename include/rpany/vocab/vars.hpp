#pragma once

namespace vars {

void create( stack &s, const std::string &name )
{
	if ( s.vfind( name ) == s.vend( ) )
		s[ name ] = var_ptr( new variable( name ) );
}

void acquire( stack &s )
{
	const std::string name = s.pull< std::string >( );
	create( s, name );
	s += s[ name ];
}

void assign( stack &s )
{
	var_ptr var = s.pull< var_ptr >( );
	auto value = s.pull( );
	if ( is< var_ptr >( value ) )
		var->value = as< var_ptr >( value )->value;
	else
		var->value = value;
}

void deref( stack &s )
{
	var_ptr var = s.pull< var_ptr >( );
	s += var->value;
}

enum var_use
{
	v_create,
	v_assign,
	v_deref,
	v_forget,
};

class parser_variable : public parser
{
	public:
		parser_variable( const var_use &use = v_create )
		: use_( use )
		{ }

		bool parse( stack &s, const rp::any &token )
		{
			name_ = if_string( token );
			return true;
		}

		void execute( stack &s )
		{
			switch( use_ )
			{
				case v_create:
					s.teach( name_, { "$", name_, acquire } );
					create( s, name_ );
					break;
				case v_assign:
					s.teach( name_, { "$", name_, acquire } );
					s << name_ << assign;
					break;
				case v_deref:
					s << name_ << "@";
					break;
				case v_forget:
					s << "forget" << name_;
					break;
			}
		}

	protected:
		const var_use use_;
		std::string name_;
};

const std::string var_string( const rp::any &any )
{
	std::string result;

	if ( ANY_EMPTY( any ) )
		result = "<unnassigned>";
	else if ( is< var_ptr >( any ) )
		result = as_string( as< var_ptr >( any )->value );
	else
		result = as_string( any );

	return result;
}

void see_variables( stack &s )
{
	scoped_block block( s );
	for( auto iter = s.vbegin( ); iter != s.vend( ); iter ++ )
		s.output( ) << iter->first << " = " << std::move( var_string( iter->second ) ) << std::endl;
}

inline rp::any var_to_string( const rp::any &any )
{
	std::ostringstream st;
	auto var = rpany::as< var_ptr >( any );
	st << "<var-" << var->name << ">";
	return std::string( st.str( ) );
}

RPANY_OPEN( vars, stack &s )
	s.teach( "variable", [] ( stack &s, const std::string &t ) { s.set_parser( new parser_variable ); } );
	s.teach( "variable!", [] ( stack &s, const std::string &t ) { s.set_parser( new parser_variable( v_assign ) ); } );
	s.teach( "@", deref );
	s.teach( "!", assign );
	s.teach( "see-variables", see_variables );
	coercion( COERCE_KEY( var_ptr, std::string ), var_to_string );
RPANY_CLOSE( vars );

inline void local( stack &s, const std::string &name )
{
	if ( name == "" ) RPANY_THROW( rpany::undefined_exception, "Invalid token for local variable name" );
	s.validate( name );
	auto &frame = s.get_stack_frame( );
	frame[ name ] = s.pull( );
}

inline void locals( stack &s )
{
	auto vector = s.pull< vector_ptr >( );
	for ( auto iter = vector->rbegin( ); iter != vector->rend( ); iter ++ )
		local( s, as< std::string >( *iter ) );
}

inline void see_local( stack &s )
{
	scoped_block block( s );
	auto &frame = s.get_stack_frame( );
	for( auto it = frame.begin( ); it != frame.end( ); it ++ )
		s.output( ) << it->first << " = " << std::move( var_string( it->second ) ) << std::endl;
}

inline void see_locals( stack &s )
{
	scoped_block block( s );
	auto &frames = s.get_all_stack_frames( );
	size_t count = 0;
	for( auto frame : frames )
	{
		if ( frame.size( ) != 0 )
		{
			s.output( ) << count << ":" << std::endl;
			for( auto it = frame.begin( ); it != frame.end( ); it ++ )
				s.output( ) << "  " << it->first << " = " << std::move( var_string( it->second ) ) << std::endl;
		}
		count ++;
	}
}

inline void is_local( stack &s )
{
	auto name = s.pull< std::string >( );
	scoped_block block( s );
	auto &frames = s.get_all_stack_frames( );
	bool result = false;
	for( auto frame : frames )
		if ( frame.find( name ) != frame.end( ) )
			result = true;
	s += result;
}

RPANY_OPEN( locals, stack &s )
	s.teach( "locals", locals );
	s.teach( "local", create_parse_token< local > );
	s.teach( "see-local", see_local );
	s.teach( "see-locals", see_locals );
	s.teach( "is-local", is_local );
RPANY_CLOSE( locals );

}
