#pragma once

namespace format {

typedef std::deque< char > specifiers_t;
const std::string types = "cdfgsxo";

inline bool parse_specifier( const std::string &format, size_t &pos, specifiers_t &specifiers )
{
	size_t t = pos + 1;
	if ( format[ t ] == '-' ) t ++;
	while ( isdigit( format[ t ] ) ) t ++;
	if ( format[ t ] == '.' )
	{
		t ++;
		while ( isdigit( format[ t ] ) ) t ++;
	}
	if ( types.find( format[ t ] ) != std::string::npos )
	{
		specifiers.push_back( format[ t ] );
		pos = t + 1;
		return true;
	}
	return false;
}

inline void collect_specifiers( const std::string &format, specifiers_t &specifiers )
{
	// Count how many stack items we need to derive the string
	auto pos = format.find( "%" );
	while ( pos != std::string::npos )
	{
		const auto next = format[ pos + 1 ];
		if ( next == '%' )
			pos += 2;
		else if ( !parse_specifier( format, pos, specifiers ) )
			RPANY_THROW( stack_exception, "Malformed format string of '" + format + "' - only %[" + types + "] and %% supported" );
		pos = format.find( '%', pos );
	}
}

inline std::string format_string( stack &s, const std::string &format, bool peek = false )
{
	specifiers_t specifiers;
	collect_specifiers( format, specifiers );
	rp::format fmt( format );
	int count = static_cast< int >( specifiers.size( ) );
	for( int index = count; index --; specifiers.pop_front( ) )
	{
		switch( specifiers.front( ) )
		{
			case 'c': fmt % static_cast< char >( s.coerce< int >( index ) ); break;
			case 'd': fmt % s.coerce< int >( index ); break;
			case 'f': fmt % s.coerce< double >( index ); break;
			case 'g': fmt % s.coerce< double >( index ); break;
			case 's': fmt % s.coerce< std::string >( index ); break;
			case 'x': fmt % s.coerce< size_t >( index ); break;
			case 'o': fmt % s.coerce< size_t >( index ); break;
		}
	}
	if ( !peek ) s.pop( count );
	return fmt.str( );
}

inline std::string consume_string( stack &s, const std::string &format )
{
	return format_string( s, format );
}

inline std::string peek_string( stack &s, const std::string &format )
{
	return format_string( s, format, true );
}

inline void consume( stack &s, const std::string &token )
{
	s += consume_string( s, token );
}

inline void consume_print( stack &s, const std::string &token )
{
	s += consume_string( s, token );
	s << ".";
}

inline void consume_echo( stack &s, const std::string &token )
{
	s += consume_string( s, token );
	s << ",";
}

inline void peek( stack &s, const std::string &token )
{
	s += peek_string( s, token );
}

inline void peek_print( stack &s, const std::string &token )
{
	s += peek_string( s, token );
	s << ".";
}

inline void peek_echo( stack &s, const std::string &token )
{
	s += peek_string( s, token );
	s << ",";
}

inline void fprintf( stack &s )
{
	s += consume_string( s, s.pull< std::string >( ) );
}

inline void fpeek( stack &s )
{
	s += peek_string( s, s.pull< std::string >( ) );
}

RPANY_OPEN( format, stack &s )
	s.teach( "fprint", fprintf );
	s.teach( "fpeek", fpeek );
	s.teach( "f$", create_parse_token< consume > );
	s.teach( "f.", create_parse_token< consume_print > );
	s.teach( "f,", create_parse_token< consume_echo > );
	s.teach( "fp$", create_parse_token< peek > );
	s.teach( "fp.", create_parse_token< peek_print > );
	s.teach( "fp,", create_parse_token< peek_echo > );
RPANY_CLOSE( format );

}
