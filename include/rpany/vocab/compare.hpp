#pragma once

namespace compare {

RPANY_OPEN( compare, stack &s )
	s.teach( "==", [] ( stack &s ) { s += s.pull< std::string >( ) == s.pull< std::string >( ); } );
	s.teach( "!=", [] ( stack &s ) { s += s.pull< std::string >( ) != s.pull< std::string >( ); } );
	s.teach( ">", [] ( stack &s ) { auto b = s.pull< double >( ); s += s.pull< double >( ) > b; } );
	s.teach( "<", [] ( stack &s ) { auto b = s.pull< double >( ); s += s.pull< double >( ) < b; } );
	s.teach( ">=", [] ( stack &s ) { auto b = s.pull< double >( ); s += s.pull< double >( ) >= b; } );
	s.teach( "<=", [] ( stack &s ) { auto b = s.pull< double >( ); s += s.pull< double >( ) <= b; } );
RPANY_CLOSE( compare );

RPANY_OPEN( bools, stack &s )
	s.teach( "&&", [] ( stack &s ) { auto b = s.pull< bool >( ); s += s.pull< bool >( ) && b; } );
	s.teach( "||", [] ( stack &s ) { auto b = s.pull< bool >( ); s += s.pull< bool >( ) || b; } );
	s.teach( "not", [] ( stack &s ) { auto a = s.pull< bool >( ); s += !a; } );
RPANY_CLOSE( bools );

RPANY_OPEN( bits, stack &s )
	s.teach( "<<", [] ( stack &s ) { auto b = s.pull< int >( ); s += ( s.pull< int >( ) << b ); } );
	s.teach( ">>", [] ( stack &s ) { auto b = s.pull< int >( ); s += ( s.pull< int >( ) >> b ); } );
	s.teach( "&", [] ( stack &s ) { auto b = s.pull< int >( ); s += ( s.pull< int >( ) & b ); } );
	s.teach( "|", [] ( stack &s ) { auto b = s.pull< int >( ); s += ( s.pull< int >( ) | b ); } );
	s.teach( "^", [] ( stack &s ) { auto b = s.pull< int >( ); s += ( s.pull< int >( ) ^ b ); } );
	s.teach( "~", [] ( stack &s ) { s += ( ~ s.pull< int >( ) ); } );
RPANY_CLOSE( bits );

}
