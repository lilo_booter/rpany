#pragma once

RPANY_OPEN( substack, stack &s )
	typedef std::shared_ptr< stack > ptr;
	substack::create< stack >( s );
	coercion( COERCE_KEY( ptr, std::string ), [] ( const rp::any & ) { return rp::any( std::string( "<ss>" ) ); } );
RPANY_CLOSE( substack );
