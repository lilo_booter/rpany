#pragma once

namespace env {

RPANY_OPEN( env, stack &s )
	s.teach( "hasenv", [] (stack &s ) { s += 0 != std::getenv( s.coerce< std::string >( ).c_str( ) ); } );
	s.teach( "getenv", [] (stack &s ) { auto e = std::getenv( s.pull< std::string >( ).c_str( ) ); s += e ? e : ""; } );
RPANY_CLOSE( env );

}
