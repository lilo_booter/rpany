#pragma once

namespace vector {

void create( stack &s )
{
	s += vector_ptr( new vector_type );
}

void append( stack &s )
{
	auto item = s.pull( );
	s.cast< vector_ptr >( )->push_back( item );
}

void join( stack &s )
{
	auto vector = s.pull< vector_ptr >( );
	auto result = s.cast< vector_ptr >( );
	result->reserve( result->size( ) + vector->size( ) );
	result->insert( result->end( ), vector->begin( ), vector->end( ) );
}

void remove( stack &s )
{
	auto item = s.pull< size_t >( );
	auto vector = s.cast< vector_ptr >( );
	vector->erase( vector->begin( ) + item );
}

void size( stack &s )
{
	s += s.cast< vector_ptr >( )->size( );
}

void query( stack &s )
{
	auto index = s.pull< size_t >( );
	s += ( *s.cast< vector_ptr >( ) )[ index ];
}

void list_to_vector( stack &s )
{
	vector_ptr vector;
	s.pop( vector );
	s += vector;
}

void vector_to_list( stack &s )
{
	auto vector = s.pull< vector_ptr >( );
	for ( auto it : *vector )
		s += it;
	s += vector->size( );
}

void vector_clone( stack &s )
{
	auto src = s.cast< vector_ptr >( );
	auto dst = vector_ptr( new vector_type );
	for ( auto it : *src )
		dst->push_back( it );
	s += dst;
}

class parser_vector : public parser
{
	public:
		parser_vector( const std::string &start = "{", const std::string &end = "}" )
		: start_( start )
		, end_( end )
		{
			result << create;
			nested_ ++;
		}

		bool parse( stack &s, const rp::any &token )
		{
			const std::string word = if_string( token );
			const bool done = word == end_ && nested_ == 1;
			if ( !done )
			{
				if ( word == start_ )
				{
					nested_ ++;
					result << create;
				}
				else if ( word == end_ )
				{
					nested_ --;
					result << append;
				}
				else
				{
					result += convert< std::string >( token );
					result << append;
				}
			}
			else if ( result.depth( ) > 1 )
			{
				result << append;
			}
			return done;
		}

		void execute( stack &s )
		{
			result << vector_clone;
			s += result.cast< vector_ptr >( );
		}

	protected:
		rpany::stack result;
		const std::string start_;
		const std::string end_;
		int nested_ = 0;
};

std::string vector_delimit( stack &s, const vector_ptr &vector )
{
	std::ostringstream out;
	for ( auto it = vector->begin( ); it != vector->end( ); it ++ )
	{
		if ( is< vector_ptr >( *it ) )
			out << vector_delimit( s, as< vector_ptr >( *it ) );
		else
			out << quoted( convert< std::string >( *it ) ) << ( it + 1 != vector->end( ) ? ',' : '\n' );
	}
	if ( vector->empty( ) ) out << '\n';
	return out.str( );
}

void vector_delimit( stack &s )
{
	auto vector = s.pull< vector_ptr >( );
	s.output( ) << vector_delimit( s, vector );
	s.output( ) << '\n';
}

bool any_compare( const rp::any &a, const rp::any &b )
{
	return as< std::string >( a ) < as< std::string >( b );
}

void vector_sort( stack &s )
{
	auto vector = s.cast< vector_ptr >( );
	std::sort( vector->begin( ), vector->end( ), any_compare );
}

RPANY_OPEN( vector, stack &s )
	coercion( COERCE_KEY( vector_ptr, std::string ), [] ( const rp::any & ) { return rp::any( std::string( "<vector>" ) ); } );

	s.teach( "vector-new", create );
	s.teach( "vector-append", append );
	s.teach( "vector-join", join );
	s.teach( "vector-remove", remove );
	s.teach( "vector-size", size );
	s.teach( "vector-query", query );
	s.teach( "list-to-vector", list_to_vector );
	s.teach( "vector-to-list", vector_to_list );
	s.teach( "vector-sort", vector_sort );
	s.teach( "vector-clone", vector_clone );

	s.teach( "{", [ ] ( stack &s, const std::string &token ) { s.set_parser( new parser_vector ); } );
	s.teach( "vector-delimit", vector_delimit );

	s.evaluate( ": vector-describe dup for i . next ;" );
	s.evaluate( ": vector-join-list list-to-vector vector-join ;" );
RPANY_CLOSE( vector );

}
