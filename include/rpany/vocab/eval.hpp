#pragma once

namespace evaluate {

#ifdef _WIN32
std::string get_exe_path( )
{
	std::string output;
	TCHAR t_result[ MAX_PATH ];
	if ( !GetModuleFileName( NULL, t_result, MAX_PATH ) )
		RPANY_THROW( stack_exception, "Could not get path from GetModuleFileName() function." );
#ifdef UNICODE
	char result[ MAX_PATH ];
	wcstombs( result, t_result, wcslen( t_result ) + 1 );
#else
	char *result = static_cast< char * >( t_result );
#endif
	output = std::string( result );
	auto i = output.rfind( '\\' );
	if ( i > 0 ) i = output.rfind( '\\', i - 1 );
	if ( i != std::string::npos )
		output = output.substr( 0, i );
	return output;
}
#elif __APPLE__
std::string get_exe_path( )
{
	std::string output;
	char result[ PATH_MAX ];
	uint32_t bufsize = sizeof( result );
	if ( _NSGetExecutablePath( result, &bufsize ) != 0 )
		RPANY_THROW( stack_exception, "_NSGetExecutablePath returns wrong value. Buffer is not large enough." );
	output = std::string( result );
	auto i = output.rfind( '/' );
	if ( i != std::string::npos )
		output = output.substr( 0, i );
	return output;
}
#else
std::string get_exe_path( )
{
	std::string output;
	char result[ PATH_MAX ];
	size_t size = readlink( "/proc/self/exe", result, PATH_MAX );
	if ( !( size > 0 && size < PATH_MAX - 1 ) )
		RPANY_THROW( stack_exception, "Unable to determine path of executable" );
	result[ size ] = '\0';
	output = std::string( result );
	auto i = output.rfind( '/' );
	if ( i != std::string::npos )
		output = output.substr( 0, i );
	return output;
}
#endif

inline void find_file( std::ifstream &result, const std::string &input )
{
	// Strip any leading prefix if specified
	const std::string filename = input.find( "file:" ) == 0 ? input.substr( 5 ) : input;

	// Check for a relative path first
	result.open( filename );

	// Current RPANY_PATH contains a single directory
	// FIXME: should allow multiple with os specific seperator and corresponding loop
	const char *path = std::getenv( "RPANY_PATH" );
	if ( !result.good( ) && path )
		result.open( std::string( path ) + "/" + filename );

	// Check from exeutable path
	std::string exe_path = get_exe_path( );
	if ( !result.good( ) )
		result.open( exe_path + "/" + filename );

	// FIXME: This is potentially problematic if file proivdes a __main__ - detect and
	// reject that?
	const char *home = std::getenv( "HOME" );
	if ( !result.good( ) && home )
		result.open( std::string( home ) + "/bin/" + filename );

	// Finally check for the install prefix
	if ( !result.good( ) )
		result.open( std::string( RPANY_PREFIX "/lib/rpany/" ) + filename );

	// All good or give up at this point
	if ( !result.good( ) )
		RPANY_THROW( stack_exception, "Unable to find '" + filename + "'" );
	else if ( !is_script( result ) )
		RPANY_THROW( stack_exception, "Invalid script file '" + filename + "'" );
}

inline void eval_file( stack &s, const std::string &filename )
{
	std::ifstream file;
	find_file( file, filename );
	eval( s, file );
	if ( s.is_parsing( ) )
		RPANY_THROW( stack_exception, "Stack in parsing state after loading '" + filename + "'" );
}

inline void eval_stdin( stack &s )
{
	eval( s, s.input( ) );
}

inline void study( stack &s, const std::string &token )
{
	eval_file( s, token );
}

inline void eval_list( stack &s )
{
	s << "list-to-vector";
	auto vector = s.pull< vector_ptr >( );
	for ( auto iter : *vector )
		s << as< std::string >( iter );
}

RPANY_OPEN( eval, stack &s )
	using namespace evaluate;
	s.teach( "study", create_parse_token< study > );
	s.teach( "eval", [] ( stack &s ) { eval( s, s.pull< std::string >( ), true ); } );
	s.teach( "exec", [] ( stack &s ) { auto token = s.pull< std::string >( ); s << token; } );
	s.teach( "eval-word?", [] ( stack &s ) { auto word = s.pull< std::string >( ); if ( s.is_word( word ) ) eval( s, word ); } );
	s.teach( "eval-file", [] ( stack &s ) { eval_file( s, s.pull< std::string >( ) ); } );
	s.teach( "eval-files", [] ( stack &s ) { auto count = s.pull< int >( ); for( auto index = count; -- index >= 0; ) eval_file( s, s.coerce< std::string >( index ) ); s.pop( count ); } );
	s.teach( "eval-stdin", eval_stdin );
	s.teach( "eval-list", eval_list );
RPANY_CLOSE( eval );

}
