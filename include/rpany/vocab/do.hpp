#pragma once

namespace loop {

typedef state_template< rp::any > loop_state_type;
typedef std::shared_ptr< loop_state_type > loop_state_ptr;
typedef state_instance< rp::any > loop_state_instance;

const std::string _loop_name( "_loop_state" );

inline loop_state_type &loop_state( stack &s )
{
	rp::any any;
	if ( !s.is_local_var( _loop_name, any ) )
	{
		auto ptr = loop_state_ptr( new loop_state_type( ) );
		s.get_stack_frame( )[ _loop_name ] = ptr;
		return *ptr;
	}
	return *as< loop_state_ptr >( any );
}

class parser_do : public parser
{
	public:
		bool parse( stack &s, const rp::any &token )
		{
			const std::string word( if_string( token ) );
			const bool done = is_end( word );
			if ( done )
				terminator_ = word;
			else if ( !s.inline_parser( inline_list( ), token ) )
				list_.push_back( token );
			return done;
		}

		bool is_end( const std::string &word )
		{
			return word == "loop" || word == "+loop";
		}

		void execute( stack &s )
		{
			const double start = s.pull< double >( );
			const double end = s.pull< double >( );

			try
			{
				loop_state_instance instance( loop_state( s ), start );
				rp::any *index = instance.ptr( );
				double *value = rp::any_cast< double >( index );

				const bool stack_increment = terminator_ == "+loop";
				double increment = end > start ? 1.0 : -1.0;

				for ( *value = start; *value != end ; *value += increment )
				{
					s.execute( list_ );
					if ( stack_increment ) increment = s.pull< double >( );
				}
			}
			catch( leave_exception & )
			{
				// ignore
			}
		}

		void optimise( stack &s )
		{
			list_ = s.optimise( list_ );
		}

	private:
		std::string terminator_;
		word_list list_;
};

class parser_for : public parser
{
	public:
		bool parse( stack &s, const rp::any &token )
		{
			// Convert token to a string
			const std::string word( if_string( token ) );

			// This parser will terminate when 'next' is obtained
			const bool done = word == "next";

			// Add token to list unless done or inline
			if ( !done && !s.inline_parser( inline_list( ), token ) )
				list_.push_back( token );

			return done;
		}

		void execute( stack &s )
		{
			// We'll be flexible here and convert lists to vectors if necessary
			rp::any tos = s.pick( );

			// A list is a number of items followed by an int count - convert to vector here
			if ( is_numeric( tos ) )
				s << "list-to-vector";
			else if ( !is< vector_ptr >( tos ) )
				RPANY_THROW( stack_exception, "for/next accepts a list or a vector at top of stack" );

			// We'll pull the vector now
			const auto vector = s.pull< vector_ptr >( );

			try
			{
				// Create our iterator for this loop
				loop_state_instance instance( loop_state( s ), rp::any( ) );
				auto &it = instance.pick( );

				// Iterate through the vector, pushing each item to the stack before executing
				// the list of tokens parsed
				for ( auto index = vector->begin( ); index != vector->end( ) ; index ++ )
				{
					it = *index;
					s.execute( list_ );
				}
			}
			catch( leave_exception & )
			{
				// ignore
			}
		}

		void optimise( stack &s )
		{
			list_ = s.optimise( list_ );
		}

	private:
		word_list list_;
};

class parser_begin : public parser
{
	public:
		bool parse( stack &s, const rp::any &token )
		{
			const std::string word( if_string( token ) );
			const bool done = cond_.size( ) ? word == "repeat" : word == "until" || word == "again";
			if ( done )
				terminal_ = word;
			else if ( word == "while" && !cond_.size( ) )
			{
				cond_ = list_;
				list_ = { };
			}
			else if ( word == "while" )
				RPANY_THROW( stack_exception, "Repeated 'while' in begin/while/repeat loop" );
			else if ( !s.inline_parser( inline_list( ), token ) )
				list_.push_back( token );
			return done;
		}

		void execute( stack &s )
		{
			bool finished = false;
			const bool terminating = terminal_ == "until";

			try
			{
				do
				{
					if ( cond_.size( ) )
					{
						s.execute( cond_ );
						if ( !s.pull< bool >( ) )
							break;
					}
					s.execute( list_ );
					if ( terminating )
						finished = s.pull< bool >( );
				}
				while ( !finished );
			}
			catch( leave_exception & )
			{
				// do nothing
			}
		}

		void optimise( stack &s )
		{
			cond_ = s.optimise( cond_ );
			list_ = s.optimise( list_ );
		}

	private:
		std::string terminal_;
		word_list cond_;
		word_list list_;
};

RPANY_OPEN( loops, stack &s )
	s.teach( "begin", [ ] ( stack &s, const std::string &token ) { s.set_parser( new parser_begin ); } );
	s.teach( "do", [ ] ( stack &s, const std::string &token ) { s.set_parser( new parser_do ); } );
	s.teach( "for", [ ] ( stack &s, const std::string &token ) { s.set_parser( new parser_for ); } );
	s.teach( "leave", [ ] ( stack &s ) { RPANY_THROW_DEFAULT( leave_exception ); } );
	s.teach( "error", [ ] ( stack &s ) { auto str = s.pull< std::string >( ); RPANY_THROW( stack_exception, str ); } );
	s.teach( "loops?", [ ] ( stack &s ) { s += loop_state( s ).size( ); } );
	s.teach( "index?", [ ] ( stack &s ) { s += loop_state( s ).pick( s.pull< size_t >( ) ); } );
	s.teach( "i", [ ] ( stack &s ) { s += loop_state( s ).pick( ); } );
	s.teach( "j", [ ] ( stack &s ) { s += loop_state( s ).pick( 1 ); } );
	s.teach( "k", [ ] ( stack &s ) { s += loop_state( s ).pick( 2 ); } );
	// Placeholders for loop terminators
	s.teach( "loop", [ ] ( stack &s ) { RPANY_THROW( stack_exception, "Interpreting a compile only word 'loop'" ); } );
	s.teach( "+loop", [ ] ( stack &s ) { RPANY_THROW( stack_exception, "Interpreting a compile only word '+loop'" ); } );
	s.teach( "next", [ ] ( stack &s ) { RPANY_THROW( stack_exception, "Interpreting a compile only word 'next'" ); } );
	s.teach( "while", [ ] ( stack &s ) { RPANY_THROW( stack_exception, "Interpreting a compile only word 'while'" ); } );
	s.teach( "again", [ ] ( stack &s ) { RPANY_THROW( stack_exception, "Interpreting a compile only word 'again'" ); } );
	s.teach( "until", [ ] ( stack &s ) { RPANY_THROW( stack_exception, "Interpreting a compile only word 'until'" ); } );
	// Deprecated - use i, j, k, index? and loops? instead
	s.teach( "it", [ ] ( stack &s ) { s += loop_state( s ).pick( ); } );
	s.teach( "it?", [ ] ( stack &s ) { s += loop_state( s ).pick( s.pull< size_t >( ) ); } );
RPANY_CLOSE( loops );

}
