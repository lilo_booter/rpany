#pragma once

namespace vocabs {

size_t get_max_width( stack &s )
{
	size_t result = 132;
	if ( s.is_word( std::string( "report-max-width" ) ) )
		s << "report-max-width" >> result;
	else if ( s.vfind( "report-max-width" ) != s.vend( ) )
		result = as< int >( s[ "report-max-width" ] );
	return result;
}

struct vocab_utils
{
	typedef dictionary::dictionary_type::vocab_type vocab_t;
	typedef dictionary::dictionary_type::vocab_iterator vocab_iter_t;
	typedef dictionary::dictionary_type::list list_t;

	stack &s;
	const vocab_t &vocabs;
	size_t max_width = 132;

	vocab_utils( stack &s_ )
	: s( s_ )
	, vocabs( s.vocabs( ) )
	, max_width( get_max_width( s ) )
	{ }

	void see_vocab( const vocab_iter_t &viter, size_t width = 0 ) const
	{
		if ( viter == vocabs.end( ) ) return;

		std::string name = viter->first;
		const auto &list = viter->second;
		size_t used = 0;
		auto &stream = s.output( );

		const std::string prefix = ( width != 0 ? name.append( width - name.length( ), ' ' ) : name ) + " : ";

		for ( auto iter = list.begin( ); iter != list.end( ); iter ++ )
		{
			auto word = *iter;

			if ( !s.is_word( word ) ) continue;

			if ( used != 0 && used + word.length( ) >= max_width )
			{
				stream << '\n';
				used = 0;
			}
			if ( used == 0 )
			{
				stream << prefix;
				used += prefix.length( );
			}

			stream << word << " ";
			used += word.length( ) + 1;
		}
		if ( used ) stream << '\n';
	}

	size_t width( ) const
	{
		size_t result = 0;
		for ( auto iter = vocabs.begin( ); iter != vocabs.end( ); iter ++ )
			result = iter->first.length( ) > result ? iter->first.length( ) : result;
		return result;
	}

	void see_vocab( const std::string &vocab ) const
	{
		see_vocab( vocabs.find( vocab ), width( ) );
	}

	void see_vocabs( ) const
	{
		size_t w = width( );
		for ( auto iter = vocabs.begin( ); iter != vocabs.end( ); iter ++ )
			see_vocab( iter, w );
	}

	const list_t &get_vocab( const std::string &vocab ) const
	{
		const auto &iter = vocabs.find( vocab );
		if ( iter == vocabs.end( ) ) RPANY_THROW( stack_exception, "Unknown vocab '" + vocab + "'" );
		return iter->second;
	}

	void list_vocab( const std::string &vocab ) const
	{
		const auto &iter = vocabs.find( vocab );
		if ( iter == vocabs.end( ) ) RPANY_THROW( stack_exception, "Unknown vocab '" + vocab + "'" );
		auto &list = iter->second;
		size_t count = 0;
		for ( auto word = list.begin( ); word != list.end( ); word ++ )
		{
			if ( !s.is_word( *word ) ) continue;
			s += *word;
			count ++;
		}
		s += count;
	}
};

inline void help_vocabs( stack &s )
{
	vocab_utils( s ).see_vocabs( );
}

inline void help_vocab( stack &s, const std::string &token )
{
	vocab_utils( s ).see_vocab( token );
}

inline word_list list_obtain( stack &s )
{
	size_t count;
	s >> count;

	word_list result( count );
	for ( size_t index = 0; index < count ; index ++ )
		result[ index ] = s.pick( static_cast< int >( count - index - 1 ) );
	s.pop( static_cast< int >( count ) );

	return result;
}

inline void see_vocab( stack &s, const std::string &token )
{
	s << "vocab-list" << token;
	auto list = list_obtain( s );
	for ( auto iter = list.begin( ); iter != list.end( ); iter ++ )
		if ( s.is_word( *iter ) )
			s << "see" << *iter;
}

inline void list_vocab( stack &s, const std::string &token )
{
	vocab_utils( s ).list_vocab( token );
}

inline void words( stack &s )
{
	const auto &lexicon = s.get_lexicon( );
	std::ostream &out = s.output( );
	for ( auto iter = lexicon.begin( ); iter != lexicon.end( ); iter ++ )
		out << ( *iter ).first << " ";
	out << std::endl;
}

RPANY_OPEN( words, stack &s )
	s.teach( "vocabs-help", help_vocabs );
	s.teach( "vocab-help", create_parse_token< help_vocab > );
	s.teach( "vocab-list", create_parse_token< list_vocab > );
	s.teach( "vocab-see", create_parse_token< see_vocab > );
	s.teach( "words", words );
RPANY_CLOSE( words );

RPANY_OPEN( help, stack &s )
	s.teach( "help", help_vocabs );
RPANY_CLOSE( help );

}
