#pragma once

namespace exceptions {

// Provides a means to handle exceptions in scripts - like:
//
// try <try-words> catch <catch-words> done
//
// If an exception is caught by the execution of <try-words>, the exception what
// return is pushed to the stack and <catch-words> are executed. In the catch, the
// word 'throw' can be used to rethrow the active exception and terminate.
//
// $ "file.mp4" try exec catch f$ "FAILED: %s" done
//
// Would leave either the result of executing "file.mp4" as a token on the stack
// or an error string.
//
// Experimental - currently, all exceptions are caught through a single catch.

class parser_try : public parser
{
	public:
		bool parse( stack &s, const rp::any &token )
		{
			// Convert token to a string
			const std::string word( if_string( token ) );

			// This parser will terminate when 'each' is obtained
			const bool done = word == "done";

			// We will move to the catch capture when the catch word is seen
			if ( in_try && word == "catch" )
			{
				in_try = false;
				return false;
			}

			// Add token to list unless done or inline
			if ( !done && !s.inline_parser( inline_list( ), token ) )
			{
				if ( in_try )
					try_.push_back( token );
				else
					catch_.push_back( token );
			}

			return done;
		}

		void execute( stack &s )
		{
			try
			{
				s.execute( try_ );
			}
			catch( const std::exception &e )
			{
				s += e.what( );
				s.execute( catch_ );
			}
		}

		void optimise( stack &s )
		{
			try_ = s.optimise( try_ );
			catch_ = s.optimise( catch_ );
		}

	private:
		bool in_try = true;
		word_list try_;
		word_list catch_;
};

RPANY_OPEN( exceptions, stack &s )
	s.teach( "try", [ ] ( stack &s, const std::string &token ) { s.set_parser( new parser_try ); } );
	s.teach( "throw", [] ( rpany::stack &s ) { if ( std::current_exception( ) != 0 ) throw; } );
RPANY_CLOSE( exceptions );

}
