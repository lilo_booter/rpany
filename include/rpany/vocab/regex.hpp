#pragma once

typedef std::shared_ptr< std::regex > regex_ptr;

inline rp::any regex_to_string( const rp::any &any )
{
	return std::string( "<regex>" );
}

inline rp::any string_to_regex( const rp::any &any )
{
	auto *str = rp::any_cast< std::string >( &any );
	return regex_ptr( new std::regex( *str ) );
}

void regex( rpany::stack &s )
{
	auto str = s.pull< std::string >( );
	s += regex_ptr( new std::regex( str ) );
}

void regex_match( rpany::stack &s )
{
	auto regex = s.pull< regex_ptr >( );
	auto str = s.cast< std::string >( );
	s += std::regex_match( str, *regex );
}

void regex_replace( rpany::stack &s )
{
	auto what = s.pull< std::string >( );
	auto with = s.pull< regex_ptr >( );
	auto str = s.pull< std::string >( );
	s += std::regex_replace( str, *with, what );
}

RPANY_OPEN( regex, rpany::stack &s )
	s.teach( "regex", regex );
	s.teach( "regex-match", regex_match );
	s.teach( "regex-replace", regex_replace );
	rpany::coercion( COERCE_KEY( regex_ptr, std::string ), regex_to_string );
	rpany::coercion( COERCE_KEY( std::string, regex_ptr ), string_to_regex );
RPANY_CLOSE( regex );

