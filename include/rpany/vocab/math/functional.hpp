#pragma once

// Plug gaps in functional c++ header file

template < typename T >
struct abs
{
	T operator( )( const T &a ) const { return a < 0 ? - a : a; }
};

template < typename T >
struct floor
{
	T operator( )( const T &a ) const { return std::floor( a ); }
};

template < typename T >
struct ceil
{
	T operator( )( const T &a ) const { return std::ceil( a ); }
};

template < typename T >
struct round
{
	T operator( )( const T &a ) const { return std::round( a ); }
};

#define RPANY_TYPE1( N, F ) \
	template < typename T > \
	struct N \
	{ \
		T operator( )( const T &a ) const \
		{ \
			return F( a ); \
		} \
	};

RPANY_TYPE1( log, std::log );
RPANY_TYPE1( exp, std::exp );

RPANY_TYPE1( cos, std::cos );
RPANY_TYPE1( sin, std::sin );
RPANY_TYPE1( tan, std::tan );
RPANY_TYPE1( acos, std::acos );
RPANY_TYPE1( asin, std::asin );
RPANY_TYPE1( atan, std::atan );
RPANY_TYPE1( cosh, std::cosh );
RPANY_TYPE1( sinh, std::sinh );
RPANY_TYPE1( tanh, std::tanh );
RPANY_TYPE1( acosh, std::acosh );
RPANY_TYPE1( asinh, std::asinh );
RPANY_TYPE1( atanh, std::atanh );

#define RPANY_TYPE2( N, F ) \
	template < typename T > \
	struct N \
	{ \
		T operator( )( const T &a, const T &b ) const \
		{ \
			return F( a, b ); \
		} \
	};

RPANY_TYPE2( pow, std::pow );
RPANY_TYPE2( fmod, std::fmod );

RPANY_TYPE2( atan2, std::atan2 );
