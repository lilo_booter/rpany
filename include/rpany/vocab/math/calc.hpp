#pragma once

// Template for single argument operators

template < template < typename T = double > class op, typename T = double >
inline void calc1( stack &s )
{
	op< T > f;
	auto &a = s.pick( 0 );
	if ( !is< T >( a ) ) ANY_EMPLACE( a, as< T >( coerce< T >( a ) ), T );
	T *av = rp::any_cast< T >( &a );
	*av = f( *av );
}

// Template for two argument operators

template < template < typename T = double > class op, typename T = double >
inline void calc2( stack &s )
{
	op< T > f;
	rp::any b = std::move( s.pull( ) );
	rp::any &a = s.pick( );
	if ( !is< T >( a ) ) ANY_EMPLACE( a, as< T >( coerce< T >( a ) ), T );
	if ( !is< T >( b ) ) ANY_EMPLACE( b, as< T >( coerce< T >( b ) ), T );
	T *av = rp::any_cast< T >( &a );
	const T *bv = rp::any_cast< T >( &b );
	*av = f( *av, *bv );
}
