#pragma once

namespace rstack {

typedef state_template< rp::any > state_type;
typedef std::shared_ptr< state_type > state_ptr;
typedef state_instance< rp::any > state_instance;

const std::string name( "_rstack_state" );

inline state_type &state( stack &s )
{
	rp::any any;
	if ( !s.is_local_var( name, any ) )
	{
		auto ptr = state_ptr( new state_type( ) );
		s.get_stack_frame( )[ name ] = ptr;
		return *ptr;
	}
	return *as< state_ptr >( any );
}

inline void pop( stack &s )
{
	state_type &r = state( s );
	s += r.pick( );
	r.pop( );
}

inline void push( stack &s )
{
	state( s ).push( s.pull( ) );
}

inline void peek( stack &s )
{
	s += state( s ).pick( );
}

inline void depth( stack &s )
{
	s += state( s ).size( );
}

inline void clear( stack &s )
{
	state( s ).clear( );
}

RPANY_OPEN( rstack, stack &s )
	s.teach( ">r", push );
	s.teach( "r>", pop );
	s.teach( "r@", peek );
	s.teach( "rdepth", depth );
	s.teach( "rclear", clear );
	s.teach( "r-list>", { "r>", "dup", 0, "do", "r>", "swap", "loop" } );
	s.teach( ">r-list", { "dup", 0, "do", "swap", ">r", "loop", ">r" } ) ;
RPANY_CLOSE( rstack );

}
