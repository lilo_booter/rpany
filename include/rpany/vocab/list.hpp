#pragma once

namespace list {

// parser_list specialisation

class parser_list : public parser
{
	public:
		parser_list( const std::string &start = "[[", const std::string &end = "]]" )
		: start_( start )
		, end_( end )
		{
		}

		bool parse( stack &s, const rp::any &token )
		{
			const std::string word = if_string( token );
			const bool done = word == end_ && nested_ == 0;
			if ( !done )
			{
				value_.push_back( convert< std::string >( token ) );
				if ( word == start_ ) nested_ ++;
				else if ( word == end_ ) nested_ --;
			}
			return done;
		}

		virtual void execute( stack &s )
		{
			for( auto iter = value_.begin( ); iter != value_.end( ); iter ++ )
				s += *iter;
			s += value_.size( );
		}

	protected:
		const std::string start_;
		const std::string end_;
		std::vector< std::string > value_;
		int nested_ = 0;
};

inline void insert_list_in_list( stack &s )
{
	const int offset = s.pull< int >( );
	const int count2 = s.pull< int >( );
	s.roll( count2 );
	const int count1 = s.pull< int >( );
	if ( offset < 0 || offset > count1 )
		RPANY_THROW( underflow_exception, "Invalid offset for list insertion" );
	const int total = count1 + count2;
	const int position = total - offset;
	for( int index = position; index > count2; index -- )
		s.roll( - position );
	s += total;
}

inline void insert_item_in_list( stack &s )
{
	s += 1;
	insert_list_in_list( s );
}

class parser_join : public parser_list
{
	public:
		parser_join( const std::string &separator = " " )
		: parser_list( "[", "]" )
		, separator_( separator )
		{ }

		void execute( stack &s )
		{
			std::stringstream stream;
			for( auto iter = value_.begin( ); iter != value_.end( ); iter ++ )
			{
				if ( iter != value_.begin( ) ) stream << separator_;
				const std::string item = as< std::string >( *iter );
				stream << quoted( item );
			}
			s += stream.str( );
		}

	private:
		const std::string separator_;
};

inline void join( stack &s )
{
	int count = s.pull< int >( );
	std::stringstream stream;
	for ( int index = - count; index < 0; index ++ )
	{
		if ( index != - count ) stream << " ";
		const std::string item = as< std::string >( s.pick( index ) );
		stream << quoted( item );
	}
	s.pop( count );
	s += stream.str( );
}

inline void split( stack &s )
{
	auto input = s.pull< std::string >( );
	std::string token;
	int count = 0;
	std::istringstream iss( input );
	while ( iss )
	{
		token = tokenise( iss );
		if ( !token.empty( ) )
		{
			s += token;
			count ++;
		}
	}
	s += count;
}

RPANY_OPEN( lists, stack &s )
	s.teach( "[", [ ] ( stack &s, const std::string &token ) { s.set_parser( new parser_join ); } );
	s.teach( RPANY_COMPILE_ONLY( stack &s, "]" ) );
	s.teach( "[[", [ ] ( stack &s, const std::string &token ) { s.set_parser( new parser_list ); } );
	s.teach( RPANY_COMPILE_ONLY( stack &s, "]]" ) );
	s.teach( "list-join", { "dup", 1, "+", "roll", "+" } );
	s.teach( "list-append", { "swap", 1, "+" } );
	s.teach( "list-in-list", insert_list_in_list );
	s.teach( "list-insert", { 1, "swap", insert_list_in_list } );
	s.teach( "list-at", { "over", "swap", "-", "pick" } );
	s.teach( "list-move", { "over", "swap", "-", "roll", "swap", 1, "-", "swap" } );
	s.teach( "join", join );
	s.teach( "split", split );
RPANY_CLOSE( lists );

}
