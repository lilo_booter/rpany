#pragma once

namespace io {

#if !( defined( _WIN32 ) || defined( _WIN32 ) )

static bool initialised = false;
static bool active = false;
static struct termios original;
static struct termios modified;

void endmode( )
{
	if ( active == true )
	{
		tcsetattr( 0, TCSADRAIN, &original );
		active = false;
	}
}

void start( stack &s )
{
	if ( initialised == false )
	{
		if ( &s.input( ) != &std::cin || !is_tty( ) )
			RPANY_THROW( stack_exception, "Attempted to use termios fails for non-tty stdin" );
		tcgetattr( 0, &original );
		modified = original;
		modified.c_lflag &= ~ICANON;
		modified.c_lflag &= ~ECHO;
		modified.c_cc[ VMIN ] = 0;
		modified.c_cc[ VTIME ] = 0;
		atexit( endmode );
		initialised = true;
	}

	if ( active == false )
	{
		tcsetattr( 0, TCSANOW, &modified );
		active = true;
	}
}

int readchar( int min )
{
	if ( modified.c_cc[ VMIN ] != min )
	{
		modified.c_cc[ VMIN ] = min;
		tcsetattr( 0, TCSANOW, &modified );
	}

	int buf = 0;
	size_t result = read( 0, &buf, 1 );
	if ( result > 1 ) RPANY_THROW( stack_exception, "kbhit failed" );
	return buf;
}

void snoop( stack &s )
{
	if ( active == false ) start( s );
	s += readchar( 0 );
}

void block( stack &s )
{
	if ( active == false ) start( s );
	s += readchar( 1 );
}

void end( stack &s )
{
	if ( active && &s.input( ) == &std::cin && is_tty( ) )
		endmode( );
}

#else

void snoop( stack &s )
{
	if ( &s.input( ) != &std::cin || !is_tty( ) )
		RPANY_THROW( stack_exception, "Attempted to use kbhit fails for non-tty stdin" );
	s += _kbhit( ) ? _getch( ) : 0;
}

void block( stack &s )
{
	if ( &s.input( ) != &std::cin || !is_tty( ) )
		RPANY_THROW( stack_exception, "Attempted to use kbhit fails for non-tty stdin" );
	s += _getch( );
}

void start( stack &s )
{
}

void end( stack &s )
{
}

#endif

RPANY_OPEN( dot, stack &s )
	s.teach( ".", [] ( stack &s ) { s.output( ) << s.pull< std::string >( ) << std::endl; } );
RPANY_CLOSE( dot );

RPANY_OPEN( io, stack &s )
	s.teach( ",", [] ( stack &s ) { s.output( ) << s.pull< std::string >( ); } );
	s.teach( "p.", { "inline", "," } );
	s.teach( "emit", [] ( stack &s ) { s.output( ) << static_cast< char >( s.pull< int >( ) ); } );
	s.teach( "read", [] ( stack &s ) { end( s ); std::string input; std::getline( s.input( ), input ); s += input; } );
	s.teach( "read-char", [] ( stack &s ) { s += int( s.input( ).get( ) ); } );
	s.teach( "eof", [] ( stack &s ) { s += s.closed( ); } );
	s.teach( "cr", { 10, "emit" } );
	s.teach( "space", { 32, "emit" } );
	s.teach( "flush", [] ( stack &s ) { s.output( ).flush( ); } );
	s.teach( "prompt", { ",", "read" } );
	s.teach( "log", [] ( stack &s ) { std::cerr << s.pull< std::string >( ) << std::endl; } );
	s.teach( "kbhit", snoop );
	s.teach( "kbwait", block );
	s.teach( "termio-start", start );
	s.teach( "termio-end", end );
RPANY_CLOSE( io );

}
