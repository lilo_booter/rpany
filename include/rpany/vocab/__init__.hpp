#pragma once

namespace rpany {
#include <rpany/vocab/word.hpp>
#include <rpany/vocab/stack.hpp>
#include <rpany/vocab/rstack.hpp>
#include <rpany/vocab/math.hpp>
#include <rpany/vocab/io.hpp>
#include <rpany/vocab/str.hpp>
#include <rpany/vocab/list.hpp>
#include <rpany/vocab/vocab.hpp>
#include <rpany/vocab/compare.hpp>
#include <rpany/vocab/if.hpp>
#include <rpany/vocab/do.hpp>
#include <rpany/vocab/eval.hpp>
#include <rpany/vocab/substack.hpp>
#include <rpany/vocab/vector.hpp>
#include <rpany/vocab/format.hpp>
#include <rpany/vocab/env.hpp>
#include <rpany/vocab/cases.hpp>
#include <rpany/vocab/vars.hpp>
#include <rpany/vocab/time.hpp>
#include <rpany/vocab/parse.hpp>
#include <rpany/vocab/tools.hpp>
#include <rpany/vocab/exception.hpp>
#include <rpany/vocab/regex.hpp>
}
