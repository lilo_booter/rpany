#pragma once

namespace string {

inline void parser_str( stack &s, const std::string &token )
{
	s += token;
}

RPANY_OPEN( string, stack &s )
	s.teach( "$", create_parse_token< parser_str > );
RPANY_CLOSE( string );

inline void strins( stack &s )
{
	auto what = s.pull< std::string >( );
	auto where = s.pull< size_t >( );
	auto in = s.pull< std::string >( );
	s += in.replace( where, 0, what );
}

inline void strrep( stack &s )
{
	auto what = s.pull< std::string >( );
	auto count = s.pull< size_t >( );
	auto where = s.pull< size_t >( );
	auto in = s.pull< std::string >( );
	s += in.replace( where, count, what );
}

size_t strifind( const std::string &haystack, const std::string &needle )
{
	auto it = std::search(
		haystack.begin( ), haystack.end( ),
		needle.begin( ), needle.end( ),
		[ ] ( char ch1, char ch2 ) { return std::toupper( ch1 ) == std::toupper( ch2 ); }
	);
	return it != haystack.end( ) ? it - haystack.begin( ) : std::string::npos;
}

size_t strifind_r( const std::string &haystack, const std::string &needle )
{
	auto it = std::search(
		haystack.rbegin( ), haystack.rend( ),
		needle.rbegin( ), needle.rend( ),
		[ ] ( char ch1, char ch2 ) { return std::toupper( ch1 ) == std::toupper( ch2 ); }
	);
	return it != haystack.rend( ) ? haystack.size( ) - ( it  - haystack.rbegin( ) + needle.size( ) ) : std::string::npos;
}

void strsplit( rpany::stack &s )
{
	auto token = s.pull< std::string >( );
	auto input = s.pull< std::string >( );
	auto pos = input.find( token );
	if ( pos != std::string::npos )
	{
		s += input.substr( 0, pos );
		s += input.substr( pos + token.size( ) );
		s += 2;
	}
	else
	{
		s += input;
		s += 1;
	}
}

RPANY_OPEN( strings, stack &s )
	s.teach( "str-len", [ ] ( stack &s ) { s += s.coerce< std::string >( ).size( ); } );
	s.teach( "str-sub", [ ] ( stack &s ) { auto end = s.pull< size_t >( ); auto start = s.pull< size_t >( ); s += s.pull< std::string >( ).substr( start, end ); } );
	s.teach( "str-cmp", [ ] ( stack &s ) { auto b = s.pull< std::string >( ); s += s.pull< std::string >( ).compare( b ); } );
	s.teach( "str-npos", [ ] ( stack &s ) { s += std::string::npos; } );
	s.teach( "str-find", [ ] ( stack &s ) { auto needle = s.pull< std::string >( ); s += s.cast< std::string >( ).find( needle ); } );
	s.teach( "str-find-r", [ ] ( stack &s ) { auto needle = s.pull< std::string >( ); s += s.cast< std::string >( ).rfind( needle ); } );
	s.teach( "str-ifind", [ ] ( stack &s ) { auto needle = s.pull< std::string >( ); s += strifind( s.cast< std::string >( ), needle ); } );
	s.teach( "str-ifind-r", [ ] ( stack &s ) { auto needle = s.pull< std::string >( ); s += strifind_r( s.cast< std::string >( ), needle ); } );
	s.teach( "str-at", [ ] ( stack &s ) { auto index = s.pull< size_t >( ); s += ( s.cast< std::string >( ) )[ index ]; } );
	s.teach( "str-ins", strins );
	s.teach( "str-rep", strrep );
	s.teach( "str-split", strsplit );
RPANY_CLOSE( strings );

}
