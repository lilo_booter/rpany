#pragma once

namespace tools {

inline void tools( stack &s )
{
	auto tools = enrolled( );
	for ( auto iter = tools.begin( ); iter != tools.end( ); iter ++ )
		s.output( ) << *iter << " ";
	s.output( ) << "user" << std::endl;
}

inline void tools_list( stack &s )
{
	auto tools = enrolled( );
	for ( auto iter = tools.begin( ); iter != tools.end( ); iter ++ )
		s += *iter;
	s += "user";
	s << tools.size( ) + 1;
}

inline void tool_help( stack &s, const std::string &token )
{
	if ( token != "user" )
	{
		auto vocabs = enrolled_vocab( token );
		std::sort( vocabs.begin( ), vocabs.end( ) );
		for ( auto iter = vocabs.begin( ); iter != vocabs.end( ); iter ++ )
			vocabs::vocab_utils( s ).see_vocab( *iter );
	}
	else
	{
		const auto &vocabs = s.vocabs( );
		for ( auto iter = vocabs.begin( ); iter != vocabs.end( ); iter ++ )
			if ( !vocab_enrolled( iter->first ) )
				vocabs::vocab_utils( s ).see_vocab( iter->first );
	}
}

RPANY_OPEN( tools, stack &s )
	s.teach( "tools", tools );
	s.teach( "tools-list", tools_list );
	s.teach( "tool-help", create_parse_token< tool_help > );
RPANY_CLOSE( tools );

}
