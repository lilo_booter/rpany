#pragma once

namespace time {

RPANY_OPEN( time, stack &s )
	using namespace std::chrono;
	s.teach( "sleep", [] ( stack &s ) { std::this_thread::sleep_for( microseconds( int( 1000000 * s.pull< double >( ) ) ) ); } );
	s.teach( "time", [] ( stack &s ) { s += system_clock::now( ); } );
	s.teach( "time-elapsed", [] ( stack &s ) { s += duration< double >( system_clock::now( ) - s.pull< system_clock::time_point >( ) ).count( ); } );
	s.teach( "time-diff", [] ( stack &s ) { auto a =s.pull< system_clock::time_point >( ); s += duration< double >( a - s.pull< system_clock::time_point >( ) ).count( ); } );
	coercion( COERCE_KEY( system_clock::time_point, std::string ), [] ( const rp::any &v ) {
		std::stringstream ss;
		std::time_t tt = std::chrono::system_clock::to_time_t( as< system_clock::time_point >( v ) );
		std::tm tm = *std::localtime( &tt );
		ss << std::put_time( &tm, "%Y-%m-%d %H:%M:%S" );
		return rp::any( ss.str( ) );
	} );
RPANY_CLOSE( time );

}
