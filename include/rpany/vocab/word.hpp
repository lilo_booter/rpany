#pragma once

namespace word {

struct word_utils
{
	typedef dictionary::dictionary_type::vocab_type vocab_t;
	typedef dictionary::dictionary_type::vocab_iterator vocab_iter_t;
	typedef dictionary::dictionary_type::list list_t;

	stack &s;
	std::ostream &stream;
	mutable bool lf = false;

	word_utils( stack &s_ )
	: s( s_ )
	, stream( s.output( ) )
	{ }

	void see( const std::string &word ) const
	{
		const auto iter = s.find_in_lexicon( word );
		if ( iter == s.end_of_lexicon( ) ) RPANY_THROW( undefined_exception, "Word '" + word + "' is not defined" );
		stream << ": " << ( word.find( "->" ) == std::string::npos ? word : word.substr( word.find( "->" ) + 2 ) ) << " ";
		lf = false;
		cite( iter->second.def );
		if ( lf ) stream << '\n';
		stream << ";" << std::endl;
	}

	void cite( const rp::any &any ) const
	{
		if ( is_numeric( any ) ) cite( convert< std::string >( any ) );
		else if ( is< std::string >( any ) ) cite( as< std::string >( any ) );
		else if ( is< word_list >( any ) ) cite( as< word_list >( any ) );
		else if ( is< parser_ptr >( any ) ) cite( as< parser_ptr >( any ) );
		else if ( is< word_function >( any ) ) cite( "<function>" );
		else if ( is< word_parser >( any ) ) cite( "<parser>" );
		else cite( std::string( any.type( ).name( ) ) );
	}

	void cite( const std::string &str ) const
	{
		stream << quoted( str ) << " ";
	}

	void cite( const char *str ) const
	{
		cite( std::string( str ) );
	}

	void cite( const word_list &word ) const
	{
		for ( auto iter = word.begin( ); iter != word.end( ); iter ++ )
		{
			const std::string &word = is< std::string >( *iter ) ? as< std::string >( *iter ) : "";
			if ( word == ":" ) { stream << std::endl << "    "; lf = true; }
			cite( *iter );
		}
	}

	void cite( const parser_ptr &parser ) const
	{
		cite( parser->tokens( ) );
	}
};

class parser_word : public parser
{
	public:
		bool parse( stack &s, const rp::any &token )
		{
			const std::string word( if_string( token ) );
			const bool done = word == ";";
			if ( !done )
			{
				if ( name_ == "" )
					name_ = word;
				else if ( list_.size( ) == 0 && word == "inline" )
					list_.push_back( token );
				else if ( is_inline( ) || !s.inline_parser( inline_list( ), token ) )
					list_.push_back( token );
			}
			return done;
		}

		void execute( stack &s )
		{
			if ( name_ == "" || name_ == ";" )
				RPANY_THROW( stack_exception, "No name specified in word definition" );
			s.teach( name_, list_ );
		}

		bool is_inline( )
		{
			return list_.size( ) > 0 && is< std::string >( list_[ 0 ] ) && as< std::string >( list_[ 0 ] ) == "inline";
		}

	private:
		std::string name_;
		word_list list_;
};

inline void set_word_parser( stack &s, const std::string &token )
{
	s.set_parser( new parser_word );
}

inline void parser_see( stack &s, const std::string &token )
{
	word_utils( s ).see( token );
}

inline void parser_forget( stack &s, const std::string &token )
{
	s.forget( token );
}

inline void find_name( stack &s )
{
	s += s.find_in_lexicon( s.pull< std::string >( ) ) != s.end_of_lexicon( );
}

inline void word_list( stack &s )
{
	size_t size = 0;
	for ( auto iter = s.start_of_lexicon( ); iter != s.end_of_lexicon( ); iter ++, size ++ )
		s += iter->first;
	s += size;
}

inline void word_see( stack &s )
{
	word_utils( s ).see( s.pull< std::string >( ) );
}

inline void word_forget( stack &s )
{
	s.forget( s.pull< std::string >( ) );
}

inline void see_word_list( stack &s )
{
	auto count = s.pull< int >( );
	for( auto index = count; -- index >= 0; )
		word_utils( s ).see( s.coerce< std::string >( index ) );
	s.pop( count );
}

class parser_comment : public parser
{
	public:
		bool parse( stack &s, const rp::any &token )
		{
			const std::string word = convert< std::string >( token );
			bool done = word == ")";
			if ( !done )
				value_.push_back( word );
			return done;
		}

		virtual void execute( stack &s )
		{
			value_.clear( );
		}

	private:
		std::vector< std::string > value_;
};

RPANY_OPEN( word, stack &s )
	s.teach( "see", create_parse_token< parser_see > );
	s.teach( ":", set_word_parser );
	s.teach( RPANY_COMPILE_ONLY( stack &s, ";" ) );
	s.teach( "forget", create_parse_token< parser_forget > );
	s.teach( "find-name", find_name );
	s.teach( "word-list", word_list );
	s.teach( "word-see", word_see );
	s.teach( "word-forget", word_forget );
	s.teach( "see-word-list", see_word_list );
	s.teach( "(", [ ] ( stack &s, const std::string &token ) { s.set_parser( new parser_comment ); } );
	s.teach( RPANY_COMPILE_ONLY( stack &s, ")" ) );
RPANY_CLOSE( word );

}
