#pragma once

namespace math {

#include "math/calc.hpp"
#include "math/functional.hpp"

RPANY_OPEN( arith, stack &s )
	using namespace math;
	s.teach( "+", calc2< std::plus > );
	s.teach( "-", calc2< std::minus > );
	s.teach( "*", calc2< std::multiplies > );
	s.teach( "/", calc2< std::divides > );
	s.teach( "%", calc2< fmod > );
RPANY_CLOSE( arith );

RPANY_OPEN( convert, stack &s )
	s.teach( "double", [] ( stack &s ) { auto &a = s.pick( 0 ); a = convert< double >( a ); } );
	s.teach( "int", [] ( stack &s ) { auto &a = s.pick( 0 ); a = convert< int >( a ); } );
	s.teach( "bool", [] ( stack &s ) { auto &a = s.pick( 0 ); a = convert< bool >( a ); } );
RPANY_CLOSE( convert );

RPANY_OPEN( rational, stack &s )
	using namespace math;
	s.teach( "rational", [] ( stack &s ) { auto a = s.pull( 0 ); s += convert< rational >( a ); } );
	s.teach( "r+", calc2< std::plus, rational > );
	s.teach( "r-", calc2< std::minus, rational > );
	s.teach( "r*", calc2< std::multiplies, rational > );
	s.teach( "r/", calc2< std::divides, rational > );
	s.teach( "r-num", [] ( stack &s ) { auto &a = s.pick( 0 ); s += ( int )( convert< rational >( a ) ).numerator( ); } );
	s.teach( "r-den", [] ( stack &s ) { auto &a = s.pick( 0 ); s += ( int )( convert< rational >( a ) ).denominator( ); } );
RPANY_CLOSE( rational );

RPANY_OPEN( math, stack &s )
	using namespace math;
	s.teach( "pi", { PI } );
	s.teach( "e", { E } );
	s.teach( "negate", calc1< std::negate > );
	s.teach( "abs", calc1< abs > );
	s.teach( "floor", calc1< floor > );
	s.teach( "round", calc1< round > );
	s.teach( "ceil", calc1< ceil > );
	s.teach( "sin", calc1< sin > );
	s.teach( "cos", calc1< cos > );
	s.teach( "tan", calc1< tan > );
	s.teach( "acos", calc1< acos > );
	s.teach( "asin", calc1< asin > );
	s.teach( "atan", calc1< atan > );
	s.teach( "cosh", calc1< cosh > );
	s.teach( "sinh", calc1< sinh > );
	s.teach( "tanh", calc1< tanh > );
	s.teach( "acosh", calc1< acosh > );
	s.teach( "asinh", calc1< asinh > );
	s.teach( "atanh", calc1< atanh > );
	s.teach( "atan2", calc2< atan2 > );
	s.teach( "ln", calc1< log > );
	s.teach( "exp", calc1< exp > );
	s.teach( "**", calc2< pow > );
	s.teach( "rand", [] ( stack &s ) { s += std::rand( ); } );
RPANY_CLOSE( math );

}
