# rpany/rpany.hpp

This document describes the internal organisation and functionality of the rpany
library.

## Preamble

The library is designed to be header only, hence to use it, you only need to
ensure your include path includes the rpany subdirectory.

By default, it builds with C++20 and has no external dependencies. If built with
older C++ versions, Boost equivalents are required for std::any and std::format.

## Top Level

The main header files which are directly accessible to an application are:

* `<rpany/rpany-host.hpp>` - includes everything
* `<rpany/rpany.hpp>` - includes the rpany namespace
* `<rpany/system.hpp>` - includes the system header dependencies

The remainder of the headers should not include anything external to the rpany
directory itself, and they do not need to specify a namespace.

## Internal Structure

The 'rpany::stack' object is the main interface to the rpany library. 

It maintains a stack of 'rp::any' obects - thus the stack can hold many types
of objects including (but not limited to) int, double, std::string, `size_t`, `int64_t`
bool etc.

It also holds a 'dictionary' and other state which is introduced by the words in
the dictionary.

A full description of all the subsystems involved is provided below.

## Including the Library

There is a necessity for some global state, and this is introduced by the
rpany-host.hpp header file, thus your main source files should have:

```
#include <rpany/rpany-host.hpp>
```

If other source files in the same project require use of the rpany::stack, it is 
sufficent for them to simply use:

```
#include <rpany/rpany.hpp>
```

## Subsystems

The library is divided into two subsystems:

* core
* vocab

The core provides the stack and associated objects, while vocab provides a
collection of vocabularies.

### core

#### `__init__.hpp`

Includes all core .hpp headers in the correct order.

#### `__host__.hpp`

Provides the global state for the host application.

Currently, this is just consists of declaring the variables to hold the vocab
registry and coercion rules.

If additional state is required, it should be introduced here.

#### `base.hpp`

This provides forward declarations to rpany types, common collection aliases and
general utility functionality.

* `bool is< T >( any )` - checks if any is of type T
* `T as< T >( any )` - obtains the value if it's of type T
* `T cast< T >( any )` - attempts to cast any to type T

Addiionally, a general purpose exception handling macro has been provided for
throwing exceptions of the form which take a string on the ctor argument.

* `RPANY_THROW_DEFAULT( exception )` - throws exception with ctor msg derived from exception
* `RPANY_THROW( exception, msg )` - throws exception with ctor msg specified

Additionally, defied the environment variable `RPANY_THROW_TRACE` cause the msg
to be decorated with the file and line number from where the exception was thrown.

#### `registry.hpp`

Provides the vocabulary registry and associated RPANY\_OPEN and RPANY\_CLOSE
macros.

* `RPANY_OPEN( name, arg )` - starts creation of vocab with name
* `RPANY_CLOSE( name )` - ends creation of vocab

A stack instance can be taught using the following functions:

* `void educate( stack )` - teaches the stack instance all vocabs
* `void educate( stack, vocab )` - teaches the stack a specific vocab string
* `void educate( stack, { vocab, ... } )` - teaches the stack a list of vocabs

#### `coerce.hpp`

Provides a general mechanism for rp::any type coercion - stack entries can be
introduced to the stack as std::string, int, double etc, and the coercion system 
will attempt to coerce them to another type on demand.

* `any coerce< T >( const any & )` - creates a new any of type T
* `T convert< T >( const any & )` - converts any to type T by way of coerce

#### `numeric.hpp`

General mechanism for identifying strings as a numeric type.

* `bool is_numeric( string )` - determines if string is numeric
* `bool is_numeric( any )` - determines if any is a numeric string

#### `parser.hpp`

Parsers allow complex word definitions - these allow words to accept tokens which
bypass the normal rules associated to parsing.

For example, a simple parser is associated to the $ word - this allows strings to be
introduced to the stack without them being parsed and executed:

```
$ "hello world" <- places "hello world" at the top of the stack
$ "1 2 3 * +"   <- places "1 2 3 * +" at the top of the stack
$ $             <- places "$" at the top of the stack
etc
```

Other uses include the : word for word creation, [[ or [ for multiple strings etc.

#### `dictionary.hpp`

The dictionary header provides the basic map which collects the defined words and their
associated definition.

* `void teach( word, definition )` - defines a new word with the specified definition

#### `stack.hpp`

Provides the stack class.

Interaction with the stack object is provided through a number of methods:

* `void push( any )` - the token is interpreted
* `void append( any )` - the token is pushed to the top of stack
* `any &pick( int i = 0 )` - returns the token at the specified stack position
* `T &cast< T >( int i = 0 )` - returns the token value if it's of type T
* `T coerce< T >( int i = 0 )` - attempts to coerce the requested stack value to type T
* `T pull< T >( )` - returns the position at the top of the stack as a value of type T
* `int depth( )` - return the number of items on the stack
* `void pop( int n = 1 )` - removes the top n items

Alternative uses:

* `s << any` - invokes s.push
* `s >> variable` - invokes s.pull and assigns to variable
* `s += any` - invokes s.append

#### `eval.hpp`

Provides helper functions for evalauting strings and arrays of char \*.

* `int eval( stack &, int, char ** )` - evaluate array of strings
* `int eval( stack &, string )` - evaluate string

### vocab

This directory contains vocabularies.

### `__init__.hpp`

Includes the vocab header files and registers the discovered vocabularies.

All core vocabs are included here - all new vocab headers must be registered
here.

More details on the existing vocabs are provided in the README.md in the root
of the rpany repo.
