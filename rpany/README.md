# rpany

This is the main exectuable for accessing the core vocabulary and loading 
scripts.

## Usage

```
rpany [ script-file | -- ] [ args ]*
```

By default, all args are treated as tokens unless a filename is specified
as the first argument.

Hence, you can use the default vocabs directly from the command line:

```
$ rpany 1 2 3 \* + .
7
$ rpany $ hello $ world $ "%s %s!" fprint .
hello world!
```

If there is a possibility that the first argument may pick up a file name
incorrectly, you can specify the first argument as a --:

```
$ touch words
$ rpany -- words
<word list>
```

If a file is specified it is parsed and evaluated immediately.

If the file provides a `__main__` word, this is invoked after all the
remaining arguments are handled.

By default, the remaining arguments are passed directly to the stack. However,
this can be overridden if the script provides a `__switches__` word. This word
locks down the argument handling to the passing of args beginning with - or --
and the script can provide words with these prefixes.
