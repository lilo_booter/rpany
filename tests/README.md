# rpany tests

The purpose of the tests is to provide a suite of unit tests for the core rpany
stack and default dictionaries.

## Usage

The executable runs through a series of registered using the command:

```
rpany-test
```

Each line of output represents a specific test of the functionality and is
prefixed with either PASS or FAIL. A final line is output with the same
choice of prefix and an indication of the number of failing tests if any
failures are detected.

## Maintaining

Please ensure all tests pass before committing a modification and add to the
tests as new features are added or edge cases are detected.
