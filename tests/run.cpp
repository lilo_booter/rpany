#include <rpany/rpany.hpp>
#include <rpany/tests.hpp>

using namespace rpany;

void run_tests( suite &t )
{
	stack &s = t.s;

	// Test empty stack first
	run_test( t, "start up", {
		RUN( "is empty on startup", { }, { } ),
		RUN( "data stack is empty on startup", { s.depth( ) }, { 0 } ),
		RUN( "stack is not in parsing state on startup", { s.is_parsing( ) }, { 0 } ),
		RUN( "dictionary is empty", { s.get_lexicon( ).begin( ) == s.get_lexicon( ).end( ) }, { true } ),
		RUN( "variables are empty", { s.vbegin( ) == s.vend( ) }, { true } ),
	} );

	// Add the default dictionaries
	educate( t.s );

	// Tests for default dictionaries
	run_test( t, "education", {
		RUN( "data stack is empty after dictionaries added", { }, { } ),
		RUN( "stack is not in parsing state", { s.is_parsing( ) }, { 0 } ),
		RUN( "dictionary is not empty", { s.get_lexicon( ).begin( ) != s.get_lexicon( ).end( ) }, { true } ),
	} );

	run_test( t, "push", {
		RUN( "zero", { 0 }, { 0 } ),
		RUN( "one", { 1 }, { 1 } ),
		RUN( "zero and one", { 0, 1 }, { 0, 1 } ),
		RUN( "negative 1", { -1 }, { -1 } ),
		RUN( "0.1", { 0.1 }, { .1 } ),
		RUN( ".1", { .1 }, { .1 } ),
		RUN( "-0.1", { -0.1 }, { -.1 } ),
		RUN( "-.1", { -.1 }, { -.1 } ),
	} );

	run_test( t, "compare", {
		RUN( "equality of 0", { 0, 0, "==" }, { 1 } ),
		RUN( "equality of 1", { 1, 1, "==" }, { 1 } ),
		RUN( "inequality of 0", { 0, 0, "!=" }, { 0 } ),
		RUN( "inequality of 0 and 1", { 0, 1, "!=" }, { 1 } ),
		RUN( "inequality of 1", { 1, 1, "!=" }, { 0 } ),
		RUN( "inequality of 1 and -1", { 1, -1, "!=" }, { 1 } ),
		RUN( "0 > 0", { 0, 0, ">" }, { 0 } ),
		RUN( "0 > -1", { 0, -1, ">" }, { 1 } ),
		RUN( "0 < 0", { 0, 0, "<" }, { 0 } ),
		RUN( "0 < -1", { 0, -1, "<" }, { 0 } ),
		RUN( "0 >= 0", { 0, 0, ">=" }, { 1 } ),
		RUN( "0 <= 0", { 0, 0, "<=" }, { 1 } ),
		RUN( "0 >= 1", { 0, 1, ">=" }, { 0 } ),
		RUN( "0 <= 1", { 0, 1, "<=" }, { 1 } ),
	} );
	run_test( t, "arith", {
		RUN( "add zero and one", { 0, 1, "+" }, { 1 } ),
		RUN( "add minus one and one", { -1, 1, "+" }, { 0 } ),
		RUN( "add one, one and one", { 1, 1, 1, "+", "+" }, { 3 } ),
		RUN( "add one and one, then one", { 1, 1, "+", 1, "+" }, { 3 } ),
		RUN( "subtract zero and one", { 0, 1, "-" }, { -1 } ),
		RUN( "subtract minus one and one", { -1, 1, "-" }, { -2 } ),
		RUN( "subtract one, one and one", { 1, 1, 1, "-", "-" }, { 1 } ),
		RUN( "subtract one and one, then one", { 1, 1, "-", 1, "-" }, { -1 } ),
		RUN( "divide 1 by 10", { 1, 10, "/" }, { .1 } ),
		RUN( "divide 10 by 1", { 10, 1, "/" }, { 10 } ),
		RUN( "divide 1 by 3", { 1, 3, "/" }, { 1.0 / 3.0 } ),
		RUN( "multiply 2 by 2", { 2, 2, "*" }, { 4 } ),
		RUN( "multiply 10 by .1", { 10, .1, "*" }, { 1 } ),
		RUN( "combine multiply and add", { 1, 2, 3, "*", "+" }, { 7 } ),
		RUN( "combine multiply and divide", { 1, 10, "/", 5, "*" }, { 0.5 } ),
	} );

	run_test( t, "stack", {
		RUN( "drop tos", { 1, "drop" }, { } ),
		RUN( "drop tos with more than one item", { 1, 2, "drop" }, { 1 } ),
		RUN( "clear stack", { 1, 2, 3, "clear" }, { } ),
		RUN( "pick tos", { 1, 0, "pick" }, { 1, 1 } ),
		RUN( "pick tos and use", { 2, 0, "pick", "*" }, { 4 } ),
		RUN( "pick one below tos", { 1, 2, 1, "pick" }, { 1, 2, 1 } ),
		RUN( "roll of 0 is a no op", { 1, 2, 0, "roll" }, { 1, 2 } ),
		RUN( "roll of 1 is a swap", { 1, 2, 1, "roll" }, { 2, 1 } ),
		RUN( "roll of 1 doesn't touch rest", { 0, 1, 2, 1, "roll" }, { 0, 2, 1 } ),
	} );

	run_test( t, "string", {
		RUN( "accepts empty string", { "$", "" }, { "" } ),
		RUN( "accepts string", { "$", "hello" }, { "hello" } ),
		RUN( "accepts $ as a string", { "$", "$" }, { "$" } ),
		RUN( "accepts string with spaces", { "$", "hello world" }, { "hello world" } ),
		RUN( "accepts string with quotes", { "$", "hello \"world\"" }, { "hello \"world\"" } ),
	} );

	run_test( t, "math", {
		RUN( "accepts pi", { "pi" }, { math::PI } ),
		RUN( "accepts e", { "e" }, { math::E } ),
		RUN( "raise 0 ** 0", { 0, 0, "**" }, { 1 } ),
		RUN( "raise 1 ** 0", { 1, 0, "**" }, { 1 } ),
		RUN( "raise 2 ** 2", { 2, 2, "**" }, { 4 } ),
		RUN( "raise 4 ** 0.5", { 4, 0.5, "**" }, { 2 } ),
		RUN( "ln e is 1", { "e", "ln" }, { 1 } ),
		RUN( "ln e ** 2 is 2", { "e", 2, "**", "ln" }, { 2 } ),
	} );

	run_test( t, "programmable math", {
		RUN( "teach log10", { ":", "log10", "ln", 10, "ln", "/", ";" }, { } ),
		RUN( "confirm log is known", { "$", "log10", "find-name" }, { 1 } ),
		RUN( "log10 10 is 1", { 10, "log10" }, { 1 } ),
		RUN( "log10 100 is 2", { 100, "log10" }, { 2 } ),
		RUN( "forget log10", { "forget", "log10" }, { } ),
		RUN( "confirm log10 is forgotten", { "$", "log10", "find-name" }, { 0 } ),
	} );

	eval_test( t, "additional math", {
		EVAL( "floor", "1.0 floor", { 1.0 } ),
		EVAL( "floor", "1.1 floor", { 1.0 } ),
		EVAL( "floor", "1.5 floor", { 1.0 } ),
		EVAL( "floor", "1.99 floor", { 1.0 } ),
		EVAL( "floor", "-1.0 floor", { -1.0 } ),
		EVAL( "floor", "-1.1 floor", { -2.0 } ),
		EVAL( "floor", "-1.5 floor", { -2.0 } ),
		EVAL( "floor", "-1.99 floor", { -2.0 } ),
		EVAL( "ceil", "1.0 ceil", { 1.0 } ),
		EVAL( "ceil", "1.1 ceil", { 2.0 } ),
		EVAL( "ceil", "1.5 ceil", { 2.0 } ),
		EVAL( "ceil", "1.99 ceil", { 2.0 } ),
		EVAL( "ceil", "-1.0 ceil", { -1.0 } ),
		EVAL( "ceil", "-1.1 ceil", { -1.0 } ),
		EVAL( "ceil", "-1.5 ceil", { -1.0 } ),
		EVAL( "ceil", "-1.99 ceil", { -1.0 } ),
		EVAL( "round", "1.0 round", { 1.0 } ),
		EVAL( "round", "1.1 round", { 1.0 } ),
		EVAL( "round", "1.5 round", { 2.0 } ),
		EVAL( "round", "1.99 round", { 2.0 } ),
		EVAL( "round", "-1.0 round", { -1.0 } ),
		EVAL( "round", "-1.1 round", { -1.0 } ),
		EVAL( "round", "-1.5 round", { -2.0 } ),
		EVAL( "round", "-1.99 round", { -2.0 } ),
	} );
}
