#include <rpany/rpany.hpp>
#include <rpany/tests.hpp>

using namespace rpany;

#define EVAL et

void eval_tests1( suite &t )
{
	eval_test( t, "eval", {
		EVAL( "empty string is a no-op", "", { } ),
		EVAL( "whitespace string is a no-op", "	 ", { } ),
		EVAL( "passes 0", "0", { 0 } ),
		EVAL( "passes 1", "1", { 1 } ),
		EVAL( "passes 1 2 3", "   1 2 3	", { 1, 2, 3 } ),
		EVAL( "passes 1 2 3 * +", "1 2 3 * +", { 7 } ),
		EVAL( "passes string", "$ hello", { "hello" } ),
		EVAL( "passes string with space", "$ \"hello world\"", { "hello world" } ),
	} );

	eval_test( t, "eval stack ops", {
		EVAL( "dup word", "10 dup", { 10, 10 } ),
		EVAL( "?dup word with non-zero tos", "10 dup", { 10, 10 } ),
		EVAL( "?dup word with zero tos", "0 ?dup", { 0 } ),
		EVAL( "swap switches the top two items", "1 2 3 swap", { 1, 3, 2 } ),
		EVAL( "over copies the one below tos to tos", "1 2 over", { 1, 2, 1 } ),
		EVAL( "nip removes the item below tos", "1 2 nip", { 2 } ),
		EVAL( "rot moves 2nd below tos to tos", "1 2 3 rot", { 2, 3, 1 } ),
		EVAL( "-rot moves tos to 2nd below tos", "1 2 3 -rot", { 3, 1, 2 } ),
		EVAL( "tuck is the same as swap over", "1 2 tuck", { 2, 1, 2 } ),
		EVAL( "2dup is the same as over over", "1 2 2dup", { 1, 2, 1, 2 } ),
		EVAL( "2drop is the same as drop drop", "1 2 2drop", { } ),
		EVAL( "2swap swap 2nd with 3rd, 1st with tos", "1 2 3 4 2swap", { 2, 1, 4, 3 } ),
		EVAL( "2over copies 2nd with 3rd to tos", "1 2 3 4 2over", { 1, 2, 3, 4, 1, 2 } ),
		EVAL( "2nip removes 2nd with 3rd", "1 2 3 4 2nip", { 3, 4 } ),
		EVAL( "2tuck moves 2 items at tos 2 down and copies them to tos", "1 2 3 4 2tuck", { 3, 4, 1, 2, 3, 4 } ),
		EVAL( "2rot is the same as 5 roll 5 roll", "1 2 3 4 5 6 2rot", { 3, 4, 5, 6, 1, 2 } ),
	} );

	eval_test( t, "word definition", {
		EVAL( "eval find-name of non-existent word", "$ mytest find-name", { false } ),
		EVAL( "eval-word? of non-existent word", "$ mytest eval-word?", { } ),
		EVAL( "eval word creation", ": mytest 1 ;", { } ),
		EVAL( "eval existence of word", "$ mytest find-name", { true } ),
		EVAL( "eval execution of word", "mytest", { true } ),
		EVAL( "eval-word? existent word", "$ mytest eval-word?", { true } ),
		EVAL( "eval word destruction", "forget mytest", { } ),
		EVAL( "eval find-name of destroyed word", "$ mytest find-name", { false } ),
		EVAL( "eval-word? of destroyed word", "$ mytest eval-word?", { } ),
		EVAL( "define false", ": false 0 ;", { } ),
		EVAL( "define true", ": true 1 ;", { } ),
	} );

	eval_test( t, "conditional if", {
		EVAL( "if true then", "true if 100 then", { 100 } ),
		EVAL( "if false then", "false if 100 then", { } ),
		EVAL( "if true else then", "true if 100 else 200 then", { 100 } ),
		EVAL( "if else false then", "false if 100 else 200 then", { 200 } ),
		EVAL( "if nested parser true then", "true if $ hello else $ goodbye then", { "hello" } ),
		EVAL( "if nested parser false then", "false if $ hello else $ goodbye then", { "goodbye" } ),
		EVAL( "if nested parser with conflicting tokens else then", "true if $ else else $ then then", { "else" } ),
		EVAL( "if nested parser with conflicting tokens else false then", "false if $ else else $ then then", { "then" } ),
		EVAL( "if nested true/true then", "true true if if $ hello then then", { "hello" } ),
		EVAL( "if nested true/false then", "true false if if $ hello then then", { true } ),
		EVAL( "if nested false/true then", "false true if if $ hello then then", { } ),
		EVAL( "if nested false/false then", "false false if if $ hello then then", { false } ),
	} );

	eval_test( t, "conditional ?{", {
		EVAL( "?{ true }", "true ?{ 100 }", { 100 } ),
		EVAL( "?{ false }", "false ?{ 100 }", { } ),
		EVAL( "?{ true }:{ }", "true ?{ 100 }:{ 200 }", { 100 } ),
		EVAL( "?{ }:{ false }", "false ?{ 100 }:{ 200 }", { 200 } ),
		EVAL( "?{ nested parser true }", "true ?{ $ hello }:{ $ goodbye }", { "hello" } ),
		EVAL( "?{ nested parser false }", "false ?{ $ hello }:{ $ goodbye }", { "goodbye" } ),
		EVAL( "?{ nested parser with conflicting tokens }:{ }", "true ?{ $ }:{ }:{ $ } }", { "}:{" } ),
		EVAL( "?{ nested parser with conflicting tokens }:{ false }", "false ?{ $ }:{ }:{ $ } }", { "}" } ),
		EVAL( "?{ nested true/true }", "true true ?{ ?{ $ hello } }", { "hello" } ),
		EVAL( "?{ nested true/false }", "true false ?{ ?{ $ hello } }", { true } ),
		EVAL( "?{ nested false/true }", "false true ?{ ?{ $ hello } }", { } ),
		EVAL( "?{ nested false/false }", "false false ?{ ?{ $ hello } }", { false } ),
	} );

	eval_test( t, "do loop", {
		EVAL( "count of 0", "loops?", { 0 } ),
		EVAL( "no op loop with start == end", "0 0 do 1 loop", { } ),
		EVAL( "no op loop with start < end", "1 0 do 1 loop", { 1 } ),
		EVAL( "no op loop with start > end", "0 1 do 1 loop", { 1 } ),
		EVAL( "count still 0", "loops?", { 0 } ),
		EVAL( "0 to 10", "10 0 do 1 loop", { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 } ),
		EVAL( "0 to 10 using i", "10 0 do i loop", { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 } ),
		EVAL( "10 to 0 using i", "0 10 do i loop", { 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 } ),
		EVAL( "ensures loops? is 1 with 1 loop", "1 0 do loops? loop", { 1 } ),
		EVAL( "ensures loops? is 2 with 2 loops", "1 0 do loops? 1 0 do loops? loop loop", { 1, 2 } ),
	} );

	eval_test( t, "format fprint", {
		EVAL( "empty string gives empty string", "$ \"\" fprint", { "" } ),
		EVAL( "string with no refs is itself", "$ \"1 2 3\" fprint", { "1 2 3" } ),
		EVAL( "string with %s ref is tos", "$ hello $ %s fprint", { "hello" } ),
		EVAL( "string with %s ref includes tos", "$ hello $ +%s+ fprint", { "+hello+" } ),
		EVAL( "string with multiple %s ref includes multiple", "$ untouched $ hello $ world $ ~%s+%s! fprint", { "untouched", "~hello+world!" } ),
	} );

	eval_test( t, "format fpeek", {
		EVAL( "empty string gives empty string", "$ \"\" fpeek", { "" } ),
		EVAL( "string with no refs is itself", "$ \"1 2 3\" fpeek", { "1 2 3" } ),
		EVAL( "string with %s ref is tos", "$ hello $ %s fpeek", { "hello", "hello" } ),
		EVAL( "string with %s ref includes tos", "$ hello $ +%s+ fpeek", { "hello", "+hello+" } ),
		EVAL( "string with multiple %s ref includes multiple", "$ untouched $ hello $ world $ ~%s+%s! fpeek", { "untouched", "hello", "world", "~hello+world!" } ),
	} );
}

void eval_tests2( suite &t )
{
	eval_test( t, "lists", {
		EVAL( "[[ ]] gives 0", "[[ ]]", { 0 } ),
		EVAL( "[[ 0 ]] gives items + count of 1", "[[ 0 ]]", { 0, 1 } ),
		EVAL( "[[ 0 1 2 ]] gives items + count of 3", "[[ 0 1 2 ]]", { 0, 1, 2, 3 } ),
		EVAL( "[[ a b 2 ]] gives items + count of 3", "[[ a b 2 ]]", { "a", "b", 2, 3 } ),
		EVAL( "[[ ]] retains spaces in strings", "[[		  \"hello world \"	   ]]", { "hello world ", 1 } ),
		EVAL( "[[ ]] can be embedded", "[[ this [[ is a ]] test ]]", { "this", "[[", "is", "a", "]]", "test", 6 } ),
		EVAL( "[[ ]] can be embedded and joined", "[[ this [[ is a ]] test ]] join", { "this [[ is a ]] test" } ),
		EVAL( "[[ ]] [[ ]] list-join = 0", "[[ ]] [[ ]] list-join", { 0 } ),
		EVAL( "[[ a b ]] [[ c ]] list-join = a b c", "[[ a b ]] [[ c ]] list-join", { "a", "b", "c", 3 } ),
		EVAL( "[[ a b ]] c list-append = a b c", "[[ a b ]] $ c list-append", { "a", "b", "c", 3 } ),

		EVAL( "[[ ]] [[ ]] 0 list-in-list = 0", "[[ ]] [[ ]] 0 list-in-list", { 0 } ),
		EVAL( "[[ a b c ]] [[ d e f ]] 0 list-in-list = d e f a b c", "[[ a b c ]] [[ d e f ]] 0 list-in-list", { "d", "e", "f", "a", "b", "c", 6 } ),
		EVAL( "[[ a b c ]] [[ d e f ]] 1 list-in-list = a d e f b c", "[[ a b c ]] [[ d e f ]] 1 list-in-list", { "a", "d", "e", "f", "b", "c", 6 } ),
		EVAL( "[[ a b c ]] [[ d e f ]] 2 list-in-list = a b d e f c", "[[ a b c ]] [[ d e f ]] 2 list-in-list", { "a", "b", "d", "e", "f", "c", 6 } ),
		EVAL( "[[ a b c ]] [[ d e f ]] 3 list-in-list = a b c d e f", "[[ a b c ]] [[ d e f ]] 3 list-in-list", { "a", "b", "c", "d", "e", "f", 6 } ),
		EVAL( "[[ a b c ]] [[ d e f ]] 4 list-in-list = error", "[[ a b c ]] [[ d e f ]] 4 list-in-list", { "a", "b", "c", "d", "e", "f" }, e_underflow ),
		EVAL( "[[ a b c ]] [[ d e f ]] -1 list-in-list = error", "[[ a b c ]] [[ d e f ]] -1 list-in-list", { "a", "b", "c", "d", "e", "f" }, e_underflow ),

		EVAL( "[[ a b c ]] 0 list-at == a",  "[[ a b c ]] 0 list-at", { "a", "b", "c", 3, "a" } ),
		EVAL( "[[ a b c ]] 0 list-at == a",  "[[ a b c ]] 1 list-at", { "a", "b", "c", 3, "b" } ),
		EVAL( "[[ a b c ]] 0 list-at == a",  "[[ a b c ]] 2 list-at", { "a", "b", "c", 3, "c" } ),

		EVAL( "[[ a b c ]] 0 list-at == a",  "[[ a b c ]] 0 list-move", { "b", "c", 2, "a" } ),
		EVAL( "[[ a b c ]] 0 list-at == a",  "[[ a b c ]] 1 list-move", { "a", "c", 2, "b" } ),
		EVAL( "[[ a b c ]] 0 list-at == a",  "[[ a b c ]] 2 list-move", { "a", "b", 2, "c" } ),
	} );

	eval_test( t, "cases", {
		EVAL( "&{ &} consumes tos, but is otherwise a no op", "true &{ &}", { } ),
		EVAL( "&{ *> {{ }} &} default case receives tos", "true &{ *> {{ }} &}", { true } ),
		EVAL( "&{ *> {{ }} &} default case receives tos regardless of value", "false &{ *> {{ }} &}", { false } ),
		EVAL( "true &{ +> {{ $ yes }} *> {{ drop $ no }} &} gives yes", "true &{ +> {{ $ yes }} *> {{ $ no }} &}", { "yes" } ),
		EVAL( "false &{ +> {{ $ yes }} *> {{ drop $ no }} &} gives no", "false &{ +> {{ $ yes }} *> {{ drop $ no }} &}", { "no" } ),
		EVAL( "1 dup &{ +> 0 > {{ negate }} *> {{ drop abs }} &} gives -1", "1 dup &{ +> 0 > {{ negate }} *> {{ drop abs }} &}", { -1 } ),
		EVAL( "0 dup &{ +> 0 > {{ negate }} *> {{ drop abs }} &} gives 0", "0 dup &{ +> 0 > {{ negate }} *> {{ drop abs }} &}", { 0 } ),
		EVAL( "-1 dup &{ +> 0 > {{ negate }} *> {{ drop abs }} &} gives 1", "-1 dup &{ +> 0 > {{ negate }} *> {{ drop abs }} &}", { 1 } ),
	} );

	eval_test( t, "exceptions", {
		EVAL( "+ causes underflow with empty stack", "+", { }, e_underflow ),
		EVAL( "+ causes underflow with 1 item on stack", "1 +", { 1 }, e_underflow ),
		EVAL( "+ causes a coercion error if it doesn't find two numerics", "$ a 1 +", { "a" }, e_coercion ),
		EVAL( "+ causes a coercion error if it doesn't find two numerics", "1 $ a +", { 1 }, e_coercion ),
		EVAL( "leave causes leave exception", "leave", { }, e_leave ),
		EVAL( "leave causes leave exception and doesn't evaluate remainder", { "leave 1 2 3" }, { }, e_leave ),
		EVAL( "leave causes leave exception and doesn't affect before", { "0 1 leave 2 3" }, { 0, 1 }, e_leave ),
		EVAL( "fprint throws underflow", "fprint", { }, e_underflow ),
		EVAL( "fprint throws underflow and removes specifier", "$ %s fprint", { }, e_underflow ),
		EVAL( "begin leave again just leaves loop", "begin leave 1 again", { } ),
	} );

	eval_test( t, "substacks", {
		EVAL( "ss-new", "ss-new", { "<ss>" } ),
		EVAL( "ss-new accepts evaluation", "ss-new ss-ready", { "<ss>", true } ),
		EVAL( "ss-new evaluate numeric", "ss-new 0 ss-eval", { "<ss>" } ),
		EVAL( "ss-new evaluate numeric and returns", "ss-new 0 ss-eval ss-pull", { "<ss>", 0 } ),
		EVAL( "ss-new evaluate numerics", "ss-new $ \"0 1 2\" ss-eval", { "<ss>" } ),
		EVAL( "ss-new evaluate numerics and returns tos", "ss-new $ \"0 1 2\" ss-eval ss-pull", { "<ss>", 2 } ),
		EVAL( "ss-new execute list", "ss-new [[ 0 1 2 ]] ss-exec-list", { "<ss>" } ),
		EVAL( "ss-new execute list and returns tos", "ss-new [[ 0 1 2 ]] ss-exec-list ss-pull", { "<ss>", 2 } ),
		EVAL( "ss-new knows nothing", "ss-new [[ word-list ]] ss-exec-list", { "<ss>" }, e_undefined ),
		EVAL( "ss-new can be taught", "ss-new [[ word ]] ss-educate-list [[ word-list ]] ss-exec-list ss-pull 0 !=", { "<ss>", 1 } ),
		EVAL( "ss-self-educated knows what i know", "ss-new ss-self-educated nip [[ word-list join ]] ss-exec-list ss-pull word-list join ==", { "<ss>", 1 } ),
		EVAL( "ss-same created", ": ss-same ss-new ss-self-educated nip ;", { } ),
		EVAL( "words$ created", ": words$ word-list join ;", { } ),
		EVAL( "ss-same knows what i know", "ss-same [[ words$ ]] ss-exec-list ss-pull words$ ==", { "<ss>", 1 } ),
		EVAL( "ss-same has its own data", "ss-same [[ 0 1 2 ]] ss-exec-list", { "<ss>" } ),
		EVAL( "ss-same retains its own data", "ss-same [[ [[ 0 1 2 ]] join ]] ss-exec-list ss-pull", { "<ss>", "0 1 2" } ),
		EVAL( "ss-same holds its own parsing state", "ss-same [[ : new-word ]] ss-exec-list ss-ready", { "<ss>", 0 } ),
		EVAL( "ss-same honours parsing state", "ss-same [[ : new-word ]] ss-exec-list ss-ready swap [[ 1 ; ]] ss-exec-list ss-ready", { 0, "<ss>", 1 } ),
		EVAL( "ss-clone", "ss-clone", { "<ss>" } ),
		EVAL( "ss-clone dictionary is isolated", "ss-clone [[ : this-is-a-new-word ; ]] ss-exec-list $ this-is-a-new-word find-name", { "<ss>", 0 } ),
		EVAL( "ss-clone data is isolated", "ss-clone [[ 10 20 30 ]] ss-exec-list depth", { "<ss>", 1 } ),
	} );

	eval_test( t, "variable", {
		EVAL( "doesn't exist", "testvar", { }, e_undefined ),
		EVAL( "creation", "variable testvar", { } ),
		EVAL( "persists", "testvar", { "<var-testvar>" } ),
		EVAL( "assignment", "10 testvar !", { } ),
		EVAL( "deref", "testvar @", { 10 } ),
		EVAL( "reassignment", "$ value testvar !", { } ),
		EVAL( "deref", "testvar @", { "value" } ),
		EVAL( "destroy", "forget testvar", { } ),
		EVAL( "gone", "testvar", { }, e_undefined ),
	} );

	eval_test( t, "inline", {
		EVAL( "sanity check - fi doesn't exist", "fi", { }, e_undefined ),
		EVAL( "define fi", ": fi inline then ;", { } ),
		EVAL( "confirm fi is a valid replacement for then", "1 if $ hello fi", { "hello" } ),
		EVAL( "confirm fi is a valid replacement for then", "0 if $ hello fi", { } ),
		EVAL( "confirm fi can be used in a word", ": inline-test if $ hello else $ bye fi ;", { } ),
		EVAL( "confirm fi can be used in a word", "1 inline-test", { "hello" } ),
		EVAL( "confirm fi can be used in a word", "0 inline-test", { "bye" } ),
		EVAL( "sanity check - not-if doesn't exist", "not-if", { }, e_undefined ),
		EVAL( "confirm parsers can be inlined", ": not-if inline 0 == if ;", { } ),
		EVAL( "confirm fi can be used in a word", ": inline-test not-if $ hello else $ bye fi ;", { } ),
		EVAL( "confirm not-if can be used in a word", "1 inline-test", { "bye" } ),
		EVAL( "confirm not-if can be used in a word", "0 inline-test", { "hello" } ),
	} );
}

void eval_tests3( suite &t )
{
	eval_test( t, "str-cmp", {
		EVAL( "sanity check - underflow", "str-cmp", { }, e_underflow ),
		EVAL( "sanity check - underflow", "$ a str-cmp", { }, e_underflow ),
		EVAL( "equal", "$ \"hello world\" $ \"hello world\" str-cmp", { 0 } ),
		EVAL( "less than", "$ a $ b str-cmp", { -1 } ),
		EVAL( "less than", "$ a $ aa str-cmp", { -1 } ),
		EVAL( "greater than", "$ b $ a str-cmp", { 1 } ),
		EVAL( "greater than", "$ bb $ b str-cmp", { 1 } ),
		EVAL( "equal", "$ a $ a str-cmp", { 0 } ),
	} );

	eval_test( t, "str-find", {
		EVAL( "sanity check - underflow", "str-find", { }, e_underflow ),
		EVAL( "sanity check - underflow", "$ a str-find", { }, e_underflow ),
		EVAL( "find at start", "$ \"hello world\" $ \"hello\" str-find", { "hello world", 0 } ),
		EVAL( "find at end", "$ \"hello world\" $ \"world\" str-find", { "hello world", 6 } ),
		EVAL( "not found", "$ \"hello world\" $ \"goodbye\" str-find", { "hello world", std::string::npos } ),
		EVAL( "str-npos equality", "$ \"hello world\" $ \"goodbye\" str-find str-npos ==", { "hello world", true } ),
	} );

	eval_test( t, "str-find-r", {
		EVAL( "sanity check - underflow", "str-find-r", { }, e_underflow ),
		EVAL( "sanity check - underflow", "$ a str-find-r", { }, e_underflow ),
		EVAL( "str-find finds first", "$ \"this is a test\" $ \"is\" str-find", { "this is a test", 2 } ),
		EVAL( "str-find-r finds last", "$ \"this is a test\" $ \"is\" str-find-r", { "this is a test", 5 } ),
		EVAL( "not found", "$ \"hello world\" $ \"goodbye\" str-find-r", { "hello world", std::string::npos } ),
	} );

	eval_test( t, "str-ifind", {
		EVAL( "sanity check - underflow", "str-ifind", { }, e_underflow ),
		EVAL( "sanity check - underflow", "$ a str-ifind", { }, e_underflow ),
		EVAL( "ifind at start", "$ \"HellO World\" $ \"hello\" str-ifind", { "HellO World", 0 } ),
		EVAL( "ifind at end", "$ \"hELlo wORld\" $ \"worlD\" str-ifind", { "hELlo wORld", 6 } ),
		EVAL( "not found", "$ \"helLO World\" $ \"goODbye\" str-ifind", { "helLO World", std::string::npos } ),
		EVAL( "str-npos equality", "$ \"Hello World\" $ \"Goodbye\" str-ifind str-npos ==", { "Hello World", true } ),
	} );

	eval_test( t, "str-ifind-r", {
		EVAL( "sanity check - underflow", "str-ifind-r", { }, e_underflow ),
		EVAL( "sanity check - underflow", "$ a str-ifind-r", { }, e_underflow ),
		EVAL( "str-ifind finds first", "$ \"this is a test\" $ \"IS\" str-ifind", { "this is a test", 2 } ),
		EVAL( "str-ifind-r finds last", "$ \"this IS a test\" $ \"is\" str-ifind-r", { "this IS a test", 5 } ),
		EVAL( "not found", "$ \"Hello World\" $ \"Goodbye\" str-ifind-r", { "Hello World", std::string::npos } ),
	} );

	eval_test( t, "boolean", {
		EVAL( "sanity check - underflow", "&&", { }, e_underflow ),
		EVAL( "sanity check - underflow", "||", { }, e_underflow ),
		EVAL( "0 and 0", "false false &&", { false } ),
		EVAL( "0 and 1", "false true &&", { false } ),
		EVAL( "1 and 0", "true false &&", { false } ),
		EVAL( "1 and 1", "true true &&", { true } ),
		EVAL( "0 or 0", "false false ||", { false } ),
		EVAL( "0 or 1", "false true ||", { true } ),
		EVAL( "1 or 0", "true false ||", { true } ),
		EVAL( "0 not", "false not", { true } ),
		EVAL( "1 not", "true not", { false } ),
	} );

	eval_test( t, "bit shifts", {
		EVAL( "left", "0 1 <<", { 0 } ),
		EVAL( "left", "1 1 <<", { 2 } ),
		EVAL( "left", "2 1 <<", { 4 } ),
		EVAL( "left", "4 2 <<", { 16 } ),
		EVAL( "left", "-1 2 <<", { -4 } ),
		EVAL( "right", "1 1 >>", { 0 } ),
		EVAL( "right", "2 1 >>", { 1 } ),
		EVAL( "right", "4 1 >>", { 2 } ),
		EVAL( "right", "4 2 >>", { 1 } ),
		EVAL( "right", "4 3 >>", { 0 } ),
		EVAL( "right", "-8 2 >>", { -2 } ),
	} );

	eval_test( t, "bit operations", {
		EVAL( "and", "0 0 &", { 0 } ),
		EVAL( "and", "0 1 &", { 0 } ),
		EVAL( "and", "1 0 &", { 0 } ),
		EVAL( "and", "1 1 &", { 1 } ),
		EVAL( "or", "0 0 |", { 0 } ),
		EVAL( "or", "0 1 |", { 1 } ),
		EVAL( "or", "1 0 |", { 1 } ),
		EVAL( "or", "1 1 |", { 1 } ),
		EVAL( "xor", "0 0 ^", { 0 } ),
		EVAL( "xor", "0 1 ^", { 1 } ),
		EVAL( "xor", "1 0 ^", { 1 } ),
		EVAL( "xor", "1 1 ^", { 0 } ),
		EVAL( "complement", "0 ~", { -1 } ),
		EVAL( "complement", "1 ~", { -2 } ),
		EVAL( "complement", "2 ~", { -3 } ),
		EVAL( "complement", "-1 ~", { 0 } ),
		EVAL( "complement", "-2 ~", { 1 } ),
		EVAL( "complement", "-3 ~", { 2 } ),
	} );
}

void eval_tests4( suite &t )
{
	eval_test( t, "return stack", {
		EVAL( "is empty", "rdepth", { 0 } ),
		EVAL( "underflows when empty", "r>", { }, e_stack ),
		EVAL( "underflows when empty", "r@", { }, e_stack ),
		EVAL( "accepts ints", "10 >r", { } ),
		EVAL( "is not empty", "rdepth", { 1 } ),
		EVAL( "holds ints", "r@", { 10 } ),
		EVAL( "allows return", "r>", { 10 } ),
		EVAL( "is empty", "rdepth", { 0 } ),
		EVAL( "can accept a list", "[[ a b c ]] >r-list", { } ),
		EVAL( "can return a list", "r-list>", { "a", "b", "c", 3 } ),
	} );

	eval_test( t, "vector", {
		EVAL( "vector-new creates a vector", "vector-new", { "<vector>" } ),
		EVAL( "vector-new creates an empty vector", "vector-new vector-size", { "<vector>", 0 } ),
		EVAL( "vector-append adds to a vector", "vector-new 999 vector-append vector-size", { "<vector>", 1 } ),
		EVAL( "vector-query obtains item", "vector-new 999 vector-append 0 vector-query", { "<vector>", 999 } ),
		EVAL( "list-to-vector consumes a list", "[[ a b c ]] list-to-vector vector-size", { "<vector>", 3 } ),
		EVAL( "vector-to-list extracts a list", "[[ a b c ]] list-to-vector vector-to-list", { "a", "b", "c", 3 } ),
		EVAL( "vector-clone creates a copy", "[[ a b c ]] list-to-vector vector-clone vector-size", { "<vector>", "<vector>", 3 } ),
		EVAL( "vector-remove removes specified index", "[[ a b c ]] list-to-vector 1 vector-remove vector-to-list", { "a", "c", 2 } ),
		EVAL( "vector-join joins vectors", "[[ a b c ]] list-to-vector [[ d e f ]] list-to-vector vector-join vector-size", { "<vector>", 6 } ),
		EVAL( "vector-join joins vectors correctly", "[[ a b c ]] list-to-vector [[ d e f ]] list-to-vector vector-join vector-to-list", { "a", "b", "c", "d", "e", "f", 6 } ),
	} );

	eval_test( t, "rational", {
		EVAL( "convert numeric to rational", "10 rational", { "10:1" } ),
		EVAL( "convert rational string to rational", "10:1 rational", { "10:1" } ),
		EVAL( "convert rational string to rational and reduce by common factors", "10:2 rational", { "5:1" } ),
		EVAL( "throw exection on 0 denominator", "10:0 rational", { }, e_std_exception ),
		EVAL( "throw exection on non-integer numeric", "10.1 double rational", { }, e_std_exception ),
		EVAL( "rational addition", "1:2 3:4 r+", { "5:4" } ),
		EVAL( "rational additional addition", "1:2 3:4 r+ 1:4 r+", { "3:2" } ),
		EVAL( "rational addition and conversion", "1:2 3:4 r+ double", { 1.25 } ),
		EVAL( "rational subtraction", "3:4 1:2 r-", { "1:4" } ),
		EVAL( "rational subtraction with negative result", "1:2 3:4 r-", { "-1:4" } ),
		EVAL( "rational multiplication by numeric", "576 16:9 *", { 1024 } ),
		EVAL( "numeric multiplication by rational", "480 4:3 *", { 640 } ),
		EVAL( "numeric division by rational", "48000 30000:1001 r/", { "8008:5" } ),
		EVAL( "rational numerator", "8008:5 r-num", { "8008:5", 8008 } ),
		EVAL( "rational denominator", "8008:5 r-den", { "8008:5", 5 } ),
		EVAL( "rational division of ints", "1024 576 r/", { "16:9" } ),
	} );

	eval_test( t, "for", {
		EVAL( "empty list and body", "0 for next", { } ),
		EVAL( "empty list and non-empty body", "0 for i next", { } ),
		EVAL( "non-empty list and empty body", "[[ a b c ]] for next", { } ),
		EVAL( "non-empty list and non-empty body", "[[ a b c ]] for i next", { "a", "b", "c" } ),
		EVAL( "i can be treated as numeric", "0 [[ 1 2 3 ]] for i + next", { 6 } ),
	} );

	eval_test( t, "locals", {
		EVAL( "locals outside of words are globals", "10 local a a", { 10 } ),
		EVAL( "locals outside of words are persistent", "a", { 10 } ),
		EVAL( "locals can be redefined", "30 local a a", { 30 } ),
		EVAL( "locals inside words don't affect globals", ": hello local a a ; 20 hello a", { 20, 30 } ),
		EVAL( "locals can have different types", ": hello local a a ; $ goodbye hello a", { "goodbye", 30 } ),
		EVAL( "locals only exist within their scope", ": hello local b b ; 50 hello b", { 50 }, e_std_exception ),
	} );

	eval_test( t, "full words", {
		EVAL( "are equivalent to short", "10 dup 10 ::stack->dup", { 10, 10, 10, 10 } ),
		EVAL( "are validated", "10 dup 10 ::not-in-stack->dup", { 10, 10, 10 }, e_std_exception ),
		EVAL( "are not accepted as names for new words", ": ::stack->dup 1 pick ;", { }, e_std_exception ),
		EVAL( "are accepted within words", ": hello 10 ::stack->dup ; hello", { 10, 10 } ),
		EVAL( "are usable for user defined words", "::user->hello", { 10, 10 } ),
		EVAL( "are forgettable", "forget ::user->hello", { } ),
		EVAL( "are forgotten", "::user->hello", { }, e_std_exception ),
		EVAL( "are recreatable", ": hello $ hello ; ::user->hello", { "hello" } ),
		EVAL( "can change vocabs", ": new-vocab vocab : hello $ hello-there ; ; new-vocab ::new-vocab->hello", { "hello-there" } ),
		EVAL( "are gone after changing vocabs", "::user->hello", { }, e_undefined ),
		EVAL( "and still function as short", "hello", { "hello-there" } ),
	} );
}
