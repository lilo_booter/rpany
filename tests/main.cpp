#include <rpany/rpany-host.hpp>
#include <rpany/tests.hpp>

using namespace rpany;

void run_tests( suite &t );
void eval_tests1( suite &t );
void eval_tests2( suite &t );
void eval_tests3( suite &t );
void eval_tests4( suite &t );

int main( int argc, char **argv )
{
	// Create the suite
	stack s;
	suite t( s, argv[ 0 ] );

	run_tests( t );
	eval_tests1( t );
	eval_tests2( t );
	eval_tests3( t );
	eval_tests4( t );

	return t.failed;
}
